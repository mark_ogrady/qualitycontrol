﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Model;

namespace Mobi.QualityControl.Framework.Seed
{
    public class SeedLanguage
    {
        public static void CreateLanguages(QcContext context)
        {

            var languageList = new List<Language>()
                {
                      new Language()
                        {
                            LanguageId = Guid.NewGuid(),
                            LanguageName = "English",
                            LanguageCulture = "en-US",
                            ImageUrl = @"/Content/Admin/img/language/en.png"
                        },
                          new Language()
                        {
                            LanguageId = Guid.NewGuid(),
                            LanguageName = "French",
                            LanguageCulture = "fr-FR",
                            ImageUrl = @"/Content/Admin/img/language/fr.png"
                        },
                           new Language()
                        {
                            LanguageId = Guid.NewGuid(),
                            LanguageName = "Polish",
                            LanguageCulture = "pl",
                            ImageUrl = @"/Content/Admin/img/language/pl.png"
                        },
                         

                };
            foreach (var language in languageList)
            {
                var existingLanguage = context.Languages.FirstOrDefault(p => p.LanguageName == language.LanguageName);
                if (existingLanguage == null)
                    context.Languages.Add(language);
            }
            context.SaveChanges();
        }
    }
}
