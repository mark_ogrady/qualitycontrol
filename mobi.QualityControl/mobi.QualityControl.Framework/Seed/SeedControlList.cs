﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Model;


namespace Mobi.QualityControl.Framework.Seed
{
    public class SeedControlList
    {
        public static void CreateControlLists(QcContext context)
        {
            var company = context.Companies.FirstOrDefault(o => o.CompanyName == "Seed Company 1");

            for (int i = 0; i < 5; i++)
            {
                var controlList = new Mobi.QualityControl.Framework.Model.ControlList();
                controlList.ControlListName = "ControlList " + i.ToString();
                controlList.ControlListId = Guid.NewGuid();
                controlList.Company = company;

                context.ControlLists.Add(controlList);
                context.SaveChanges();

                for (int j = 0; j < 10; j++)
                {
                    var controlListItem = new ControlListItem();
                    controlListItem.ControlListItemId = Guid.NewGuid();
                    controlListItem.ControlValue = "Value " + j.ToString();
                    controlList.ControlListItems.Add(controlListItem);
                }
                context.SaveChanges();
            }

            context.SaveChanges();


        }
    }
}
