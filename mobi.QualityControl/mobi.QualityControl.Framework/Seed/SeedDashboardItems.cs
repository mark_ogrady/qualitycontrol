﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Enum;
using Mobi.QualityControl.Framework.Model;

namespace Mobi.QualityControl.Framework.Seed
{
    public class SeedDashboardItems
    {
        public static void CreateDashboardItems(QcContext context)
        {
            var userProfile = context.UserProfiles.FirstOrDefault(o => o.UserName == "mark@markogrady.com");

            if (userProfile != null)
            {
                var dashboardItems = new List<DashboardItem>()
                {
                    new DashboardItem()
                    {
                        DashboardItemId = Guid.NewGuid(),
                        DashboardType = DashboardType.QuestionSetStart,
                        Description = "QuickStart",
                        Name = "Quick Start 1",
                        EntityId = userProfile.Company.QuestionSets.FirstOrDefault().QuestionSetId,
                        OrderPosition = 1,
                        UserProfile = userProfile
                    },
                             new DashboardItem()
                    {
                        DashboardItemId = Guid.NewGuid(),
                        DashboardType = DashboardType.QuestionSetStart,
                        Description = "QuickStart",
                        Name = "Quick Start 2",
                        EntityId = userProfile.Company.QuestionSets.FirstOrDefault().QuestionSetId,
                        OrderPosition = 2,
                        UserProfile = userProfile
                    },
                };

                foreach (var dashboardItem in dashboardItems)
                {
                    var existingDashboardItem = context.DashboardItems.FirstOrDefault(p => p.UserProfile.UserName == userProfile.UserName && p.Name == dashboardItem.Name);
                    if (existingDashboardItem == null)
                        context.DashboardItems.Add(dashboardItem);
                }
                context.SaveChanges();
            }
        }
    }
}
