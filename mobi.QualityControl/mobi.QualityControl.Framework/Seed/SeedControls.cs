﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Model;

namespace Mobi.QualityControl.Framework.Seed
{
    public static class SeedControls
    {
        public static void CreateStandardControls(QcContext context)
        {
            var controlList = new List<ControlType>()
                {
                    new ControlType(){
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "SingleText",
                            Description = "Single Line of Text",
                            Icon = "",
                            OrderPosition = 1,
                            Version = 1
                        },
                         new ControlType()
                        {
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "MultiText",
                            Description = "Paragraph Text",
                            Icon = "",
                            OrderPosition = 2,
                            Version = 1
                        },
						  new ControlType()
                        {
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "Number",
                            Description = "Number",
                            Icon = "",
                            OrderPosition = 2,
                            Version = 1
                        },
						  new ControlType()
                        {
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "Date",
                            Description = "Date",
                            Icon = "",
                            OrderPosition = 2,
                            Version = 1
                        },
                          new ControlType()
                        {
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "Image Range",
                            Description = "Image Range",
                            AllowControlList = true,
                            Icon = "",
                            OrderPosition = 2,
                            Version = 1
                        },
						  new ControlType()
                        {
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "Time",
                            Description = "Time",
                            Icon = "",
                            OrderPosition = 2,
                            Version = 1
                        },
						  new ControlType()
                        {
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "DropDown",
                            Description = "Dropdown list of items",
                            AllowControlList = true,
                            Icon = "",
                            OrderPosition = 2,
                            Version = 1
                        },
						  new ControlType()
                        {
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "Checkbox",
                            Description = "Single Checkbox",
                            Icon = "",
                            OrderPosition = 2,
                            DefaultValue = "0",
                            Version = 1
                        },
						   new ControlType()
                        {
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "Camera",
                            Description = "Camera Image",
                            Icon = "",
                            OrderPosition = 2,
                            Version = 1
                        },
						  new ControlType()
                        {
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "Barcode",
                            Description = "Barcode",
                            Icon = "",
                            OrderPosition = 2,
                            Version = 1
                        },
                         new ControlType()
                        {
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "Count",
                            Description = "Counter",
                            Icon = "",
                            OrderPosition = 2,
                            Version = 1
                        },
						 new ControlType()
                        {
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "Timer",
                            Description = "Timer",
                            Icon = "",
                            OrderPosition = 2,
                            Version = 1
                        },
                         new ControlType()
                        {
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "Decimal",
                            Description = "Decimal",
                            Icon = "",
                            OrderPosition = 2,
                            Version = 1
                        },
                         new ControlType()
                        {
                            ControlTypeId = Guid.NewGuid(),
                            ControlName = "Location",
                            Description = "Location",
                            Icon = "",
                            OrderPosition = 2,
                            Version = 1
                        }
						
						
						
						 
                };

            foreach (var controlType in controlList)
            {
                //Find controls
                var existingControl = context.ControlTypes.FirstOrDefault(o => o.ControlName == controlType.ControlName);
                if (existingControl == null)
                {
                    context.ControlTypes.Add(controlType);

                }

            }


            context.SaveChanges();
        }
    }
}
