﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Enum;
using Mobi.QualityControl.Framework.Model;

namespace Mobi.QualityControl.Framework.Seed
{
    public class SeedSubscription
    {
        public static void CreateSubscriptions(QcContext context)
        {

            var companySubscriptions = new List<CompanySubscription>()
                {
                      new CompanySubscription()
                        {
                            SubscriptionTypeId = Guid.Parse("D622C359-CF3D-4747-8D10-04439B2FE22E"),
                           Name = "Free",
                           BillingCycleType = BillingCycleType.Monthly,
                           Price = 0,
                           NumberOfUsers = 999999999,
                           TransactionLimit = 100

                           },
                            new CompanySubscription()
                        {
                            SubscriptionTypeId = Guid.Parse("26914EFA-FAC5-4F0A-8576-902ACC289AA9"),
                           Name = "Basic",
                           BillingCycleType = BillingCycleType.Monthly,
                           Price = 7,
                           NumberOfUsers = 1,
                           TransactionLimit = 999999999

                           },
                           
                            new CompanySubscription()
                        {
                            SubscriptionTypeId = Guid.Parse("8A11E54F-01EA-4219-9910-D72DF0829F14"),
                           Name = "Basic",
                           BillingCycleType = BillingCycleType.Monthly,
                           Price = 7,
                           NumberOfUsers = 999999999,
                           TransactionLimit = 999999999

                           },
                         

                };
            foreach (var companySub in companySubscriptions)
            {
                var existingSubs = context.CompanySubscriptions.FirstOrDefault(p => p.Name == companySub.Name);
                if (existingSubs == null)
                    context.CompanySubscriptions.Add(companySub);
            }
            context.SaveChanges();
        }
    }
}
