﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Model;

namespace Mobi.QualityControl.Framework.Seed
{
    public static class SeedMenu
    {
        public static void CreateStandardMenu(QcContext context)
        {
          
            var menuList = new List<Menu>()
                {
                      new Menu()
                        {
                            MenuId = Guid.NewGuid(),
                            MenuName = "Questions",
                            MenuType = 2,
                            Link = "/QuestionSet",
                            Icon = "fa-pencil-square-o",
                            OrderPosition = 1
                        },
                         new Menu()
                        {
                            MenuId = Guid.NewGuid(),
                            MenuName = "Jobs",
                            MenuType = 2,
                            Link = "/Jobset",
                            Icon = "fa-pencil-square-o",
                            OrderPosition = 2
                        },
                         new Menu()
                        {
                            MenuId = Guid.NewGuid(),
                            MenuName = "Approvals",
                            MenuType = 2,
                            Link = "/Approval",
                            Icon = "fa-thumbs-up",
                            OrderPosition = 3
                        },
                          new Menu()
                        {
                            MenuId = Guid.NewGuid(),
                            MenuName = "Results",
                            MenuType = 2,
                            Link = "/results",
                            Icon = " fa-bar-chart-o",
                            OrderPosition = 4
                        },
                         new Menu()
                        {
                            MenuId = Guid.NewGuid(),
                            MenuName = "Control Lists",
                            MenuType = 2,
                            Link = "/controllist",
                            Icon = "fa-align-justify",
                            OrderPosition = 5
                        },
                     new Menu()
                        {
                            MenuId = Guid.NewGuid(),
                            MenuName = "Connections",
                            MenuType = 2,
                            Link = "/Relationship",
                            Icon = "fa-group",
                            OrderPosition = 6
                        },
                    
                     new Menu()
                        {
                            MenuId = Guid.NewGuid(),
                            MenuName = "Settings",
                            MenuType = 2,
                            Link = "/settings",
                            Icon = "fa-cogs",
                            OrderPosition = 7
                        }

                };
            foreach (var menu in menuList)
            {
                var existingMenu = context.Menus.FirstOrDefault(p => p.MenuName == menu.MenuName && p.MenuType == menu.MenuType);
                if (existingMenu == null)
                    context.Menus.Add(menu);
            }
            context.SaveChanges();
        }
    }
}
