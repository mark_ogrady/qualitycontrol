﻿using System;
using System.Linq;
using System.Web.Mvc;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Enum;
using Mobi.QualityControl.Framework.Model;

namespace Mobi.QualityControl.Framework.Seed
{
    public static class SeedQuestion
    {
        public static void CreateQuestionSetsForLpCompany(QcContext context)
        {
            var company = context.Companies.FirstOrDefault(c => c.CompanyName == "Little Parsley");

            if (company != null)
            {
                for (int i = 0; i < 5; i++)
                {
                    var questionSetName = "Question Set Sd" + i.ToString();

                    var questionSetQuery =
                        context.QuestionSets.Where(
                            o => o.QuestionSetName == questionSetName && o.Company.CompanyId == company.CompanyId);

                    if (questionSetQuery.Count() == 0)
                    {
                        var questionSet = new QuestionSet();
                        questionSet.QuestionSetId = Guid.NewGuid();
                        questionSet.QuestionSetName = questionSetName;
                        questionSet.Company = company;
                        context.QuestionSets.Add(questionSet);
                    }
                }

                context.SaveChanges();
            }

        }


        public static void CreateQuestionSetsForLpCompanyFlowers(QcContext context)
        {
            var company = context.Companies.FirstOrDefault(c => c.CompanyName == "Little Parsley");

            if (company != null)
            {

                var questionFlowers = "Flowers in Fields";

                var questionSetQuery =
                    context.QuestionSets.Where(
                        o => o.QuestionSetName == questionFlowers && o.Company.CompanyId == company.CompanyId);

                if (questionSetQuery.Count() == 0)
                {
                    var questionSet = new QuestionSet();
                    questionSet.QuestionSetId = Guid.NewGuid();
                    questionSet.QuestionSetName = questionFlowers;
                    questionSet.Company = company;
                    context.QuestionSets.Add(questionSet);

                    questionSet.Questions.Add(new Question()
                    {
                        QuestionText = "Which field are you in?",
                        ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "DropDown")
                    });
                    questionSet.Questions.Add(new Question()
                    {
                        QuestionText = "What is your exact GPS location?",
                        ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Location")
                    });
                    questionSet.Questions.Add(new Question()
                    {
                        QuestionText = "What type of bulb?",
                        ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "DropDownList")
                    });
                    questionSet.Questions.Add(new Question()
                    {
                        QuestionText = "How many bulbs?",
                        ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Count")
                    });
                    questionSet.Questions.Add(new Question()
                    {
                        QuestionText = "How many blossomed?",
                        ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Count")
                    });
                    questionSet.Questions.Add(new Question()
                    {
                        QuestionText = "Predicted Bloom date?",
                        ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Date")
                    });
                    questionSet.Questions.Add(new Question()
                    {
                        QuestionText = "Take a picture",
                        ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Camera")
                    });
                }


                context.SaveChanges();
            }

        }

        public static void CreateQuestionSetsForHerrcoCompany(QcContext context)
        {
            var company = context.Companies.FirstOrDefault(c => c.CompanyName == "Herrco Cosmetics Ltd.");

            if (company != null)
            {

                if (company.QuestionSets.FirstOrDefault(o => o.QuestionSetName == "Sample 1") == null)
                {
                    var questionSet = new QuestionSet();
                    questionSet.QuestionSetId = new Guid();
                    questionSet.QuestionSetName = "Sample 1";
                    company.QuestionSets.Add(questionSet);

                    //Batch
                    var question1 = new Question();
                    question1.QuestionId = Guid.NewGuid();
                    question1.QuestionText = "Batch Code";
                    question1.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Single Text");
                    question1.OrderPosition = 1;
                    questionSet.Questions.Add(question1);


                    //Batch
                    var question2 = new Question();
                    question2.QuestionId = Guid.NewGuid();
                    question2.QuestionText = "Number";
                    question2.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Number");
                    question2.OrderPosition = 1;
                    questionSet.Questions.Add(question2);

                    //Batch
                    var question3 = new Question();
                    question3.QuestionId = Guid.NewGuid();
                    question3.QuestionText = "Batch Code";
                    question3.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Single Text");
                    question3.OrderPosition = 1;
                    questionSet.Questions.Add(question3);

                    //Batch
                    var question4 = new Question();
                    question4.QuestionId = Guid.NewGuid();
                    question4.QuestionText = "Batch Code";
                    question4.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Single Text");
                    question4.OrderPosition = 1;
                    questionSet.Questions.Add(question4);

                    //Batch
                    var quetsion5 = new Question();
                    quetsion5.QuestionId = Guid.NewGuid();
                    quetsion5.QuestionText = "Batch Code";
                    quetsion5.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Single Text");
                    quetsion5.OrderPosition = 1;
                    questionSet.Questions.Add(quetsion5);


                    //Batch
                    var question6 = new Question();
                    question6.QuestionId = Guid.NewGuid();
                    question6.QuestionText = "Batch Code";
                    question6.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Single Text");
                    question6.OrderPosition = 1;
                    questionSet.Questions.Add(question6);

                    //Batch
                    var question7 = new Question();
                    question7.QuestionId = Guid.NewGuid();
                    question7.QuestionText = "Batch Code";
                    question7.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Single Text");
                    question7.OrderPosition = 1;
                    questionSet.Questions.Add(question7);





                }




                context.SaveChanges();
            }

        }

        public static void CreateQuestionsForMany(QcContext context)
        {
            var manyCompanies = context.Companies.Select(o => new { o.CompanyId, o.CompanyName, o.QuestionSets.Count }).Where(o => o.CompanyName.StartsWith("Many") && o.Count == 0).ToList();

            var testcounnt = manyCompanies.Count();
            foreach (var manyCompany in manyCompanies)
            {
                for (int i = 0; i < 10000; i++)
                {
                    var questionSetName = "ManyQuestionSet" + i.ToString();
                    //create question set with 30 questions
                    var existingQuestionSet =
                        context.QuestionSets.Select(o => new { o.Company.CompanyId, o.QuestionSetName })
                        .FirstOrDefault(o => o.CompanyId == manyCompany.CompanyId && o.QuestionSetName == questionSetName);
                    if (existingQuestionSet == null)
                    {
                        
                        //create question set
                        var quesetionSet = new QuestionSet();
                        quesetionSet.QuestionSetId = Guid.NewGuid();
                        quesetionSet.Company =
                            context.Companies.FirstOrDefault(o => o.CompanyId == manyCompany.CompanyId);
                        quesetionSet.QuestionSetName = questionSetName;
                        
                        for (int j = 0; j < 10; j++)
                        {
                            var questionText = "What" + j.ToString();
                            var existingQuestion = context.Questions.Select(o => new {o.QuestionText, o.QuestionSet.QuestionSetId})
                                    .FirstOrDefault(o => o.QuestionSetId == quesetionSet.QuestionSetId && o.QuestionText == questionText);
                            if (existingQuestion == null)
                            {
                                var question = new Question();
                                question.QuestionId = Guid.NewGuid();
                                question.QuestionText = questionText;
                                question.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "SingleText");
                                
                                quesetionSet.Questions.Add(question);

                            }
                        }
                        context.QuestionSets.Add(quesetionSet);
                        
                    }
                    context.SaveChanges();

                }

            }

        }

        public static void CreateQuestionSetsForSeedCompany(QcContext context)
        {
            var company = context.Companies.FirstOrDefault(c => c.CompanyName == "Seed Standard1");


            for (int i = 0; i < 5; i++)
            {
                var questionSetName = "Seed Question Set" + i.ToString();

                var questionSetQuery = context.QuestionSets.Where(o => o.QuestionSetName == questionSetName && o.Company.CompanyId == company.CompanyId);

                if (questionSetQuery.Count() == 0)
                {
                    var questionSet = new QuestionSet();
                    questionSet.QuestionSetId = Guid.NewGuid();
                    questionSet.QuestionSetName = questionSetName;
                    questionSet.Company = company;
                    context.QuestionSets.Add(questionSet);
                }
            }

            context.SaveChanges();


        }
        
        public static void AddQuestionsToQuestionSet(QcContext context)
        {
            var company = context.Companies.FirstOrDefault(c => c.CompanyName == "Little Parsley");

            foreach (var questionSet in company.QuestionSets.Where(o => !o.Questions.Any()).ToList())
            {

                var i = 0;
                var controls = context.ControlTypes.Where(o => !o.IsDeleted);
                foreach (var controlType in controls.ToList())
                {
                    var question = new Question();
                    question.QuestionId = Guid.NewGuid();
                    question.ControlType = controlType;
                    question.QuestionText = controlType.ControlName;
                    question.OrderPosition = i;
                    questionSet.Questions.Add(question);
                    i++;
                }

            }

            context.SaveChanges();
        }

        public static void CreateQuestionSetWith1Rule(QcContext context)
        {
            var companyId = Guid.Parse("F2A390FF-9171-4C00-99D8-151D250D0433");
            var company = context.Companies.FirstOrDefault(c => c.CompanyId == companyId);
            var questionSetId = Guid.Parse("CE458E27-2C5E-4528-97B9-36852261F39D");
            if (company != null)
            {

                if (company.QuestionSets.FirstOrDefault(o => o.QuestionSetId == questionSetId) == null)
                {
                    var questionSet = new QuestionSet();
                    questionSet.QuestionSetId = Guid.Parse("CE458E27-2C5E-4528-97B9-36852261F39D");
                    questionSet.QuestionSetName = "Question with one rule";
                    company.QuestionSets.Add(questionSet);

                    //Batch
                    var question1 = new Question();
                    question1.QuestionId = Guid.Parse("3BD17ED0-0BEF-4D90-8856-44F57FE757F4");
                    question1.QuestionText = "Batch Code";
                    question1.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Single Text");
                    question1.OrderPosition = 1;
                    questionSet.Questions.Add(question1);

                    //Batch
                    var question2 = new Question();
                    question2.QuestionId = Guid.Parse("88FCA838-598C-4996-AF40-1158616C0C28");
                    question2.QuestionText = "Number";
                    question2.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Number");
                    question2.OrderPosition = 1;

                    var rule = new QuestionRule();
                    rule.QuestionRuleId = Guid.Parse("004F0FD0-67E7-4BAC-9186-7E99DBA49F9B");
                    rule.FromQuestion = question1;
                    rule.ViewEvalutionType = ViewEvalutionType.Equals;
                    rule.EvaluationValue = "2";
                    rule.CreatedDate = DateTime.UtcNow;
                   
                    
                    questionSet.Questions.Add(question2);
                    context.QuestionRules.Add(rule);
                    //Batch
                    var question3 = new Question();
                    question3.QuestionId = Guid.NewGuid();
                    question3.QuestionText = "Batch Code";
                    question3.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Single Text");
                    question3.OrderPosition = 1;
                    questionSet.Questions.Add(question3);

                    //Batch
                    var question4 = new Question();
                    question4.QuestionId = Guid.NewGuid();
                    question4.QuestionText = "Batch Code";
                    question4.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Single Text");
                    question4.OrderPosition = 1;
                    questionSet.Questions.Add(question4);

                    //Batch
                    var quetsion5 = new Question();
                    quetsion5.QuestionId = Guid.NewGuid();
                    quetsion5.QuestionText = "Batch Code";
                    quetsion5.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Single Text");
                    quetsion5.OrderPosition = 1;
                    questionSet.Questions.Add(quetsion5);


                    //Batch
                    var question6 = new Question();
                    question6.QuestionId = Guid.NewGuid();
                    question6.QuestionText = "Batch Code";
                    question6.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Single Text");
                    question6.OrderPosition = 1;
                    questionSet.Questions.Add(question6);

                    //Batch
                    var question7 = new Question();
                    question7.QuestionId = Guid.NewGuid();
                    question7.QuestionText = "Batch Code";
                    question7.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Single Text");
                    question7.OrderPosition = 1;
                    questionSet.Questions.Add(question7);





                }




                context.SaveChanges();
            }
        }
    }
}
