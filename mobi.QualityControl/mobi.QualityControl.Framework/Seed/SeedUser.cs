﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Model;

namespace Mobi.QualityControl.Framework.Seed
{
    public class SeedUser
    {
        public static void FixUsers(QcContext context)
        {



            var userMark = context.UserProfiles.FirstOrDefault(o => o.UserName == "mark@markogrady.com");
            if (userMark != null)
            {
                userMark.FirstName = "Mark";
                userMark.LastName = "O'Grady";

            }
            context.SaveChanges();
            var seedUsers = context.UserProfiles.Where(o => o.FirstName == "");
            if (seedUsers != null)
            {
                foreach (var userProfile in seedUsers)
                {
                    userProfile.FirstName = "Seed";
                    userProfile.LastName =  userProfile.UserId.ToString().Substring(0,5);
                    context.SaveChanges();
                }
                

            }
           
        }
    }
}
