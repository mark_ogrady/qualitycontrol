﻿using System;
using System.Linq;
using Mobi.QualityControl.Framework.DataContext;

namespace Mobi.QualityControl.Framework.Seed
{
    public static class SeedCompany
    {
        public static void CreateStandardCompany(QcContext context)
        {
            var lpCompany = new Model.Company();
            lpCompany.CompanyId = Guid.NewGuid();
            lpCompany.CompanyName = "Little Parsley";

            var existingLpCompany = context.Companies.FirstOrDefault(o => o.CompanyName == lpCompany.CompanyName);
            if (existingLpCompany == null)
            {
                context.Companies.Add(lpCompany);
                context.SaveChanges();
            }


            var herCompany = new Model.Company();
            herCompany.CompanyId = Guid.NewGuid();
            herCompany.CompanyName = "Herrco Cosmetics Ltd.";
            

            var existinghrCompany = context.Companies.FirstOrDefault(o => o.CompanyName == herCompany.CompanyName);
            if (existinghrCompany == null)
            {
                context.Companies.Add(herCompany);
                context.SaveChanges();
            }

            var unitCompany = new Model.Company();
            unitCompany.CompanyId = Guid.Parse("C1C21A46-43C4-4F7B-A462-8B53CD613936");
            unitCompany.CompanyName = "Unit";

            var existingUnitCompany = context.Companies.FirstOrDefault(o => o.CompanyId == unitCompany.CompanyId);
            if (existingUnitCompany == null)
            {
                context.Companies.Add(unitCompany);
                context.SaveChanges();
            }   
            
            
            var unitCompany2 = new Model.Company();
            unitCompany2.CompanyId = Guid.Parse("F2A390FF-9171-4C00-99D8-151D250D0433");
            unitCompany2.CompanyName = "Unit2";

            var existingUnit2Company = context.Companies.FirstOrDefault(o => o.CompanyId == unitCompany2.CompanyId);
            if (existingUnit2Company == null)
            {
                context.Companies.Add(unitCompany2);
                context.SaveChanges();
            }




            for (int i = 0; i < 20; i++)
            {
                var company = new Model.Company();
                company.CompanyId = Guid.NewGuid();
                company.CompanyName = "Seed Standard" + i.ToString();
                var existingCompany = context.Companies.FirstOrDefault(o => o.CompanyName == company.CompanyName);
            if (existingCompany == null)
            {
                context.Companies.Add(company);
                context.SaveChanges();
            }
               
            }
           
        }

        public static void CreateManyCompanies(QcContext context)
        {
            for (int i = 0; i < 20000; i++)
            {
                var company = new Model.Company();
                company.CompanyId = Guid.NewGuid();
                company.CompanyName = "Many " + i.ToString();
                var existingCompany = context.Companies.Select(o => new { o.CompanyId, o.CompanyName}).FirstOrDefault(o => o.CompanyName == company.CompanyName);
                if (existingCompany == null)
                {
                    context.Companies.Add(company);
                }
            }
            context.SaveChanges();

        }


        public static void AddSeedUserToCompany(QcContext context)
        {



            var user = context.UserProfiles.FirstOrDefault(o => o.UserName == "st@qc.mobi");
            var company1 = context.Companies.FirstOrDefault(o => o.CompanyName == "Seed Standard1");
            if (company1 != null && user != null)
                user.Company = company1;
            var company2 = context.Companies.FirstOrDefault(o => o.CompanyName == "Little Parsley");
            var user1 = context.UserProfiles.FirstOrDefault(o => o.UserName == "mark@markogrady.com");
            if (company2 != null && user1 != null)
                user1.Company = company2;
            var company3 = context.Companies.FirstOrDefault(o => o.CompanyName == "Herrco Cosmetics Ltd.");
            var user3 = context.UserProfiles.FirstOrDefault(o => o.UserName == "Derek.Fraser@herrco.co.uk");
            if (company3 != null && user3 != null)
                user3.Company = company3;
            context.SaveChanges();
        }

    }
}
