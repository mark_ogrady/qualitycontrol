namespace Mobi.QualityControl.Framework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newsite : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationLog",
                c => new
                    {
                        ApplicationLogId = c.Guid(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        User = c.String(),
                        Company = c.String(),
                        Thread = c.String(),
                        Level = c.String(),
                        Logger = c.String(),
                        Message = c.String(),
                        Exception = c.String(),
                        IsDismissed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ApplicationLogId);
            
            CreateTable(
                "dbo.ApprovalRule",
                c => new
                    {
                        ApprovalRuleId = c.Guid(nullable: false),
                        UserId = c.Int(nullable: false),
                        OrderPosition = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedUser = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedUser = c.String(),
                        Version = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        QuestionSet_QuestionSetId = c.Guid(),
                    })
                .PrimaryKey(t => t.ApprovalRuleId)
                .ForeignKey("dbo.QuestionSet", t => t.QuestionSet_QuestionSetId)
                .Index(t => t.QuestionSet_QuestionSetId);
            
            CreateTable(
                "dbo.ApprovalStep",
                c => new
                    {
                        ApprovalStepId = c.Guid(nullable: false),
                        OrderPosition = c.Int(nullable: false),
                        CurrentStep = c.Boolean(nullable: false),
                        AcceptedDate = c.DateTime(),
                        RejectedDate = c.DateTime(),
                        UserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedUser = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedUser = c.String(),
                        Version = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        ApprovalRule_ApprovalRuleId = c.Guid(),
                        TransactionSet_TransactionSetId = c.Guid(),
                    })
                .PrimaryKey(t => t.ApprovalStepId)
                .ForeignKey("dbo.ApprovalRule", t => t.ApprovalRule_ApprovalRuleId)
                .ForeignKey("dbo.TransactionSet", t => t.TransactionSet_TransactionSetId)
                .Index(t => t.ApprovalRule_ApprovalRuleId)
                .Index(t => t.TransactionSet_TransactionSetId);
            
            CreateTable(
                "dbo.Job",
                c => new
                    {
                        JobId = c.Guid(nullable: false),
                        Jobcode = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedUser = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedUser = c.String(),
                        Version = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        JobSet_JobSetId = c.Guid(),
                    })
                .PrimaryKey(t => t.JobId)
                .ForeignKey("dbo.JobSet", t => t.JobSet_JobSetId)
                .Index(t => t.JobSet_JobSetId);
            
            CreateTable(
                "dbo.JobSet",
                c => new
                    {
                        JobSetId = c.Guid(nullable: false),
                        JobSetName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedUser = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedUser = c.String(),
                        Version = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        Company_CompanyId = c.Guid(),
                    })
                .PrimaryKey(t => t.JobSetId)
                .ForeignKey("dbo.Company", t => t.Company_CompanyId)
                .Index(t => t.Company_CompanyId);
            
            CreateTable(
                "dbo.Invoice",
                c => new
                    {
                        InvoiceId = c.Guid(nullable: false),
                        PaymentProvider = c.String(),
                        PaymentProviderCode = c.String(),
                        OrderNo = c.String(),
                        CustomerOrderNo = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedUser = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedUser = c.String(),
                        Version = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        Company_CompanyId = c.Guid(),
                        CompanySubscription_SubscriptionTypeId = c.Guid(),
                    })
                .PrimaryKey(t => t.InvoiceId)
                .ForeignKey("dbo.Company", t => t.Company_CompanyId)
                .ForeignKey("dbo.CompanySubscription", t => t.CompanySubscription_SubscriptionTypeId)
                .Index(t => t.Company_CompanyId)
                .Index(t => t.CompanySubscription_SubscriptionTypeId);
            
            CreateTable(
                "dbo.InvoiceLine",
                c => new
                    {
                        InvoiceLineId = c.Guid(nullable: false),
                        LineDescription = c.String(),
                        NetValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        VatRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        VatValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GrossValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OrderPosition = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedUser = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedUser = c.String(),
                        Version = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        Invoice_InvoiceId = c.Guid(),
                    })
                .PrimaryKey(t => t.InvoiceLineId)
                .ForeignKey("dbo.Invoice", t => t.Invoice_InvoiceId)
                .Index(t => t.Invoice_InvoiceId);
            
            CreateTable(
                "dbo.QuestionLocal",
                c => new
                    {
                        QuestionLocalId = c.Guid(nullable: false),
                        QuestionText = c.String(),
                        Language_LanguageId = c.Guid(),
                        Question_QuestionId = c.Guid(),
                    })
                .PrimaryKey(t => t.QuestionLocalId)
                .ForeignKey("dbo.Language", t => t.Language_LanguageId)
                .ForeignKey("dbo.Question", t => t.Question_QuestionId)
                .Index(t => t.Language_LanguageId)
                .Index(t => t.Question_QuestionId);
            
            CreateTable(
                "dbo.QuestionRule",
                c => new
                    {
                        QuestionRuleId = c.Guid(nullable: false),
                        ViewEvalutionType = c.Int(nullable: false),
                        EvaluationValue = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedUser = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedUser = c.String(),
                        Version = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        FromQuestion_QuestionId = c.Guid(),
                    })
                .PrimaryKey(t => t.QuestionRuleId)
                .ForeignKey("dbo.Question", t => t.FromQuestion_QuestionId)
                .Index(t => t.FromQuestion_QuestionId);
            
            CreateTable(
                "dbo.ResultViewShare",
                c => new
                    {
                        ResultViewShareId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedUser = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedUser = c.String(),
                        Version = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        Company_CompanyId = c.Guid(),
                        ResultView_ResultViewId = c.Guid(),
                    })
                .PrimaryKey(t => t.ResultViewShareId)
                .ForeignKey("dbo.Company", t => t.Company_CompanyId)
                .ForeignKey("dbo.ResultView", t => t.ResultView_ResultViewId)
                .Index(t => t.Company_CompanyId)
                .Index(t => t.ResultView_ResultViewId);
            
            CreateTable(
                "dbo.ResultView",
                c => new
                    {
                        ResultViewId = c.Guid(nullable: false),
                        ResultViewName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedUser = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedUser = c.String(),
                        Version = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        QuestionSet_QuestionSetId = c.Guid(),
                    })
                .PrimaryKey(t => t.ResultViewId)
                .ForeignKey("dbo.QuestionSet", t => t.QuestionSet_QuestionSetId)
                .Index(t => t.QuestionSet_QuestionSetId);
            
            CreateTable(
                "dbo.ResultViewDisplayItem",
                c => new
                    {
                        ResultViewDisplayItemId = c.Guid(nullable: false),
                        Question_QuestionId = c.Guid(),
                        ResultView_ResultViewId = c.Guid(),
                    })
                .PrimaryKey(t => t.ResultViewDisplayItemId)
                .ForeignKey("dbo.Question", t => t.Question_QuestionId)
                .ForeignKey("dbo.ResultView", t => t.ResultView_ResultViewId)
                .Index(t => t.Question_QuestionId)
                .Index(t => t.ResultView_ResultViewId);
            
            CreateTable(
                "dbo.ResultViewItem",
                c => new
                    {
                        ResultViewItemId = c.Guid(nullable: false),
                        ViewEvalutionType = c.Int(nullable: false),
                        ExpressionValue = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedUser = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedUser = c.String(),
                        Version = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        Question_QuestionId = c.Guid(),
                        ResultView_ResultViewId = c.Guid(),
                    })
                .PrimaryKey(t => t.ResultViewItemId)
                .ForeignKey("dbo.Question", t => t.Question_QuestionId)
                .ForeignKey("dbo.ResultView", t => t.ResultView_ResultViewId)
                .Index(t => t.Question_QuestionId)
                .Index(t => t.ResultView_ResultViewId);
            
            CreateTable(
                "dbo.UserSession",
                c => new
                    {
                        UserSessionId = c.Guid(nullable: false),
                        DeviceId = c.String(),
                        ExpirationDate = c.DateTime(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedUser = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.UserSessionId)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.TransactionSetNote",
                c => new
                    {
                        TransactionSetNoteId = c.Guid(nullable: false),
                        Note = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedUser = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedUser = c.String(),
                        Version = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        TransactionSet_TransactionSetId = c.Guid(),
                    })
                .PrimaryKey(t => t.TransactionSetNoteId)
                .ForeignKey("dbo.TransactionSet", t => t.TransactionSet_TransactionSetId)
                .Index(t => t.TransactionSet_TransactionSetId);
            
            CreateTable(
                "dbo.DocumentType",
                c => new
                    {
                        DocumentTypeId = c.Guid(nullable: false),
                        SystemName = c.String(),
                        FriendlyName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedUser = c.String(),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedUser = c.String(),
                        Version = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.DocumentTypeId);
            
            CreateTable(
                "dbo.QuestionSetJobSet",
                c => new
                    {
                        QuestionSet_QuestionSetId = c.Guid(nullable: false),
                        JobSet_JobSetId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.QuestionSet_QuestionSetId, t.JobSet_JobSetId })
                .ForeignKey("dbo.QuestionSet", t => t.QuestionSet_QuestionSetId, cascadeDelete: true)
                .ForeignKey("dbo.JobSet", t => t.JobSet_JobSetId, cascadeDelete: true)
                .Index(t => t.QuestionSet_QuestionSetId)
                .Index(t => t.JobSet_JobSetId);
            
            AddColumn("dbo.Company", "PaymentSubscriptionToken", c => c.String());
            AddColumn("dbo.Company", "IsSubscriptionActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.Company", "IsInPublicDirectory", c => c.Boolean(nullable: false));
            AddColumn("dbo.Company", "NumberOfUsers", c => c.Int(nullable: false));
            AddColumn("dbo.QuestionSet", "TrackLocation", c => c.Boolean(nullable: false));
            AddColumn("dbo.ControlType", "AllowControlList", c => c.Boolean(nullable: false));
            AddColumn("dbo.TransactionSet", "TransactionsSerialised", c => c.String(unicode: false));
            AddColumn("dbo.TransactionSet", "IsEntryComplete", c => c.Boolean(nullable: false));
            AddColumn("dbo.TransactionSet", "IsFullyComplete", c => c.Boolean(nullable: false));
            AddColumn("dbo.TransactionSet", "IsRejected", c => c.Boolean(nullable: false));
            AddColumn("dbo.TransactionSet", "Latitude", c => c.Double(nullable: false));
            AddColumn("dbo.TransactionSet", "Longitude", c => c.Double(nullable: false));
            AddColumn("dbo.TransactionSet", "Job_JobId", c => c.Guid());
            AddColumn("dbo.TransactionSet", "JobSet_JobSetId", c => c.Guid());
            AddColumn("dbo.CompanySubscription", "TransactionLimit", c => c.Int(nullable: false));
            AddColumn("dbo.CompanySubscription", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.CompanySubscription", "NumberOfUsers", c => c.Int(nullable: false));
            AddColumn("dbo.CompanySubscription", "BillingCycleType", c => c.Int(nullable: false));
            AddColumn("dbo.UserProfile", "IsLicensed", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserProfile", "APIKey", c => c.String());
            AddColumn("dbo.UserProfile", "Secret", c => c.String());
            AddColumn("dbo.CompanyRelationship", "CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.CompanyRelationship", "CreatedUser", c => c.String());
            AddColumn("dbo.CompanyRelationship", "UpdatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.CompanyRelationship", "UpdatedUser", c => c.String());
            AddColumn("dbo.CompanyRelationship", "Version", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.CompanyRelationship", "IsDeleted", c => c.Boolean(nullable: false));
            CreateIndex("dbo.TransactionSet", "QuestionSet_QuestionSetId");
            CreateIndex("dbo.TransactionSet", "Job_JobId");
            CreateIndex("dbo.TransactionSet", "JobSet_JobSetId");
            CreateIndex("dbo.Company", "SubscriptionType_SubscriptionTypeId");
            CreateIndex("dbo.ControlList", "Company_CompanyId");
            CreateIndex("dbo.ControlListItem", "ControlList_ControlListId");
            CreateIndex("dbo.QuestionSet", "Company_CompanyId");
            CreateIndex("dbo.Question", "ControlList_ControlListId");
            CreateIndex("dbo.Question", "ControlType_ControlTypeId");
            CreateIndex("dbo.Question", "QuestionSet_QuestionSetId");
            CreateIndex("dbo.Transaction", "Question_QuestionId");
            CreateIndex("dbo.Transaction", "TransactionSet_TransactionSetId");
            CreateIndex("dbo.UserProfile", "Company_CompanyId");
            CreateIndex("dbo.UserProfile", "Language_LanguageId");
            CreateIndex("dbo.DashboardItem", "UserProfile_UserId");
            CreateIndex("dbo.ForgottenPasswordRequest", "UserProfile_UserId");
            CreateIndex("dbo.ControlTypeLocal", "QcControlType_ControlTypeId");
            CreateIndex("dbo.MenuItem", "QcMenu_MenuId");
            CreateIndex("dbo.webpages_UsersInRoles", "UserId");
            CreateIndex("dbo.webpages_UsersInRoles", "RoleId");
            AddForeignKey("dbo.TransactionSet", "Job_JobId", "dbo.Job", "JobId");
            AddForeignKey("dbo.TransactionSet", "JobSet_JobSetId", "dbo.JobSet", "JobSetId");
            DropColumn("dbo.TransactionSet", "IsComplete");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TransactionSet", "IsComplete", c => c.Boolean(nullable: false));
            DropForeignKey("dbo.TransactionSetNote", "TransactionSet_TransactionSetId", "dbo.TransactionSet");
            DropForeignKey("dbo.TransactionSet", "JobSet_JobSetId", "dbo.JobSet");
            DropForeignKey("dbo.TransactionSet", "Job_JobId", "dbo.Job");
            DropForeignKey("dbo.Job", "JobSet_JobSetId", "dbo.JobSet");
            DropForeignKey("dbo.JobSet", "Company_CompanyId", "dbo.Company");
            DropForeignKey("dbo.UserSession", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.ResultViewShare", "ResultView_ResultViewId", "dbo.ResultView");
            DropForeignKey("dbo.ResultViewItem", "ResultView_ResultViewId", "dbo.ResultView");
            DropForeignKey("dbo.ResultViewItem", "Question_QuestionId", "dbo.Question");
            DropForeignKey("dbo.ResultViewDisplayItem", "ResultView_ResultViewId", "dbo.ResultView");
            DropForeignKey("dbo.ResultViewDisplayItem", "Question_QuestionId", "dbo.Question");
            DropForeignKey("dbo.ResultView", "QuestionSet_QuestionSetId", "dbo.QuestionSet");
            DropForeignKey("dbo.ResultViewShare", "Company_CompanyId", "dbo.Company");
            DropForeignKey("dbo.QuestionRule", "FromQuestion_QuestionId", "dbo.Question");
            DropForeignKey("dbo.QuestionLocal", "Question_QuestionId", "dbo.Question");
            DropForeignKey("dbo.QuestionLocal", "Language_LanguageId", "dbo.Language");
            DropForeignKey("dbo.QuestionSetJobSet", "JobSet_JobSetId", "dbo.JobSet");
            DropForeignKey("dbo.QuestionSetJobSet", "QuestionSet_QuestionSetId", "dbo.QuestionSet");
            DropForeignKey("dbo.ApprovalRule", "QuestionSet_QuestionSetId", "dbo.QuestionSet");
            DropForeignKey("dbo.InvoiceLine", "Invoice_InvoiceId", "dbo.Invoice");
            DropForeignKey("dbo.Invoice", "CompanySubscription_SubscriptionTypeId", "dbo.CompanySubscription");
            DropForeignKey("dbo.Invoice", "Company_CompanyId", "dbo.Company");
            DropForeignKey("dbo.ApprovalStep", "TransactionSet_TransactionSetId", "dbo.TransactionSet");
            DropForeignKey("dbo.ApprovalStep", "ApprovalRule_ApprovalRuleId", "dbo.ApprovalRule");
            DropIndex("dbo.webpages_UsersInRoles", new[] { "RoleId" });
            DropIndex("dbo.webpages_UsersInRoles", new[] { "UserId" });
            DropIndex("dbo.QuestionSetJobSet", new[] { "JobSet_JobSetId" });
            DropIndex("dbo.QuestionSetJobSet", new[] { "QuestionSet_QuestionSetId" });
            DropIndex("dbo.MenuItem", new[] { "QcMenu_MenuId" });
            DropIndex("dbo.ControlTypeLocal", new[] { "QcControlType_ControlTypeId" });
            DropIndex("dbo.TransactionSetNote", new[] { "TransactionSet_TransactionSetId" });
            DropIndex("dbo.UserSession", new[] { "UserProfile_UserId" });
            DropIndex("dbo.ForgottenPasswordRequest", new[] { "UserProfile_UserId" });
            DropIndex("dbo.DashboardItem", new[] { "UserProfile_UserId" });
            DropIndex("dbo.UserProfile", new[] { "Language_LanguageId" });
            DropIndex("dbo.UserProfile", new[] { "Company_CompanyId" });
            DropIndex("dbo.ResultViewItem", new[] { "ResultView_ResultViewId" });
            DropIndex("dbo.ResultViewItem", new[] { "Question_QuestionId" });
            DropIndex("dbo.ResultViewDisplayItem", new[] { "ResultView_ResultViewId" });
            DropIndex("dbo.ResultViewDisplayItem", new[] { "Question_QuestionId" });
            DropIndex("dbo.ResultView", new[] { "QuestionSet_QuestionSetId" });
            DropIndex("dbo.ResultViewShare", new[] { "ResultView_ResultViewId" });
            DropIndex("dbo.ResultViewShare", new[] { "Company_CompanyId" });
            DropIndex("dbo.Transaction", new[] { "TransactionSet_TransactionSetId" });
            DropIndex("dbo.Transaction", new[] { "Question_QuestionId" });
            DropIndex("dbo.QuestionRule", new[] { "FromQuestion_QuestionId" });
            DropIndex("dbo.QuestionLocal", new[] { "Question_QuestionId" });
            DropIndex("dbo.QuestionLocal", new[] { "Language_LanguageId" });
            DropIndex("dbo.Question", new[] { "QuestionSet_QuestionSetId" });
            DropIndex("dbo.Question", new[] { "ControlType_ControlTypeId" });
            DropIndex("dbo.Question", new[] { "ControlList_ControlListId" });
            DropIndex("dbo.QuestionSet", new[] { "Company_CompanyId" });
            DropIndex("dbo.InvoiceLine", new[] { "Invoice_InvoiceId" });
            DropIndex("dbo.Invoice", new[] { "CompanySubscription_SubscriptionTypeId" });
            DropIndex("dbo.Invoice", new[] { "Company_CompanyId" });
            DropIndex("dbo.ControlListItem", new[] { "ControlList_ControlListId" });
            DropIndex("dbo.ControlList", new[] { "Company_CompanyId" });
            DropIndex("dbo.Company", new[] { "SubscriptionType_SubscriptionTypeId" });
            DropIndex("dbo.JobSet", new[] { "Company_CompanyId" });
            DropIndex("dbo.Job", new[] { "JobSet_JobSetId" });
            DropIndex("dbo.TransactionSet", new[] { "JobSet_JobSetId" });
            DropIndex("dbo.TransactionSet", new[] { "Job_JobId" });
            DropIndex("dbo.TransactionSet", new[] { "QuestionSet_QuestionSetId" });
            DropIndex("dbo.ApprovalStep", new[] { "TransactionSet_TransactionSetId" });
            DropIndex("dbo.ApprovalStep", new[] { "ApprovalRule_ApprovalRuleId" });
            DropIndex("dbo.ApprovalRule", new[] { "QuestionSet_QuestionSetId" });
            DropColumn("dbo.CompanyRelationship", "IsDeleted");
            DropColumn("dbo.CompanyRelationship", "Version");
            DropColumn("dbo.CompanyRelationship", "UpdatedUser");
            DropColumn("dbo.CompanyRelationship", "UpdatedDate");
            DropColumn("dbo.CompanyRelationship", "CreatedUser");
            DropColumn("dbo.CompanyRelationship", "CreatedDate");
            DropColumn("dbo.UserProfile", "Secret");
            DropColumn("dbo.UserProfile", "APIKey");
            DropColumn("dbo.UserProfile", "IsLicensed");
            DropColumn("dbo.CompanySubscription", "BillingCycleType");
            DropColumn("dbo.CompanySubscription", "NumberOfUsers");
            DropColumn("dbo.CompanySubscription", "Price");
            DropColumn("dbo.CompanySubscription", "TransactionLimit");
            DropColumn("dbo.TransactionSet", "JobSet_JobSetId");
            DropColumn("dbo.TransactionSet", "Job_JobId");
            DropColumn("dbo.TransactionSet", "Longitude");
            DropColumn("dbo.TransactionSet", "Latitude");
            DropColumn("dbo.TransactionSet", "IsRejected");
            DropColumn("dbo.TransactionSet", "IsFullyComplete");
            DropColumn("dbo.TransactionSet", "IsEntryComplete");
            DropColumn("dbo.TransactionSet", "TransactionsSerialised");
            DropColumn("dbo.ControlType", "AllowControlList");
            DropColumn("dbo.QuestionSet", "TrackLocation");
            DropColumn("dbo.Company", "NumberOfUsers");
            DropColumn("dbo.Company", "IsInPublicDirectory");
            DropColumn("dbo.Company", "IsSubscriptionActive");
            DropColumn("dbo.Company", "PaymentSubscriptionToken");
            DropTable("dbo.QuestionSetJobSet");
            DropTable("dbo.DocumentType");
            DropTable("dbo.TransactionSetNote");
            DropTable("dbo.UserSession");
            DropTable("dbo.ResultViewItem");
            DropTable("dbo.ResultViewDisplayItem");
            DropTable("dbo.ResultView");
            DropTable("dbo.ResultViewShare");
            DropTable("dbo.QuestionRule");
            DropTable("dbo.QuestionLocal");
            DropTable("dbo.InvoiceLine");
            DropTable("dbo.Invoice");
            DropTable("dbo.JobSet");
            DropTable("dbo.Job");
            DropTable("dbo.ApprovalStep");
            DropTable("dbo.ApprovalRule");
            DropTable("dbo.ApplicationLog");
        }
    }
}
