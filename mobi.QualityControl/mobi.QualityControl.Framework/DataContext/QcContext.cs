﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Mobi.QualityControl.Framework.Model;
using EntityState = System.Data.Entity.EntityState;


namespace Mobi.QualityControl.Framework.DataContext
{
    public class QcContext : DbContext
    {
        static QcContext()
        {
            Database.SetInitializer(new QcDatabaseInitialiser());
        }

        public QcContext(): base("QcConnection")
        {
            
        }

        public override int SaveChanges()
        {
            ApplyAudit();
            return base.SaveChanges();
        }

       

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Entity<UserProfile>().HasMany<Role>(r => r.Roles).WithMany(u => u.UserProfiles).Map(m =>
            {
                m.ToTable("webpages_UsersInRoles");
                m.MapLeftKey("UserId");
                m.MapRightKey("RoleId");
            });
        }

        public void ApplyAudit()
        {
            foreach (var entry in ChangeTracker.Entries()
                                      .Where(
                                          e => e.Entity is IQcAudit &&
                                               (e.State == EntityState.Added) ||
                                               (e.State == EntityState.Modified)))
            {
                try
                {
                    var e = (IQcAudit) entry.Entity;

                    if (entry.State == EntityState.Added)
                    {
                        e.CreatedDate = DateTime.UtcNow;
                    }

                    e.UpdatedDate = DateTime.UtcNow;
                }
                catch (Exception)
                {
                    
                }
            }
        }

       //Companies
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyRelationshipRequest> CompanyRelationshipRequests { get; set; }
        public DbSet<CompanyRelationship> CompanyRelationships { get; set; }
        public DbSet<CompanySubscription> CompanySubscriptions { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceLine> InvoiceLines { get; set; }
      

        //Questions
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionRule> QuestionRules { get; set; }
        public DbSet<QuestionSet> QuestionSets { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<ApprovalRule> ApprovalRules { get; set; }
        public DbSet<ApprovalStep> ApprovalSteps { get; set; }
        public DbSet<TransactionSet> TransactionSets { get; set; }
        public DbSet<TransactionSetNote> TransactionSetNotes { get; set; }
        public DbSet<ResultView> ResultViews { get; set; }
        public DbSet<ResultViewItem> ResultViewItems { get; set; }
        public DbSet<ResultViewDisplayItem> ResultViewDisplayItems { get; set; }
        public DbSet<ResultViewShare> ResultViewShares { get; set; }
        public DbSet<JobSet> JobSets { get; set; }
        public DbSet<Job> Jobs { get; set; }


        //ControlType
        public DbSet<ControlType> ControlTypes { get; set; }
        public DbSet<ControlTypeLocal> ControlTypeLocals { get; set; }
        public DbSet<ControlList> ControlLists { get; set; }
        public DbSet<ControlListItem> ControlListItems { get; set; }


        //Users
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<MenuItem> MenuItems { get; set; }
        public DbSet<DashboardItem> DashboardItems { get; set; }
        public DbSet<UserSession> UserSessions { get; set; }
        public DbSet<ForgottenPasswordRequest> ForgottenPasswordRequests {get; set;}
         

        //Notifications
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<InternalMessage> InternalMessages { get; set; }

        //Languages
        public DbSet<Language> Languages { get; set; }
        public DbSet<QuestionLocal> QuestionLocals { get; set; }


        public DbSet<Role> Role { get; set; }
        public DbSet<Membership> Membership { get; set; }

        public DbSet<ApplicationLog> ApplicationLogs { get; set; }


        //Reporting
        public DbSet<DocumentType> DocumentTypes { get; set; }
      
    }
}
