﻿using System.Data.Entity;
using Mobi.QualityControl.Framework.Seed;

namespace Mobi.QualityControl.Framework.DataContext
{
    public class QcDatabaseInitialiser :
       // CreateDatabaseIfNotExists<QcContext>      // when model is stable
        DropCreateDatabaseIfModelChanges<QcContext>  // when iterating
    {
        protected override void Seed(QcContext context)
        {
            SeedData(context);
            base.Seed(context);
        }

        public void SeedData(QcContext context)
        {
            #region Controls
            SeedControls.CreateStandardControls(context);
           
            #endregion

            #region language
            SeedLanguage.CreateLanguages(context);
            #endregion


            #region Menu

            SeedMenu.CreateStandardMenu(context);
            #endregion

            #region Companies
           
            SeedCompany.CreateStandardCompany(context);
            SeedCompany.AddSeedUserToCompany(context);
            
            #endregion

            #region Question sets

           Framework.Seed.SeedQuestion.CreateQuestionSetsForSeedCompany(context);
                Framework.Seed.SeedQuestion.CreateQuestionSetsForLpCompany(context);
                Framework.Seed.SeedQuestion.AddQuestionsToQuestionSet(context);
            #endregion

                #region Dashboards
            SeedDashboardItems.CreateDashboardItems(context);
                #endregion
                SeedControlList.CreateControlLists(context);
            context.SaveChanges();


        }
    }


}
