﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Enum
{
    public enum NotificationType
    {
        Register = 1,
        ChangePAssword = 2,
        RelationshipRequest =3,
        RelationshipResponse =4,
        ApprovalRequired = 5

    }
}
