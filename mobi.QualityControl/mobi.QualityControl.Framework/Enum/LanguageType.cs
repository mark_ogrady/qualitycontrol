﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Enum
{
    public static class LanguageType
    {

        public static Guid English = Guid.Parse("7DF57FE3-57F1-4330-A363-68095D2D46BF");
        public static Guid Polish = Guid.Parse("F58DEAED-F28D-4DAA-870A-ACE1CE2E7503");
        public static Guid French = Guid.Parse("9E94F572-7505-404F-A265-4E35269A461A");
    }
}
