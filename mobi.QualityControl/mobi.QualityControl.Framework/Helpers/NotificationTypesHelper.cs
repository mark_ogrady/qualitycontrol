﻿using System;
using Mobi.QualityControl.Framework.Enum;

namespace Mobi.QualityControl.Framework.Helpers
{
    public class NotificationTypesHelper
    {
        //TODO dd different types of notifications in here
        public void RegisterMessage(int userId)
        {
            var subject = "Welcome to Quality Control mobi";
            var body = "Welcome";
            new NotificationHelper().SendNotification(userId, 0, true, subject, body, NotificationType.Register);

        }

        public void ForgottenMessage(Guid requestId, int userId)
        {
            var subject = "Password Reset";
            var body = "Welcome";
            new NotificationHelper().SendNotification(userId, 0, true, subject, body, NotificationType.Register);

        }
    }
}