﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pechkin;

namespace Mobi.QualityControl.Framework.Helpers
{
    public class ReportHelper
    {
        public void CreateReport()
        {
            var css = @"";
            var overallHtml = "";

            using (StreamReader sr = new StreamReader(@"C:\Users\Mark\Documents\GitHub\qualitycontrol\mobi.QualityControl\mobi.QualityControl.Framework\Temp\Html1.txt"))
            {
                overallHtml = sr.ReadToEnd();
                
            }
            byte[] pdfBuf = new SimplePechkin(new GlobalConfig()).Convert(overallHtml);

            File.WriteAllBytes(@"c:\temp\marktest"+ Guid.NewGuid().ToString() +".pdf",pdfBuf);
        }


    }
}
