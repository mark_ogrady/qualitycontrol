﻿using System;
using System.Configuration;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Mobi.QualityControl.Framework.Helpers
{
    public class ImageHelper
    {


        public string StoreListImage(HttpPostedFileBase image, Guid listItemId)
        {
            return StoreImage("listitems", image, listItemId);
        }


        public string StoreCompanyLogoImage(HttpPostedFileBase image, Guid companyId)
        {

            return StoreImage("companylogos", image, companyId);

        }

        public string StoreTransactionImage(HttpPostedFileBase image, Guid transactionId)
        {
            return StoreImage("transaction", image, transactionId);
        }

        public string StoreImage(String imageType, HttpPostedFileBase file, Guid transactionId)
        {
            if (file.ContentLength > 0)
            {
                var filename = Path.GetFileName(file.FileName);

                System.Drawing.Image sourceimage = System.Drawing.Image.FromStream(file.InputStream);


                var compressedImage = ImageUtilities.ResizeImage(sourceimage, 200, 200);
                var storageConnectionString = ConfigurationManager.AppSettings["StorageConnectionString"];

                var storageAccount = CloudStorageAccount.Parse(storageConnectionString);
                var blobStorage = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobStorage.GetContainerReference(imageType.ToLower());
                if (container.CreateIfNotExists())
                {
                    // configure container for public access
                    var permissions = container.GetPermissions();
                    permissions.PublicAccess = BlobContainerPublicAccessType.Container;
                    container.SetPermissions(permissions);
                }


                string uniqueBlobName = string.Format("{0}/image_{1}{2}", imageType.ToLower(), transactionId.ToString(),
                    Path.GetExtension(file.FileName));
                CloudBlockBlob blob = container.GetBlockBlobReference(uniqueBlobName);
                blob.Properties.ContentType = file.ContentType;
                var stream = new System.IO.MemoryStream();
                compressedImage.Save(stream, ImageFormat.Jpeg);
                stream.Position = 0;
                blob.UploadFromStream(stream);
                stream.Dispose();

                return blob.Uri.ToString();
            }
            return null;
        }



        public HttpPostedFileBase CompressImage(HttpPostedFileBase file, int height, int width)
        {
            return file;
        }

    }
}