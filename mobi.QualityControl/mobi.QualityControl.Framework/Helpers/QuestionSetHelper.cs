﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.ViewModel.Transaction;


namespace Mobi.QualityControl.Framework.Helpers
{
    public class QuestionSetHelper
    {


        public bool DoesQuestionSetExist(Guid questionSetId, Guid companyId)
        {

            using (var context = new QcContext())
            {
                var questionSetQuery =
                    context.QuestionSets.FirstOrDefault(
                        o => o.QuestionSetId == questionSetId && o.Company.CompanyId == companyId);
                if (questionSetQuery != null)
                {
                    return true;
                }
            }
            
            return false;
        }
    

    public List<QuestionListItem> GetQuestionList(Guid id, Guid? transactionSetId, Guid languageId)
        {
            var model = new List<QuestionListItem>();
            using (var context = new QcContext())
            {
                
                var questionSetQuery = context.QuestionSets.FirstOrDefault(o => o.QuestionSetId == id);
                if (questionSetQuery != null)
                {
                    foreach (var question in questionSetQuery.Questions.Where(o => !o.IsDeleted || !o.IsHidden).OrderBy(o => o.OrderPosition).ToList())
                    {

                        var result = "";
                        var transaction = context.Transactions.FirstOrDefault(o => o.Question.QuestionId == question.QuestionId && o.TransactionSet.TransactionSetId == transactionSetId);
                        if (transaction != null)
                        {

                            result = transaction.Result;
                        }

                        var shortQuestionText = new LanaguageHelper().GetQuestionText(question.QuestionId, languageId).PadRight(20).Substring(0, 18);


                        model.Add(new QuestionListItem()
                        {
                            QuestionId = question.QuestionId,
                            QuestionText = new LanaguageHelper().GetQuestionText(question.QuestionId, languageId),
                            ShortQuestionText = shortQuestionText,
                            IsMandatory = question.IsMandatory,
                            OrderPosition = question.OrderPosition,
                            Complete = !String.IsNullOrEmpty(result),
                            Result = result

                        });
                    }

                }
            }
            return model;


        }
    }
}