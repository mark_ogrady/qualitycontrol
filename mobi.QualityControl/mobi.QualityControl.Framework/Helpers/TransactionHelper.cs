﻿using System;
using System.Linq;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Framework.ViewModel.Transaction;

namespace Mobi.QualityControl.Framework.Helpers
{
    public class TransactionHelper
    {
        public TransactionSet CreateTransactionSet(Guid questionSetId, Guid? transactionSetId = null)
        {
            var transactionSet = new TransactionSet();
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommitted
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;
            //create the transaction scope, passing our options in
            using (var transactionScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {

                using (var context = new QcContext())
                {
                    //Create Transaction
                    transactionSet.TransactionSetId = transactionSetId == null ? Guid.NewGuid() : (Guid) transactionSetId;

                    transactionSet.QuestionSet = context.QuestionSets.FirstOrDefault(o => o.QuestionSetId == questionSetId);
                    context.TransactionSets.Add(transactionSet);

                    //Create Transactions for each question
                    foreach (var question in transactionSet.QuestionSet.Questions.ToList())
                    {
                        var transaction = new Transaction();
                        transaction.TransactionId = Guid.NewGuid();
                        transaction.Result = question.DefaultValue;
                        transaction.Question = context.Questions.FirstOrDefault(o => o.QuestionId == question.QuestionId);
                        transactionSet.Transactions.Add(transaction);
                    }
                    //Create ApprovalSteps
                    foreach (var approvalRule in transactionSet.QuestionSet.ApprovalRules.Where(o => !o.IsDeleted).OrderBy(o => o.OrderPosition))
                    {
                        var approvalStep = new ApprovalStep();
                        approvalStep.ApprovalStepId = Guid.NewGuid();
                        approvalStep.UserId = approvalRule.UserId;
                        approvalStep.OrderPosition = approvalRule.OrderPosition;
                        transactionSet.ApprovalSteps.Add(approvalStep);
                    }

                    context.SaveChanges();
                    transactionScope.Complete();

                }

            }
            return transactionSet;
        }

        public TransactionItemModel SaveAndNextQuestion(TransactionItemModel model, Guid languageId)
        {
            using (var context = new QcContext())
            {
                TransactionSet transactionSet;
                
                if (model.TransactionSetId == Guid.Empty)
                {
                    transactionSet = new TransactionHelper().CreateTransactionSet(model.QuestionSetId);
                    model.TransactionSetId = transactionSet.TransactionSetId;

                }
                else
                {
                    //Find Transaction
                    transactionSet = context.TransactionSets.FirstOrDefault(o => o.TransactionSetId == model.TransactionSetId);
                }
                //Save Transaction

                Guid transactionId;
                var transactionQuery = context.Transactions.FirstOrDefault(o => o.Question.QuestionId == model.QuestionId && o.TransactionSet.TransactionSetId == model.TransactionSetId);
                if (transactionQuery != null)
                {
                    if (transactionQuery.Question.ControlType.ControlName == "Checkbox")
                    {
                        model.Result = model.Result == null ? "false" : "true";
                    }
                    transactionId = transactionQuery.TransactionId;
                    transactionQuery.Result = model.Result;
                }
                else
                {
                    var transaction = new Transaction();
                    transaction.TransactionId = Guid.NewGuid();
                    transaction.Question = context.Questions.FirstOrDefault(o => o.QuestionId == model.QuestionId);
                    transaction.Result = model.Result;
                    transactionSet.Transactions.Add(transaction);
                    transactionId = transaction.TransactionId;
                }
                var transactionSetId = model.TransactionSetId;
                var questionSetId = model.QuestionSetId;

                context.SaveChanges();

                //If final then display finish
                //Next Question
               

                model = new TransactionHelper().GetNextQuestion(transactionId, languageId);
            }
            return model;
        }


        public TransactionItemModel GetNextQuestion(Guid previousTranactionId, Guid languageId)
        {
            TransactionItemModel model = null;

            using (var context = new QcContext())
            {

                //Get the question from the transaction.
                var prevTrans = context.Transactions.Select(o => new
                {
                    o.TransactionId,
                    o.TransactionSet.TransactionSetId,
                    o.Question.QuestionId,
                    o.Question.QuestionSet.QuestionSetId,
                    o.Question.OrderPosition
                }).FirstOrDefault(o => o.TransactionId == previousTranactionId);
                if (prevTrans != null)
                {
                    var orderPos = prevTrans.OrderPosition;
                    while (model == null)
                    {
                        //Find the next question in the queue
                        var nextQuestion = context.Questions.OrderBy(o => o.OrderPosition).FirstOrDefault(o => o.QuestionSet.QuestionSetId == prevTrans.QuestionSetId && o.OrderPosition > orderPos);
                        if (nextQuestion != null)
                        {
                            if (nextQuestion.QuestionRules.Count > 0)
                            {
                                var ruleCount = nextQuestion.QuestionRules.Count;
                                var rulesPassed = 0;
                                //Check the rules find it matches the rules for the transaction Set
                                foreach (var questionRule in nextQuestion.QuestionRules)
                                {
                                    //get question transaction result
                                    var fromTransaction = context.Transactions.FirstOrDefault(o => o.Question.QuestionId == questionRule.FromQuestion.QuestionId && o.TransactionSet.TransactionSetId == prevTrans.TransactionSetId);
                                    if (fromTransaction != null)
                                    {
                                        if (fromTransaction.Result == questionRule.EvaluationValue)
                                        {
                                            rulesPassed++;
                                            if (ruleCount == rulesPassed)
                                            {
                                                model = new TransactionItemModel();
                                                model.QuestionId = nextQuestion.QuestionId;
                                                model.QuestionText = new LanaguageHelper().GetQuestionText(nextQuestion.QuestionId, languageId);
                                                model.SequenceNo = nextQuestion.OrderPosition;
                                                model.TransactionSetId = prevTrans.TransactionSetId;
                                                model.ControlType = nextQuestion.ControlType.ControlName;
                                                model.Result = nextQuestion.DefaultValue;
                                                model.QuestionsList = new QuestionSetHelper().GetQuestionList(model.QuestionSetId, model.TransactionSetId, languageId);

                                                var nextTransactionQuery = context.Transactions.FirstOrDefault(o => o.Question.QuestionId == nextQuestion.QuestionId && o.TransactionSet.TransactionSetId == model.TransactionSetId);
                                                if (nextTransactionQuery != null)
                                                {
                                                    model.Result = nextTransactionQuery.Result;
                                                }


                                                if (nextQuestion.ControlList != null)
                                                    model.ControlListItems = new DropDownHelper().GetControlList(nextQuestion.ControlList.ControlListId);
                                            }
                                        }
                                    }
                                }
                                orderPos++;

                                //Find next question until it matches
                            }

                            else
                            {
                                model = new TransactionItemModel();
                                model.QuestionId = nextQuestion.QuestionId;
                                model.QuestionText = new LanaguageHelper().GetQuestionText(nextQuestion.QuestionId, languageId);
                                model.SequenceNo = nextQuestion.OrderPosition;
                                model.TransactionSetId = prevTrans.TransactionSetId;
                                model.ControlType = nextQuestion.ControlType.ControlName;
                                model.Result = nextQuestion.DefaultValue;
                                model.QuestionsList = new QuestionSetHelper().GetQuestionList(model.QuestionSetId, model.TransactionSetId, languageId);

                                var nextTransactionQuery = context.Transactions.FirstOrDefault(o => o.Question.QuestionId == nextQuestion.QuestionId && o.TransactionSet.TransactionSetId == model.TransactionSetId);
                                if (nextTransactionQuery != null)
                                {
                                    model.Result = nextTransactionQuery.Result;
                                }


                                if (nextQuestion.ControlList != null)
                                    model.ControlListItems = new DropDownHelper().GetControlList(nextQuestion.ControlList.ControlListId);
                            }

                        }
                        else
                        {
                            break;
                        }
                    }
                }
                return model;

            }

        }

        public void DeleteTransactionSet(Guid transactionSetId)
        {
            using (var context = new QcContext())
            {
                var transactionSet = context.TransactionSets.FirstOrDefault(o => o.TransactionSetId == transactionSetId);

                if (transactionSet != null)
                {
                    foreach (var transaction in transactionSet.Transactions.ToList())
                    {
                        context.Transactions.Remove(transaction);
                    }
                    context.TransactionSets.Remove(transactionSet);
                    context.SaveChanges();
                }
            }

        }

        
    }
}