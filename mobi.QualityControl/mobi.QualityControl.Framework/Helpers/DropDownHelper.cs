﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Enum;
using Mobi.QualityControl.Framework.ViewModel.Transaction;

namespace Mobi.QualityControl.Framework.Helpers
{
    public class DropDownHelper
    {

        /// <summary>
        /// Languages
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetLanguaguesDropdown()
        {
            var modelList = new List<SelectListItem>();
            using (var context = new QcContext())
            {
                var languagesQuery = context.Languages.Where(l => !l.IsDeleted).Select(l => new
                {
                    l.LanguageId,
                    l.LanguageName
                });

                foreach (var language in languagesQuery)
                {
                    modelList.Add(new SelectListItem()
                    {

                        Text = language.LanguageName,
                        Value = language.LanguageId.ToString()
                    });
                }

            }
            return modelList;
        }


        /// <summary>
        /// Languages
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetLanguaguesForQuestionDropdown(Guid questionId)
        {
            var modelList = new List<SelectListItem>();
            using (var context = new QcContext())
            {
                var languagesQuery = context.Languages.Where(l => !l.IsDeleted).Select(l => new
                {
                    l.LanguageId,
                    l.LanguageName
                });

                var existingLanguage =
                    context.QuestionLocals.Where(o => o.Question.QuestionId == questionId)
                        .Select(o => o.Language.LanguageId).ToList();

                foreach (var language in languagesQuery.ToList())
                {
                    if (!existingLanguage.Contains(language.LanguageId))
                    {
                        modelList.Add(new SelectListItem()
                        {

                            Text = language.LanguageName,
                            Value = language.LanguageId.ToString()
                        });
                    }
                }


            }
            return modelList;
        }

        /// <summary>
        /// Languages
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetQuestionFromDropdown(Guid questionId)
        {
            var modelList = new List<SelectListItem>();
            using (var context = new QcContext())
            {
                var question = context.Questions.Where(q => q.QuestionId == questionId).Select(q => new
                {
                    q.QuestionId,
                    q.QuestionSet.QuestionSetId,
                    Questions = q.QuestionSet.Questions.Select(o => new
                    {
                        o.QuestionId,
                        o.QuestionText
                    }).Where(o => o.QuestionId != questionId).ToList()

                }).FirstOrDefault();

                if (question != null)

                foreach (var questionfrom in question.Questions.ToList())
                {
                   
                        modelList.Add(new SelectListItem()
                        {

                            Text = questionfrom.QuestionText,
                            Value = questionfrom.QuestionId.ToString()
                        });
                    
                }


            }
            return modelList;
        }

        /// <summary>
        /// Control List
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetControlTypeDropdown()
        {
            var modelList = new List<SelectListItem>();
            using (var context = new QcContext())
            {
                var controlTypeQuery = from c in context.ControlTypes
                                       where !c.IsDeleted
                                       select new
                                       {
                                           c.ControlTypeId,
                                           c.ControlName
                                       };

                foreach (var controlType in controlTypeQuery)
                {
                    modelList.Add(new SelectListItem()
                    {

                        Text = controlType.ControlName,
                        Value = controlType.ControlTypeId.ToString()
                    });
                }

            }
            return modelList;
        }

        /// <summary>
        /// Control List
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetControlListDropdown(Guid companyId)
        {
            var modelList = new List<SelectListItem>();
            using (var context = new QcContext())
            {
                var controlTypeQuery = from c in context.ControlLists
                                       where !c.IsDeleted &&
                                       c.Company.CompanyId == companyId
                                       select new
                                       {
                                           c.ControlListId,
                                           c.ControlListName
                                       };

                foreach (var controlType in controlTypeQuery)
                {
                    modelList.Add(new SelectListItem()
                    {

                        Text = controlType.ControlListName,
                        Value = controlType.ControlListId.ToString()
                    });
                }

            }
            return modelList;
        }

      


        public List<SelectListItem> GetQuestionSetDropDown(Guid companyId)
        {
            var modelList = new List<SelectListItem>();
            using (var context = new QcContext())
            {
                var questionSetQuery = from qs in context.QuestionSets
                                       where !qs.IsDeleted &&
                                       qs.Company.CompanyId == companyId
                                       select new
                                       {
                                           qs.QuestionSetId,
                                           qs.QuestionSetName
                                       };

                foreach (var controlType in questionSetQuery)
                {
                    modelList.Add(new SelectListItem()
                    {

                        Text = controlType.QuestionSetName,
                        Value = controlType.QuestionSetId.ToString()
                    });
                }

            }
            return modelList;
        }

        public List<SelectListItem> GetQuestionDropDown(Guid companyId, Guid questionSetId)
        {
            var modelList = new List<SelectListItem>();
            using (var context = new QcContext())
            {
                var questionQuery = context.Questions.Where(q => !q.IsDeleted &&
                                                                 q.QuestionSet.QuestionSetId == questionSetId &&
                                                                 q.QuestionSet.Company.CompanyId == companyId).Select(q => new
                                                                 {
                                                                     q.QuestionId,
                                                                     q.QuestionText
                                                                 });

                foreach (var question in questionQuery)
                {
                    modelList.Add(new SelectListItem()
                    {

                        Text = question.QuestionText,
                        Value = question.QuestionId.ToString()
                    });
                }

            }
            return modelList;
        }

        public List<SelectListItem> GetUserListDropDown(Guid companyId)
        {
            var modelList = new List<SelectListItem>();
            using (var context = new QcContext())
            {
                var userList = context.UserProfiles.Select(o => new
                {
                    o.Company.CompanyId,
                    o.UserId,
                    UserActualName = o.FirstName + " "+ o.LastName
                }).Where(o => o.CompanyId == companyId);

                foreach (var user in userList)
                {
                    modelList.Add(new SelectListItem()
                    {
                        Text =  user.UserActualName,
                        Value = user.UserId.ToString()
                    });
                }
            }
            return modelList;
        }
 

        public List<SelectListItem> GetSubscriptionTypeDropDown()
        {
            var modelList = new List<SelectListItem>();
            using (var context = new QcContext())
            {
                var subscriptionQuery = from s in context.CompanySubscriptions
                                        where !s.IsDeleted

                                        select new
                                        {
                                            s.SubscriptionTypeId,
                                            s.Name
                                        };

                foreach (var question in subscriptionQuery)
                {
                    modelList.Add(new SelectListItem()
                    {

                        Text = question.Name,
                        Value = question.SubscriptionTypeId.ToString()
                    });
                }

            }
            return modelList;
        }

        public List<SelectListItem> GetViewEvalutionDropDown()
        {
            var modelList = new List<SelectListItem>();


            foreach (var evalutionType in System.Enum.GetValues(typeof(ViewEvalutionType)))
            {
                modelList.Add(new SelectListItem()
                {
                    Text = evalutionType.ToString(),
                    Value = evalutionType.ToString()
                });
            }
            return modelList;
        }

        public List<SelectListItem> GetDashboardTypeDropDown()
        {
            var modelList = new List<SelectListItem>();


            foreach (var dashboardType in System.Enum.GetValues(typeof(DashboardType)))
            {
                modelList.Add(new SelectListItem()
                {
                    Text = dashboardType.ToString(),
                    Value = dashboardType.ToString()
                });
            }
            return modelList;
        }

        public List<SelectListItem> GetAllRelationshipCompaniesDropDown(Guid companyId)
        {
            var modelList = new List<SelectListItem>();
            using (var context = new QcContext())
            {
                var companyRelationships = from cr in context.CompanyRelationships
                                        where !cr.IsDeleted &&
                                        cr.CompanyFromId == companyId
                                        select new
                                        {
                                            cr.CompanyToId,
                                            
                                        };

                foreach (var companyRelationship in companyRelationships)
                {
                    modelList.Add(new SelectListItem()
                    {

                        Text = context.Companies.FirstOrDefault(o => o.CompanyId == companyRelationship.CompanyToId).CompanyName,
                        Value = companyRelationship.CompanyToId.ToString()
                    });
                }

            }
            return modelList;

        }

        public List<SelectListItem> GetAllRelationshipResultCompaniesDropDown(Guid companyId, Guid resultViewId)
        {
            var modelList = new List<SelectListItem>();
            using (var context = new QcContext())
            {
                var companyRelationships = from cr in context.CompanyRelationships
                                        where !cr.IsDeleted &&
                                        cr.CompanyFromId == companyId
                                        select new
                                        {
                                            cr.CompanyToId,
                                            
                                        };
                var resultViewShare = context.ResultViewShares.Where(o => o.ResultView.ResultViewId == resultViewId).Select(o => o.Company.CompanyId) ;

                foreach (var companyRelationship in companyRelationships.ToList())
                {
                    if (!resultViewShare.Contains(companyRelationship.CompanyToId))
                    {

                        modelList.Add(new SelectListItem()
                        {
                            Text = context.Companies.FirstOrDefault(o => o.CompanyId == companyRelationship.CompanyToId).CompanyName,
                            Value = companyRelationship.CompanyToId.ToString()
                        });
                    }
                }

            }
            return modelList;

        }

        public List<ControlListItemVm> GetControlList(Guid id)
        {
            var list = new List<ControlListItemVm>();
            using (var context = new QcContext())
            {
               
                var controlListQuery = context.ControlListItems.Where(o => o.ControlList.ControlListId == id);

                foreach (var controlListItem in controlListQuery)
                {
                    list.Add(new ControlListItemVm()
                    {
                        ImageUrl = controlListItem.ImageUrl,
                        Name = controlListItem.ControlValue,
                        Value = controlListItem.ControlListItemId.ToString()
                    });
                }
            }
            return list;
        }
    }
}