﻿using System;
using System.Linq;
using Mobi.QualityControl.Framework.DataContext;

namespace Mobi.QualityControl.Framework.Helpers
{
    public class LanaguageHelper
    {
        public string GetQuestionText(Guid questionId, Guid languageId)
        {
            using (var context = new QcContext())
            {
                var questionTextQuery =
                    context.QuestionLocals.FirstOrDefault(
                        o => o.Question.QuestionId == questionId && o.Language.LanguageId == languageId);

                if (questionTextQuery != null)
                {
                    return questionTextQuery.QuestionText;
                }

                var questionQuery = context.Questions.FirstOrDefault(o => o.QuestionId == questionId);
                if (questionQuery != null)
                    return questionQuery.QuestionText;


            }
            return null;
        }
    }
}