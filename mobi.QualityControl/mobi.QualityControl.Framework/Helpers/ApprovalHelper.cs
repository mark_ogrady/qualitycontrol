﻿using System;
using System.Linq;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Enum;
using Mobi.QualityControl.Framework.Model;

namespace Mobi.QualityControl.Framework.Helpers
{
    public class ApprovalHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="approvalStepId"></param>
        /// <returns>nev</returns>
        public bool AcceptStep(Guid approvalStepId)
        {
            using (var context = new QcContext())
            {
                //approval step set to true
                var approvalStep = context.ApprovalSteps.FirstOrDefault(o => o.ApprovalStepId == approvalStepId);
                if (approvalStep != null)
                {
                    approvalStep.AcceptedDate = DateTime.UtcNow;
                    //get next approval step
                    var nextApprovalStep = approvalStep.TransactionSet.ApprovalSteps.OrderBy(o => o.OrderPosition).FirstOrDefault(o => o.OrderPosition > approvalStep.OrderPosition);

                    // if final approval set transaction to IsComplete true
                    if (nextApprovalStep == null)
                    {
                        approvalStep.CurrentStep = false;
                        nextApprovalStep.CurrentStep = true;
                        var transactionSet = context.TransactionSets.FirstOrDefault(o => o.TransactionSetId == approvalStep.TransactionSet.TransactionSetId);
                        if (transactionSet != null)
                        {
                            transactionSet.IsFullyComplete = true;
                            context.SaveChanges();
                            return true;
                        }

                    }
                    else
                    {
                        //Generate Notfication
                        new NotificationHelper().SendNotification(nextApprovalStep.UserId, 1, true, "Approval Required", "There is an outstanding approval required", NotificationType.ApprovalRequired);
                    }
                   
                }
                context.SaveChanges();
            }
            return false;
        }

        public bool RejectStep(Guid approvalStepId, string message)
        {
            using (var context = new QcContext())
            {
                //set approval step to rejected
                 var approvalStep = context.ApprovalSteps.FirstOrDefault(o => o.ApprovalStepId == approvalStepId);
                if (approvalStep != null)
                {
                    approvalStep.RejectedDate = DateTime.UtcNow;

                     var transactionSet = context.TransactionSets.FirstOrDefault(o => o.TransactionSetId == approvalStep.TransactionSet.TransactionSetId);
                    if (transactionSet != null)
                    {
                        if (!String.IsNullOrEmpty(message))
                        {
                            //add transaction set note
                            var transactionSetNote = new TransactionSetNote();
                            transactionSetNote.TransactionSetNoteId = Guid.NewGuid();
                            transactionSetNote.Note = message;
                            transactionSet.TransactionSetNotes.Add(transactionSetNote);
                        }
                        //set transaction to be rejected
                        transactionSet.IsRejected = true;
                       
                    }
                }
                context.SaveChanges();
            }
            return true;
        }

    }
}