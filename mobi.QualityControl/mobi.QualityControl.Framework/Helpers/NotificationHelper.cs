﻿using System;
using System.Linq;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Enum;

namespace Mobi.QualityControl.Framework.Helpers
{
    public class NotificationHelper
    {


        public bool SendNotification(int toUser,int fromUser, bool sendEmail, string subject, string body, NotificationType notificationType)
        {
           return SendNotification(toUser,fromUser, "edmund@stedmundsday.com", sendEmail, subject, body, notificationType);

        }

        public bool SendNotification(int toUser, int fromUser, string fromEmail, bool sendEmail, string subject, string body, NotificationType notificationType )
        {
            using (var context = new QcContext())
            {

                var user = context.UserProfiles.FirstOrDefault(o => o.UserId == toUser);
                if (user != null)
                {
                    var notification = new Framework.Model.Notification();
                    notification.NotificationId = Guid.NewGuid();
                    notification.ToId = user.UserId;
                    notification.FromId = fromUser;
                    notification.MessageBody = body;
                    notification.Subject = subject;
                    notification.NotificationType = notificationType;
                    context.Notifications.Add(notification);

                    if (sendEmail)
                    {
                        new Framework.Mail.Gmail().SendMail(user.UserName, fromEmail,subject,body);
                    }
                }
                else
                {
                    return false;
                }

            }
            return true;
        }
    }
}