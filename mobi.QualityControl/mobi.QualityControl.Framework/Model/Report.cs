﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class Report
    {
        public Guid ReportId { get; set; }
        public string MainReport { get; set; }
        public string ReportRepeat1 { get; set; }
        public string ReportRepeat2 { get; set; }
        public string ReportRepeat3 { get; set; }
        public string ReportTotal { get; set; }

    }
}
