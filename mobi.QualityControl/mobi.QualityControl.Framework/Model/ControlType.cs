﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Mobi.QualityControl.Framework.Model
{
    public class ControlType :IQcAudit
    {
        [Key]
        public Guid ControlTypeId { get; set; }
        [Required]
        public String ControlName { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public bool AllowDefaultValue { get; set; }
        public bool AllowMinValue { get; set; }
        public bool  AllowMaxValue { get; set; }
        public bool AllowRegex { get; set; }
        public string DefaultValue { get; set; }
        public bool AllowControlList { get; set; }

        public int OrderPosition { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
