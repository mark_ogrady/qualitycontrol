﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class ControlList : IQcAudit
    {
        public Guid ControlListId { get; set; }
        public string ControlListName { get; set; }
        public virtual Company Company { get; set; }
        public virtual ICollection<ControlListItem> ControlListItems { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }

        public ControlList()
        {
            ControlListItems = new Collection<ControlListItem>();
        }
    }
}
