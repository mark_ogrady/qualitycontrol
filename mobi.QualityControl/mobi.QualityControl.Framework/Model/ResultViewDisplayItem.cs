﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class ResultViewDisplayItem
    {
        public Guid ResultViewDisplayItemId { get; set; }
        public virtual Question Question { get; set; }
        public virtual ResultView ResultView { get; set; }
    }
}
