﻿namespace Mobi.QualityControl.Framework.Model
{
    public static class Version
    {
        public static decimal WebAppVersion = 0;
        public static decimal MobileVersion = 1;
        public static decimal AndroidVersion = 0;
        public static decimal IphoneVersion = 0;
    }
}
