﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class Job : IQcAudit
    {
        public Job()
        {
            TransactionSets = new Collection<TransactionSet>();
        }

        [Key]
        public Guid JobId { get; set; }

        public string Jobcode { get; set; }
        public JobSet JobSet { get; set; }
        public ICollection<TransactionSet> TransactionSets { get; set; }


        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
