﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mobi.QualityControl.Framework.Enum;

namespace Mobi.QualityControl.Framework.Model
{
    public class Notification :IQcAudit
    {
        [Key]
        public Guid NotificationId { get; set; }

        public int FromId { get; set; }
        public int ToId { get; set; }
        public virtual NotificationType NotificationType { get; set; }
        public string Subject { get; set; }
        public string MessageBody { get; set; }
        public bool Dismissed { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
