﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Mobi.QualityControl.Framework.Model
{
    public class JobSet :IQcAudit
    {
        [Key]
        public Guid JobSetId { get; set; }
        public string JobSetName { get; set; }

        public virtual Company Company { get; set; }
        public virtual ICollection<QuestionSet> QuestionSets { get; set; }
        public virtual ICollection<Job>  Jobs { get; set; }

        public JobSet()
        {
            QuestionSets = new Collection<QuestionSet>();
            Jobs = new Collection<Job>();
        }

        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
