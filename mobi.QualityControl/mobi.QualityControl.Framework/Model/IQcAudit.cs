﻿using System;
using System.Data;

namespace Mobi.QualityControl.Framework.Model
{
    public interface IQcAudit
    {
        DateTime CreatedDate { get; set; }
        string CreatedUser { get; set; }
        DateTime UpdatedDate { get; set; }
        string UpdatedUser { get; set; }
        decimal Version { get; set; }
        bool IsDeleted { get; set; }
        

    }
}
