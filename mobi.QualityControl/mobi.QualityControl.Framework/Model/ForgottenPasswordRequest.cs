﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class ForgottenPasswordRequest 
    {
        public Guid ForgottenPasswordRequestId { get; set; }
        public string UserName { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public string IpAddress { get; set; }
        public bool IsProcessed { get; set; }

       

       
    }
}
