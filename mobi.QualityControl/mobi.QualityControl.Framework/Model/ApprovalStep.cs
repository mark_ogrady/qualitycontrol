﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class ApprovalStep :IQcAudit
    {
        [Key]
        public Guid ApprovalStepId { get; set; }
        public int OrderPosition { get; set; }
        public bool CurrentStep { get; set; }
        public DateTime? AcceptedDate { get; set; }
        public DateTime? RejectedDate { get; set; }
        public int UserId { get; set; }
        public virtual ApprovalRule ApprovalRule { get; set; }

        public virtual TransactionSet TransactionSet { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
