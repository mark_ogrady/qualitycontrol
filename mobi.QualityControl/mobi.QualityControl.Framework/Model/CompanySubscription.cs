﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.AccessControl;
using Mobi.QualityControl.Framework.Enum;

namespace Mobi.QualityControl.Framework.Model
{
    public class CompanySubscription : IQcAudit
    {
        [Key]
        public Guid SubscriptionTypeId { get; set; }
        public string Name { get; set; }
        public int TransactionLimit { get; set; }
        public decimal Price { get; set; }
        public int NumberOfUsers { get; set; }
        public BillingCycleType BillingCycleType { get; set; }
        public virtual ICollection<Company> Companies { get; set; }
        public virtual ICollection<Invoice> CompanyPayments { get; set; }

        public DateTime CreatedDate{ get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
