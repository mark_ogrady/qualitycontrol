﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class QuestionLocal
    {
        [Key]
        public Guid QuestionLocalId { get; set; }

        public string QuestionText { get; set; }
       


        public virtual Language Language { get; set; }
        public virtual Question Question { get; set; }
    }
}
