﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class Invoice : IQcAudit
    {
        public Invoice()
        {
            InvoiceLines = new Collection<InvoiceLine>();
        }

        public Guid InvoiceId { get; set; }
        public virtual Company Company { get; set; }
        public virtual CompanySubscription CompanySubscription { get; set; }
        public string PaymentProvider { get; set; }
        public string PaymentProviderCode { get; set; }
        public string  OrderNo { get; set; }
        public string CustomerOrderNo { get; set; }
        public virtual ICollection<InvoiceLine> InvoiceLines { get; set; } 

        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
