﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class UserSession 
    {
        public Guid UserSessionId { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public string DeviceId { get; set; }
        public DateTime ExpirationDate { get; set; }
            
            public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        
        public bool IsDeleted { get; set; }
    }
}
