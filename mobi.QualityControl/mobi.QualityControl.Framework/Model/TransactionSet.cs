﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobi.QualityControl.Framework.Model
{
    public class TransactionSet : IQcAudit
    {
       

        [Key]
        public Guid  TransactionSetId { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string TransactionsSerialised { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
        public virtual ICollection<TransactionSetNote> TransactionSetNotes { get; set; }
        public virtual ICollection<ApprovalStep> ApprovalSteps { get; set; }
        public virtual JobSet JobSet { get; set; }
        public virtual Job Job { get; set; }
        public virtual QuestionSet QuestionSet { get; set; }
        public bool IsEntryComplete { get; set; }
        public bool IsFullyComplete { get; set; }
        public bool IsRejected { get; set; }
        public double Latitude { get; set; }
        public double  Longitude { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }



        public TransactionSet()
        {
            Transactions = new Collection<Transaction>();
            TransactionSetNotes = new Collection<TransactionSetNote>();
            ApprovalSteps = new Collection<ApprovalStep>();
        }
    }
}
