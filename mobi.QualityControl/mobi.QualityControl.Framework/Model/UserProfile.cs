﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.Win32.SafeHandles;

namespace Mobi.QualityControl.Framework.Model
{
    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [DataType(DataType.EmailAddress)]
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsLicensed { get; set; }
        public virtual Company Company { get; set; }
        public virtual Language Language { get; set; }
        public virtual ICollection<DashboardItem> DashboardItems { get; set; } 
        public ICollection<Role> Roles { get; set; }
        public ICollection<ForgottenPasswordRequest> ForgottenPasswordRequests { get; set; }
        public string APIKey { get; set; }
        public string Secret { get; set; }
        public ICollection<UserSession> UserSessions { get; set; }
    }
}
