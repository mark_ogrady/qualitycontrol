﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class ControlListItem :IQcAudit
    {
        public Guid ControlListItemId { get; set; }
        public string ControlIdValue { get; set; }
        public string ControlValue { get; set; }
        public string ImageUrl { get; set; }

        //Links
        public virtual ControlList ControlList { get; set; }
        
        
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
