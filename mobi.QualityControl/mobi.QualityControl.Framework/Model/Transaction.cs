﻿using System;
using System.ComponentModel.DataAnnotations;
using Mobi.QualityControl.Framework.Enum;

namespace Mobi.QualityControl.Framework.Model
{
    public  class Transaction :IQcAudit
    {
        

        [Key]
        public Guid TransactionId { get; set; }
        public virtual Question Question { get; set; }
        public string Result { get; set; }
        public TransactionStatus TransactionStatus { get; set; }

        public virtual TransactionSet TransactionSet { get; set; }
        
        
        
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
