﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security;

namespace Mobi.QualityControl.Framework.Model
{
    public class Question : IQcAudit
    {
        [Key]
        public Guid QuestionId { get; set; }
        [Required]
        public string QuestionText { get; set; }
        public virtual ControlType ControlType { get; set; }
        public virtual QuestionSet QuestionSet { get; set; }
        public int OrderPosition { get; set; }
        
        public string DefaultValue { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsHidden { get; set; }
       
        public decimal MaxValue { get; set; }
        public decimal MinValue { get; set; }
        public string Regex { get; set; }
        public string Instructions { get; set; }
        
        //Links
        public virtual ICollection<Transaction> Transactions { get; set; }
        public virtual ControlList ControlList { get; set; }
        public virtual ICollection<QuestionLocal> QuestionLocals { get; set; } 
        public virtual ICollection<QuestionRule> QuestionRules  { get; set; } 
        

        //Audit
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
