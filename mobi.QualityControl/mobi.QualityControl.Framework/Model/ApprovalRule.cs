﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class ApprovalRule :IQcAudit 
    {
        [Key]
        public Guid ApprovalRuleId { get; set; }
        public int UserId { get; set; }
        public int  OrderPosition { get; set; }
        public virtual ICollection<ApprovalStep> ApprovalSteps { get; set; }
        public virtual QuestionSet QuestionSet { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }

        public ApprovalRule()
        {
            ApprovalSteps = new Collection<ApprovalStep>();
        }
    }
}
