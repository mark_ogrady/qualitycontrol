﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class InternalMessage
    {
        [Key]
        public Guid InternalMessageId { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
        public bool  Dismissed { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
