﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class Language : IQcAudit
    {
        public Guid LanguageId { get; set; }
        public string LanguageName { get; set; }
        public string LanguageCulture { get; set; }
        public bool Rtl { get; set; }
        public string ImageUrl { get; set; }
        public virtual ICollection<QuestionLocal> QuestionLocals { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
