﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Mobi.QualityControl.Framework.Model
{
    public class ControlTypeLocal
    {
        [Key]
        public Guid ControlTypeLocalId{get;set;}

        public virtual ControlType QcControlType { get; set; }
        public string Language { get; set; }
        public string Localisation { get; set; }
        public string ControlName { get; set; }
        public string Description { get; set; }
    }
}
