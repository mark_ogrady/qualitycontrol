﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mobi.QualityControl.Framework.Enum;

namespace Mobi.QualityControl.Framework.Model
{
    public class ResultViewItem :IQcAudit
    {
        public Guid ResultViewItemId { get; set; }
        public virtual Question Question { get; set; }
        public virtual ResultView ResultView { get; set; }
        public ViewEvalutionType ViewEvalutionType { get; set; }
        public string ExpressionValue { get; set; }


        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
