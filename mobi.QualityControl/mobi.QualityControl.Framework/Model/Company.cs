﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mobi.QualityControl.Framework.Model
{
    public class Company : IQcAudit
    {
        [Key]
        public Guid CompanyId { get; set; }
        [StringLength(50)]
        [DisplayName("Company Name")]
        public string CompanyName { get; set; }
        public string CompanyImageUrl { get; set; }
        [DataType(DataType.Url)]
        public string CompanyWebsiteUrl { get; set; }
        public string CompanyAddress1 { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyAddress3 { get; set; }
        public string CompanyAddress4 { get; set; }
        public string PostCode { get; set; }
        public string ContactNumber { get; set; }
        public string PaymentSubscriptionToken { get; set; }
        public virtual CompanySubscription SubscriptionType { get; set; }
        public bool IsSubscriptionActive { get; set; }
        public bool IsInPublicDirectory { get; set; }
        public int NumberOfUsers { get; set; }
        //Links
        public virtual ICollection<QuestionSet> QuestionSets { get; set; }
        public virtual ICollection<ControlList> ControlLists { get; set; }
        public virtual ICollection<UserProfile> UserProfiles { get; set; }
        public virtual ICollection<ResultViewShare> ResultViewShares { get; set; }
        public virtual ICollection<Invoice> Invoices { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }

        public Company()
        {
            UserProfiles = new Collection<UserProfile>();
            QuestionSets = new Collection<QuestionSet>();
            ControlLists = new Collection<ControlList>();
            ResultViewShares = new Collection<ResultViewShare>();
            Invoices = new Collection<Invoice>();
        }
    }
}
