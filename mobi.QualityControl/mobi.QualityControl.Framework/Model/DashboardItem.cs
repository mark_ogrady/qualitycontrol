﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mobi.QualityControl.Framework.Enum;

namespace Mobi.QualityControl.Framework.Model
{
    public class DashboardItem :IQcAudit
    {
        public Guid DashboardItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DashboardType DashboardType { get; set; }
        public int OrderPosition { get; set; }
        public Guid EntityId { get; set; }

        public virtual UserProfile UserProfile { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
