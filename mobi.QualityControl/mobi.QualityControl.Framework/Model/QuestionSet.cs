﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Mobi.QualityControl.Framework.Model
{
    public class QuestionSet : IQcAudit
    {
        public QuestionSet()
        {
            Questions = new Collection<Question>();
            TransactionSets = new Collection<TransactionSet>();
            ApprovalRules = new Collection<ApprovalRule>();
        }


        [Key]
        public Guid QuestionSetId { get; set; }
        public string QuestionSetName { get; set; }
        public virtual Company Company { get; set; }
        public bool TrackLocation { get; set; }
        //Links
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<TransactionSet> TransactionSets { get; set; }
        public virtual ICollection<ApprovalRule> ApprovalRules { get; set; }
        public virtual ICollection<JobSet> JobSets { get; set; }

        //Audit
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
