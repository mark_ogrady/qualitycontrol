﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class ResultView :IQcAudit
    {
        public Guid ResultViewId { get; set; }
        public string ResultViewName { get; set; }
        public virtual QuestionSet QuestionSet { get; set; }
        public virtual ICollection<ResultViewItem> ResultViewItems { get; set; }
        public virtual ICollection<ResultViewDisplayItem> ResultViewDisplayItems { get; set; }
        public virtual ICollection<ResultViewShare> ResultViewShares { get; set; }


        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }

        public ResultView()
        {
            ResultViewItems = new Collection<ResultViewItem>();
            ResultViewDisplayItems = new Collection<ResultViewDisplayItem>();
        }
    }
}
