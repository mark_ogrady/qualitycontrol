﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Mobi.QualityControl.Framework.Model
{
    public class MenuItem
    {
        [Key]
        public Guid MenuItemId { get; set; }

        public string Icon { get; set; }
        public string ItemName { get; set; }
        public string Link { get; set; }
        public int OrderPosition { get; set; }

        public virtual Menu QcMenu { get; set; }
       
    }
}
