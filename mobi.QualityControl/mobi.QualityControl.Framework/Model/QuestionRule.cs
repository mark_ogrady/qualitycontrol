﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mobi.QualityControl.Framework.Enum;

namespace Mobi.QualityControl.Framework.Model
{
    public class QuestionRule :IQcAudit
    {
        public Guid QuestionRuleId { get; set; }       
        public virtual Question FromQuestion { get; set; }
        public ViewEvalutionType ViewEvalutionType { get; set; }
        public string EvaluationValue { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
