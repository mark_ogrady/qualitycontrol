﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Framework.Model
{
    public class InvoiceLine : IQcAudit
    {
        public Guid InvoiceLineId { get; set; }
        public virtual Invoice Invoice { get; set; }
        public string LineDescription { get; set; }
        public decimal NetValue { get; set; }
        public decimal VatRate { get; set; }
        public decimal VatValue { get; set; }
        public decimal GrossValue { get; set; }
        public int OrderPosition { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
        public bool IsDeleted { get; set; }
    }
}
