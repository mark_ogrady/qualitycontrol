﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Mobi.QualityControl.Framework.ViewModel.Transaction
{
    public class TransactionItemModel
    {
        public Guid TransactionSetId { get; set; }
        public Guid QuestionSetId { get; set; }
        public Guid QuestionId { get; set; }
        public string QuestionText { get; set; }
        public string ControlType { get; set; }
        public string Result { get; set; }
        public int SequenceNo { get; set; }
        public List<ControlListItemVm> ControlListItems { get; set; }
        public List<QuestionListItem> QuestionsList { get; set; }
        public bool IsFirst { get; set; }
        public HttpPostedFileBase CameraImage { get; set; }
        public bool TrackLocation { get; set; }
       

        public TransactionItemModel()
        {
            ControlListItems = new List<ControlListItemVm>();
            QuestionsList = new List<QuestionListItem>();
        }



    }
    public class QuestionListItem
    {
        public Guid QuestionId { get; set; }
        public bool Complete { get; set; }
        public int OrderPosition { get; set; }
        public string QuestionText { get; set; }
        public string ShortQuestionText { get; set; }
        public bool IsMandatory { get; set; }
        
        public string Result { get; set; }
    }

    public class ControlListItemVm
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string ImageUrl { get; set; }
    }
}