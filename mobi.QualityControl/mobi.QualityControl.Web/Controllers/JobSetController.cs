﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Helpers;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Web.Models;
using Mobi.QualityControl.Web.ViewModels.JobSet;

namespace Mobi.QualityControl.Web.Controllers
{
    public class JobSetController : AuthenticatedController
    {

        private QcContext _context = new QcContext();
        //
        // GET: /JobSet/
        public ActionResult Index()
        {
            var model = new JobSetIndexModel();
            var companyId = AuthenticatedUser.Company.CompanyId;
            var jobsetQuery = _context.JobSets.Select(o => new {o.JobSetId, o.JobSetName, o.Company.CompanyId, o.IsDeleted}).Where(o => o.CompanyId == companyId && !o.IsDeleted);

            foreach (var jobset in jobsetQuery)
            {
                model.JobSetIndexItems.Add(new JobSetIndexItem()
                {
                    JobSetId = jobset.JobSetId,
                    JobSetName = jobset.JobSetName
                });
            }


            return View(model);
        }

        public ActionResult Create()
        {
            var model = new JobSetEditModel();
            model.ScreenType = ScreenType.Create;
            return View("Edit", model);
        }

        public ActionResult Details(Guid id)
        {
            var model = new JobSetEditModel();
            model.ScreenType = ScreenType.Details;
            model.JobSetId = id;
            PopulateEditModel(model);
            return View("Edit", model);
        }

        public ActionResult Edit(Guid id)
        {
            var model = new JobSetEditModel();
            model.ScreenType = ScreenType.Edit;
            model.JobSetId = id;
            PopulateEditModel(model);
            return View("Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(JobSetEditModel model)
        {
            if (model.ScreenType == ScreenType.Create)
            {
                var existingQuestionSet = _context.JobSets.Select(o => new
                {
                    o.JobSetId,
                    o.Company.CompanyId,
                    o.JobSetName
                }).FirstOrDefault(o => o.CompanyId == this.AuthenticatedUser.Company.CompanyId && o.JobSetName == model.JobSetName);
                if (existingQuestionSet != null)
                {
                    ModelState.AddModelError("", "Job Set Already Exists");
                }
            }
            else
            {
                var existingQuestionSet = _context.JobSets.Select(o => new
                {
                    o.JobSetId,
                    o.Company.CompanyId,
                    o.JobSetName
                }).FirstOrDefault(o => o.CompanyId == this.AuthenticatedUser.Company.CompanyId && o.JobSetName == model.JobSetName && o.JobSetId != model.JobSetId);
                if (existingQuestionSet != null)
                {
                    ModelState.AddModelError("", "Job Set Already Exists");
                }
            }

              var jobSetId = model.JobSetId;
            if (ModelState.IsValid)
            {
                JobSet jobSet;
                if (model.ScreenType == ScreenType.Create)
                {
                    jobSet = new JobSet();
                    jobSet.JobSetId = Guid.NewGuid();
                    jobSetId = jobSet.JobSetId;
                }
                else
                {
                    jobSet = _context.JobSets.FirstOrDefault(o => o.JobSetId == model.JobSetId && o.Company.CompanyId == this.AuthenticatedUser.Company.CompanyId);
                }
                jobSet.JobSetName = model.JobSetName;
               
                if (model.ScreenType == ScreenType.Create)
                {
                    jobSet.Company = _context.Companies.FirstOrDefault(o => o.CompanyId == AuthenticatedUser.Company.CompanyId);
                    _context.JobSets.Add(jobSet);
                }
                _context.SaveChanges();
                TempData["SuccessMessage"] = "The Job Set has been successfully updated.";
                return RedirectToAction("Edit", new { id = jobSetId });
            }
            model.QuestionSetListItems = new DropDownHelper().GetQuestionSetDropDown(AuthenticatedUser.Company.CompanyId);
            return View("Edit", model);
        }

        private void PopulateEditModel(JobSetEditModel model)
        {
            var jobsetQuery = _context.JobSets.Select(o => new
            {
                o.JobSetId,
                o.JobSetName,
                QuestionSets = o.QuestionSets.Select(q => new
                {
                    q.QuestionSetId,
                    q.QuestionSetName
                }),
                Jobs = o.Jobs.Select(j => new
                {
                    j.JobId,
                    j.Jobcode
                })
            }).FirstOrDefault(o => o.JobSetId == model.JobSetId);
            if (jobsetQuery != null)
            {
                model.JobSetName = jobsetQuery.JobSetName;

                foreach (var questionSet in jobsetQuery.QuestionSets)
                {
                    model.JobSetQuestionSets.Add(new JobSetQuestionSet()
                    {
                        QuestionSetId = questionSet.QuestionSetId,
                        QuestionSetName = questionSet.QuestionSetName
                    });
                }

                foreach (var job in jobsetQuery.Jobs)
                {
                    model.JobSetJobsItemModels.Add(new JobSetJobsItemModel()
                    {
                        JobId = job.JobId,
                        JobReference = job.Jobcode
                    });
                }
            }
            model.QuestionSetListItems = new DropDownHelper().GetQuestionSetDropDown(AuthenticatedUser.Company.CompanyId);
        }

        [HttpPost]
        public ActionResult AddQuestionSet(AddQuestionSetModel model)
        {
            var jobSet = _context.JobSets.FirstOrDefault(o => o.JobSetId == model.JobSetId);
            if (jobSet != null)
            {
                var questionSet = _context.QuestionSets.FirstOrDefault(o => o.QuestionSetId == model.QuestionSetId);
                if (questionSet != null)
                {
                    jobSet.QuestionSets.Add(questionSet);
                }

                TempData["SuccessMessage"] = "The question set has been successfully created.";
                _context.SaveChanges();

            }
            return RedirectToAction("Edit", "JobSet", new {id = model.JobSetId});
        }

        public ActionResult StartJob(Guid id)
        {
            var model = new StartJobModel();
            model.JobSetId = id;
            return View(model);
        }

        [HttpPost]
        public ActionResult StartJob(StartJobModel model)
        {
            //get jobset 
            var jobsetQuery = _context.JobSets.FirstOrDefault(o => o.JobSetId == model.JobSetId);
            var job = new Job();
            job.JobId = Guid.NewGuid();
            job.Jobcode = model.JobReference;
            jobsetQuery.Jobs.Add(job);
            _context.SaveChanges();
            
            //loop through each question set
            foreach (var questionset in jobsetQuery.QuestionSets)
            {
               var transSet =  new TransactionHelper().CreateTransactionSet(questionset.QuestionSetId);
                transSet.Job.JobId = job.JobId;
                _context.SaveChanges();
            }
            _context.SaveChanges();
           
            return View(model);
        }


        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }
	}
}