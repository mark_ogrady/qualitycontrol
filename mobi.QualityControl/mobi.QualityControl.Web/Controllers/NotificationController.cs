﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Web.ViewModels.Notification;

namespace Mobi.QualityControl.Web.Controllers
{
    public class NotificationController : AuthenticatedController
    {
        ILog log = log4net.LogManager.GetLogger(typeof(NotificationController));

        private QcContext context = new QcContext();

        //
        // GET: /Notification/

        public ActionResult Index()
        {
            var model = new NotificationIndexModel();
            var notificationQuery = context.Notifications.Where(o => o.ToId == AuthenticatedUser.UserId && o.Dismissed == false);
            foreach (var notification in notificationQuery)
            {
                model.NotificationIndexItems.Add(new NotificationIndexItem()
                {
                NotificiationId = notification.NotificationId,
                Subject = notification.Subject,
                    MessageBody = notification.MessageBody,
                    IsDismissed = false
                });
            }

            return View(model);
        }

        public ActionResult ViewArchived()
        {
            var model = new NotificationIndexModel();

            return View("index",model);
        }

        protected override void Dispose(bool disposing)
        {
            context.Dispose();
            base.Dispose(disposing);
        }
    }
}
