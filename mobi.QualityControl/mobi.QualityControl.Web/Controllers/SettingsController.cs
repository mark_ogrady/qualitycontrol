﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;

namespace Mobi.QualityControl.Web.Controllers
{
    public class SettingsController : Controller
    {
        ILog log = log4net.LogManager.GetLogger(typeof(SettingsController));

        //
        // GET: /Settings/
        public ActionResult Index()
        {
            return RedirectToAction("ManageProfile","User");
        }
	}
}