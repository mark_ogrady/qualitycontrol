﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Mobi.QualityControl.Web.ViewModels.Payment;
using Stripe;

namespace Mobi.QualityControl.Web.Controllers
{
    public class PaymentController : Controller
    {
        //
        // GET: /Payment/
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Process(PaymentProcessModel model)
        {
            var charge = new StripeChargeCreateOptions();
            
            charge.Amount = 200;
            charge.Currency = "gbp";
            charge.Description = "Test";
            charge.TokenId = model.StripeToken;
           
            var chargeService = new StripeChargeService();
           
            StripeCharge stripeCharge = chargeService.Create(charge);

            return View();
        }
	}
}