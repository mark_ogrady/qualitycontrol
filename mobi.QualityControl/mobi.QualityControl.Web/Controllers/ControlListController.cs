﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Helpers;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Web.Models;
using Mobi.QualityControl.Web.ViewModels.ControlList;
using OfficeOpenXml;

namespace Mobi.QualityControl.Web.Controllers
{
    [Authorize]
    public class ControlListController : AuthenticatedController
    {

        ILog _log = LogManager.GetLogger(typeof(ControlListController));

        //
        // GET: /ControlList/
        private QcContext _context = new QcContext();

        public ActionResult Index()
        {
            var model = new ControlListIndexModel();

            var controlListQuery = _context.ControlLists.Select(o => new
            {
                o.IsDeleted,
                o.ControlListId,
                o.ControlListName,
               ListCount =  o.ControlListItems.Count
            }).Where(o => !o.IsDeleted);
            foreach (var controlList in controlListQuery.ToList())
            {
                model.ControlListIndexItems.Add(new ControlListIndexItem
                {
                    ControlListId = controlList.ControlListId,
                    ControlListName = controlList.ControlListName,
                    ItemCount = controlList.ListCount
                });
            }
            return View(model);
        }

        

        public ActionResult Details(Guid id)
        {
            var model = new ControlListEditModel();
            model.ScreenType = ScreenType.Details;
            PopulateEditModel(id, model);
            return View("Edit", model);
        }


        public ActionResult Create()
        {
            var model = new ControlListEditModel();
            model.ScreenType = ScreenType.Create;
            return View("Edit", model);
        }

        public ActionResult Edit(Guid id)
        {
            var model = new ControlListEditModel();
            model.ScreenType = ScreenType.Edit;
            PopulateEditModel(id, model);

            return View(model);
        }

        [HttpPost]
        public ActionResult ImportNew(ControlListImportModel model)
        {
            var excelFile = HttpContext.Request.Files[0];
            if (excelFile != null)
            {
                var excelPackage = new ExcelPackage(excelFile.InputStream);
                var importItems = new List<ImportListItem>();
                var worksheet = excelPackage.Workbook.Worksheets[1];
                var rowCount = worksheet.Dimension.End.Row + 1;

                var controlList = _context.ControlLists.FirstOrDefault(o => o.ControlListId == model.ControlListId);

                for (int i = 1; i < rowCount; i++)
                {

                    if (worksheet.Cells[i, 1].Value != null)
                        importItems.Add(new ImportListItem
                        {
                   
                            Value = worksheet.Cells[i,1].Value.ToString()
                        });
                }

                foreach (var importListItem in importItems)
                {
                    if (controlList != null)
                    {
                        var existingItem = controlList.ControlListItems.FirstOrDefault(o => o.ControlValue == importListItem.Value);
                        if (existingItem == null)
                        {
                            var controlListItem = new ControlListItem();
                            controlListItem.ControlListItemId = Guid.NewGuid();
                            controlListItem.ControlValue = importListItem.Value;
                            controlList.ControlListItems.Add(controlListItem);
                        }
                    }
                }
            }

            _context.SaveChanges();
            TempData["SuccessMessage"] = "Item Saved";

            return RedirectToAction("Edit", new {id = model.ControlListId});
        }

        private void PopulateEditModel(Guid id, ControlListEditModel model)
        {
            var controlList = _context.ControlLists.Select(o => new
            {
                o.ControlListId,
                o.Company.CompanyId,
                o.ControlListName,
                o.IsDeleted,
                ControlListItems = o.ControlListItems.Select(i => new
                {
                    i.ImageUrl,
                    i.ControlListItemId,
                    i.ControlValue,
                    i.IsDeleted
                })
            }).FirstOrDefault(o => o.ControlListId == id
                                                              && o.CompanyId == AuthenticatedUser.Company.CompanyId
                                                                    && !o.IsDeleted);

            if (controlList != null)
            {
                model.ControlListId = controlList.ControlListId;
                model.ControlListName = controlList.ControlListName;
                foreach (var controlListItem in controlList.ControlListItems.Where(o => !o.IsDeleted))
                {
                    if (controlListItem.ImageUrl != null)
                        model.HasImages = true;
                    model.ControlListModelItems.Add(new ControlListModelItem
                    {
                        ControlListItemId = controlListItem.ControlListItemId,
                        ItemName = controlListItem.ControlValue,
                        ImageUrl = controlListItem.ImageUrl
                    });
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ControlListEditModel model)
        {
            var controlListId = model.ControlListId;
            if (model.ScreenType == ScreenType.Create)
            {
                var existingControlList = _context.ControlLists.FirstOrDefault(o => o.Company.CompanyId == AuthenticatedUser.Company.CompanyId && o.ControlListName == model.ControlListName);
                if (existingControlList != null)
                {
                    ModelState.AddModelError("", "Control List Name Already Exists");
                }

            }
            else
            {
                var existingControlList = _context.ControlLists.FirstOrDefault(o => o.Company.CompanyId == AuthenticatedUser.Company.CompanyId && o.ControlListName == model.ControlListName && o.ControlListId != model.ControlListId);
                if (existingControlList != null)
                {
                    ModelState.AddModelError("", "Control List Name Already Exists");
                }
            }

            if (ModelState.IsValid)
            {
                ControlList controlList;
                if (model.ScreenType == ScreenType.Create)
                {

                    controlList = new ControlList();
                    controlList.ControlListId = Guid.NewGuid();
                    controlListId = controlList.ControlListId;
                }
                else
                {
                    controlList = _context.ControlLists.FirstOrDefault(o => o.ControlListId == model.ControlListId && o.Company.CompanyId == AuthenticatedUser.Company.CompanyId);
                }
                if (controlList != null)
                {
                    controlList.ControlListName = model.ControlListName;

                    if (model.ScreenType == ScreenType.Create)
                    {
                        controlList.Company = _context.Companies.FirstOrDefault(o => o.CompanyId == AuthenticatedUser.Company.CompanyId);
                        _context.ControlLists.Add(controlList);

                    }
                }
                _context.SaveChanges();
                TempData["SuccessMessage"] = "The Control List has been successfully updated.";
                return RedirectToAction("Edit", new { id = controlListId });
            }
            return View(model);
        }

        public ActionResult Delete(Guid id)
        {

            var controlList = _context.ControlLists.FirstOrDefault(o => o.ControlListId == id);
            if (controlList != null)
            {
                controlList.IsDeleted = true;
                controlList.UpdatedDate = DateTime.UtcNow;
                controlList.UpdatedUser = AuthenticatedUser.UserName;
                _context.SaveChanges();
                TempData["SuccessMessage"] = "The ControlList Set " + controlList.ControlListName +
                                             " has successfully been removed";
            }
            else
            {
                TempData["ErrorMessage"] = "There was an issue removing Control List " + controlList.ControlListName;
            }
            return RedirectToAction("Index");
        }

        public ActionResult DeleteItem(Guid id)
        {
            Guid controlListId = Guid.Empty;
            var controlListItem = _context.ControlListItems.FirstOrDefault(o => o.ControlListItemId == id);
            if (controlListItem != null)
            {
                controlListId = controlListItem.ControlList.ControlListId;
                controlListItem.IsDeleted = true;
                controlListItem.UpdatedDate = DateTime.UtcNow;
                controlListItem.UpdatedUser = AuthenticatedUser.UserName;
                _context.SaveChanges();
                TempData["SuccessMessage"] = "The ControlList Item " + controlListItem.ControlValue +
                                             " has successfully been removed";
            }
            else
            {
                TempData["ErrorMessage"] = "There was an issue removing Control List Item " + controlListItem.ControlValue;
            }
            return RedirectToAction("Edit","ControlList",new {id = controlListId});
        }

        [HttpPost]
        public ActionResult AddControlListItem(ControlListModelItem model)
        {


            var controlListId = model.ControlListId;
            var existingControlListItem = _context.ControlListItems.FirstOrDefault(o => o.ControlList.ControlListId == model.ControlListId && o.ControlValue == model.ItemName);
            if (existingControlListItem == null)
            {
                var controlList = _context.ControlLists.FirstOrDefault(o => o.ControlListId == model.ControlListId);
                if (controlList != null)
                {


                    var controlListItem = new ControlListItem();
                    controlListItem.ControlListItemId = Guid.NewGuid();
                    controlListItem.ControlValue = model.ItemName;

                    if (model.UploadFile != null)
                    {
                        controlListItem.ImageUrl = new ImageHelper().StoreListImage(model.UploadFile,
                            controlListItem.ControlListItemId);
                    }


                    controlList.ControlListItems.Add(controlListItem);
                    _context.SaveChanges();
                    TempData["SuccessMessage"] = "Item Saved";
                }
            }
            else
            {
                TempData["SuccessMessage"] = "Item already existed.";
            }
            return RedirectToAction("Edit", new { id = controlListId });
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }
    }
}
