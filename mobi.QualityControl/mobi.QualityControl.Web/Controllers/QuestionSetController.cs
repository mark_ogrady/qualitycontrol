﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using log4net;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Helpers;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Web.Models;
using Mobi.QualityControl.Web.ViewModels.Question;
using Mobi.QualityControl.Web.ViewModels.QuestionSet;

namespace Mobi.QualityControl.Web.Controllers
{
    [Authorize]
    public class QuestionSetController : AuthenticatedController
    {
        ILog log = log4net.LogManager.GetLogger(typeof(QuestionSetController));

        private QcContext context = new QcContext();
        //
        // GET: /QuestionSet/
        //List of Question Sets for Company
        public ActionResult Index()
        {
            ViewBag.Title = "Question Sets";
            var model = new QuestionSetIndexModel();
            var companyid = this.AuthenticatedUser.Company.CompanyId;
            var questionSetQuery = context.QuestionSets.Where(qs => qs.Company.CompanyId == companyid &&
                                                                    !qs.IsDeleted).Select(qs => new
                                                                    {
                                                                        QuestionSetId = qs.QuestionSetId,
                                                                        QuestionSetName = qs.QuestionSetName,
                                                                        QuestionCount = qs.Questions.Count,
                                                                        TransactionCount = qs.TransactionSets.Count
                                                                    }).ToList();

            foreach (var questionSet in questionSetQuery)
            {

                model.QuestionSetItems.Add(new QuestionSetItem()
                    {
                        QuestionSetId = questionSet.QuestionSetId,
                        QuestionSetName = questionSet.QuestionSetName,
                        QuestionCount = questionSet.QuestionCount,
                        TransactionCount = questionSet.TransactionCount

                    });
            }



            return View(model);
        }

        public ActionResult Details(Guid id)
        {
            ViewBag.Title = "View Question Set";

            var model = new QuestionSetEditModel();
            model.ScreenType = ScreenType.Details;
            PopulateEditModel(id, model);
            return View("Edit", model);
        }


        public ActionResult Create()
        {
            ViewBag.Title = "Create Question Set";
            var model = new QuestionSetEditModel();
            model.ScreenType = ScreenType.Create;
            return View("Edit", model);
        }

        public ActionResult Edit(Guid id)
        {
            ViewBag.Title = "Edit Question Set";
            var model = new QuestionSetEditModel();
            model.ScreenType = ScreenType.Edit;
            PopulateEditModel(id, model);

            return View(model);
        }

        private void PopulateEditModel(Guid id, QuestionSetEditModel model)
        {
            var questionSet = context.QuestionSets.Select(o => new
            {
                o.QuestionSetId,
                o.Company.CompanyId,
                o.IsDeleted,
                o.QuestionSetName,
                Questions = o.Questions.Select(i => new
                {
                    i.QuestionId,
                    i.QuestionText,
                    i.IsDeleted,
                    i.OrderPosition
                }).OrderBy(i => i.OrderPosition),
                ApprovalRule = o.ApprovalRules.Select(a => new
                {
                    a.ApprovalRuleId,
                    a.UserId,
                    a.OrderPosition
                })
            }).FirstOrDefault(o => o.QuestionSetId == id
                                                                    && o.CompanyId == this.AuthenticatedUser.Company.CompanyId
                                                                    && !o.IsDeleted);

            if (questionSet != null)
            {
                model.QuestionSetId = questionSet.QuestionSetId;
                model.QuestionSetName = questionSet.QuestionSetName;
                foreach (var question in questionSet.Questions.Where(o => !o.IsDeleted))
                {
                    model.QuestionItems.Add(new Mobi.QualityControl.Web.ViewModels.QuestionSet.QuestionItem()
                        {
                            QuestionId = question.QuestionId,
                            QuestionText = question.QuestionText,
                        });
                }


                foreach (var approvalRule in questionSet.ApprovalRule)      
                {
                    var user = context.UserProfiles.Select(o => new {o.UserId, ActualName = o.FirstName +" "+ o.LastName}).FirstOrDefault(o => o.UserId == approvalRule.UserId);
                    model.ApprovalRuleItems.Add(new ApprovalRuleItem()
                    {
                        ApprovalRuleId = approvalRule.ApprovalRuleId,
                        UserName = user.ActualName
                    });
                }
            }
            model.UserList = new DropDownHelper().GetUserListDropDown(AuthenticatedUser.Company.CompanyId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuestionSetEditModel model)
        {
            if (model.ScreenType == ScreenType.Create)
            {
                var existingQuestionSet = context.QuestionSets.Select(o => new
                {
                    o.QuestionSetId,
                    o.Company.CompanyId,
                    o.QuestionSetName
                }).FirstOrDefault(o => o.CompanyId == this.AuthenticatedUser.Company.CompanyId && o.QuestionSetName == model.QuestionSetName);
                if (existingQuestionSet != null)
                {
                    ModelState.AddModelError("", "Question Set Already Exists");
                }

            }
            else
            {
                var existingQuestionSet = context.QuestionSets.Select(o => new
                {
                    o.QuestionSetId,
                    o.Company.CompanyId,
                    o.QuestionSetName
                }).FirstOrDefault(o => o.CompanyId == this.AuthenticatedUser.Company.CompanyId && o.QuestionSetName == model.QuestionSetName && o.QuestionSetId != model.QuestionSetId);
                if (existingQuestionSet != null)
                {
                    ModelState.AddModelError("", "Question Set Already Exists");
                }
            }
            var questionSetId = model.QuestionSetId;
            if (ModelState.IsValid)
            {
                QuestionSet questionSet;
                if (model.ScreenType == ScreenType.Create)
                {
                    questionSet = new QuestionSet();
                    questionSet.QuestionSetId = Guid.NewGuid();
                    questionSetId = questionSet.QuestionSetId;
                }
                else
                {
                    questionSet = context.QuestionSets.FirstOrDefault(o => o.QuestionSetId == model.QuestionSetId && o.Company.CompanyId == this.AuthenticatedUser.Company.CompanyId);
                }
                questionSet.QuestionSetName = model.QuestionSetName;
                questionSet.TrackLocation = model.TrackLocation;
                if (model.ScreenType == ScreenType.Create)
                {
                    questionSet.Company = context.Companies.FirstOrDefault(o => o.CompanyId == AuthenticatedUser.Company.CompanyId);
                    context.QuestionSets.Add(questionSet);
                }


                //Reorder questions incase out of sync
                if (questionSet.Questions.Any())
                {
                    var questions = questionSet.Questions.OrderBy(o => o.OrderPosition);
                    var i = 0;
                    foreach (var question in questions)
                    {
                        question.OrderPosition = i++;
                    }


                }

                context.SaveChanges();
                TempData["SuccessMessage"] = "The Question Set has been successfully updated.";
                return RedirectToAction("Edit", new { id = questionSetId });

            }
            return View("Edit", model);
        }

        [HttpPost]
        public ActionResult AddApprovalRule(AddApprovalRuleModel model)
        {
            var questionSet = context.QuestionSets.FirstOrDefault(o => o.QuestionSetId == model.QuestionSetId);
            if (questionSet != null)
            {
                var approvalRule = new ApprovalRule();
                approvalRule.ApprovalRuleId = Guid.NewGuid();
                approvalRule.UserId = model.UserId;
                approvalRule.OrderPosition = questionSet.ApprovalRules.Count();

                questionSet.ApprovalRules.Add(approvalRule);

                TempData["SuccessMessage"] = "The approval rule has been successfully created.";
                context.SaveChanges();
            }

            return RedirectToAction("Edit", "QuestionSet", new { id = model.QuestionSetId });
        }

        public ActionResult Delete(Guid id)
        {

            var questionSet = context.QuestionSets.FirstOrDefault(o => o.QuestionSetId == id);
            if (questionSet != null)
            {
                questionSet.IsDeleted = true;
                questionSet.UpdatedDate = DateTime.UtcNow;
                questionSet.UpdatedUser = AuthenticatedUser.UserName;
                context.SaveChanges();
                TempData["SuccessMessage"] = "The Question Set " + questionSet.QuestionSetName +
                                             " has successfully been removed";
            }
            else
            {
                TempData["ErrorMessage"] = "There was an issue removing Question Set " + questionSet.QuestionSetName;
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult QuestionOrder(string items)
        {
            var itemArray = items.Split('&');

            foreach (var item in itemArray)
            {
                if (!String.IsNullOrEmpty(item))
                {
                    var split = item.Split('=');
                    var questionId = Guid.Parse(split[0]);
                    var question = context.Questions.FirstOrDefault(o => o.QuestionId == questionId);
                    question.OrderPosition = Convert.ToInt32(split[1]);
                    context.SaveChanges();
                }
            }

           return null;
        }

        protected override void Dispose(bool disposing)
        {
            context.Dispose();
            base.Dispose(disposing);
        }
    }
}
