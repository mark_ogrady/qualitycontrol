﻿using System.Linq;
using System.Web.Mvc;
using log4net;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Helpers;
using Mobi.QualityControl.Web.Helpers;
using Mobi.QualityControl.Web.ViewModels.User;

namespace Mobi.QualityControl.Web.Controllers
{
    public class UserController : AuthenticatedController
    {
        ILog _log = LogManager.GetLogger(typeof(UserController));

        private QcContext _context = new QcContext();
        //
        // GET: /User/


        public ActionResult ManageProfile()
        {
            var model = new ManageProfileModel();
            model.EmailAddress = AuthenticatedUser.UserName;
            model.CompanyId = AuthenticatedUser.Company.CompanyId;
            model.CompanyName = AuthenticatedUser.Company.CompanyName;
            model.FirstName = AuthenticatedUser.FirstName;
            model.LastName = AuthenticatedUser.LastName;
            if (AuthenticatedUser.Language != null)
            model.LanguageId = AuthenticatedUser.Language.LanguageId;
            model.Languages = new DropDownHelper().GetLanguaguesDropdown();
            
           

            return View(model);

        }


        [HttpPost]
        public ActionResult ManageProfile(ManageProfileModel model)
        {
            var userProfile = _context.UserProfiles.FirstOrDefault(o => o.UserId == AuthenticatedUser.UserId);

            if (userProfile != null)
            {
                
                userProfile.Language = _context.Languages.FirstOrDefault(o => o.LanguageId ==  model.LanguageId);
                userProfile.FirstName = model.FirstName;
                userProfile.LastName = model.LastName;
                _context.SaveChanges();
                TempData["SuccessMessage"] = "Your profile has been updated.";
            }
            model.Languages = new DropDownHelper().GetLanguaguesDropdown();

            return View(model);
        }

        [ChildActionOnly]
        public ActionResult UserInfo()
        {

            return PartialView("User/UserInfo",this.AuthenticatedUser);
        }

        [ChildActionOnly]
        public ActionResult Notifications()
        {
            var model = new NotificationModel();
            model.NotificationCount = 10;

            return PartialView("User/Notifications", model);
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }

    }
}
