﻿using System.Linq;
using System.Web.Mvc;
using log4net;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Web.ViewModels.ControlType;

namespace Mobi.QualityControl.Web.Controllers
{
      [Authorize]
    public class ControlTypeController : Controller
    {
        //
        // GET: /ControlType/

          ILog log = log4net.LogManager.GetLogger(typeof(ControlType));
   
       
        public ActionResult Index()
        {
            var model = new ControlTypeIndexModel();
            using (var context = new QcContext())
            {
                var controlTypes = from ct in context.ControlTypes
                                    select ct;
                foreach (var controlType in controlTypes)
                {
                    model.ControlTypeItems.Add(controlType);
                }
            }

            

            ViewBag.SelectedMenu = "Controls";
            return View(model);
        }

        [HttpPost]
        public ActionResult Add(ControlType controlType)
        {
            if (ModelState.IsValid)
            {
                using (var context = new QcContext())
                {
                    var controlExists = context.ControlTypes.Where(ct => ct.ControlName == controlType.ControlName);

                    if (!controlExists.Any())
                    {
                        context.ControlTypes.Add(controlType);
                        context.SaveChanges();
                        return new HttpStatusCodeResult(200, "The control has been successfully created");
                    }
                    else
                    {
                        return new HttpStatusCodeResult(400,"The control already exists");
                    }
                }

            }
            else
            {
                return new HttpStatusCodeResult(400, "The data entered is invalid");
            }
        }


    }
}
