﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Web.Models;
using Mobi.QualityControl.Web.ViewModels.Job;

namespace Mobi.QualityControl.Web.Controllers
{
    public class JobController : Controller
    {
        private QcContext _context = new QcContext();
        //
        // GET: /Job/
        public ActionResult Details(Guid jobId)
        {
            var model = PopulateJobEditModel(jobId);
            model.ScreenType = ScreenType.Details;
            return View(model);
        }


        public JobEditModel PopulateJobEditModel(Guid id)
        {
            var model = new JobEditModel();
            var jobQuery = _context.Jobs.Select(o => new
            {
                o.JobId,
                o.Jobcode,
                TransactionSets = o.TransactionSets.Select(ts => new
                {
                  ts.TransactionSetId,
                  ts.IsFullyComplete,
                  ts.IsEntryComplete,
                  ts.CreatedDate
                })
            }).FirstOrDefault(o => o.JobId == id);

            foreach (var transactionSet in jobQuery.TransactionSets)
            {
                model.JobTransactionSetItems.Add(new JobTransactionSetItem()
                {
                    TransactionSetId = transactionSet.TransactionSetId,
                    TransRef = transactionSet.CreatedDate.ToShortDateString()
                });
            }

            return model;

        }
	}
}