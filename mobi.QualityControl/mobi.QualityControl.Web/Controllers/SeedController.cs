﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Mobi.QualityControl.Framework.DataContext;

namespace Mobi.QualityControl.Web.Controllers
{
    public class SeedController : Controller
    {
        ILog log = log4net.LogManager.GetLogger(typeof(SeedController));

        //
        // GET: /Seed/
        private  QcContext _context = new QcContext();

        public ActionResult Index()
        {

            new QcDatabaseInitialiser().SeedData(_context); 
            return View();
        }

    }
}
