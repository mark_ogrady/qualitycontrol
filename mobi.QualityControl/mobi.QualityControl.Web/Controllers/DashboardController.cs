﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Razor.Generator;
using log4net;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Helpers;
using Mobi.QualityControl.Web.Helpers;
using Mobi.QualityControl.Web.ViewModels.Dashboard;

namespace Mobi.QualityControl.Web.Controllers
{
    [Authorize]
    public class DashboardController : AuthenticatedController
    {
        ILog log = log4net.LogManager.GetLogger(typeof(DashboardController));

          private QcContext context = new QcContext();
        //
        // GET: /Dashboard/
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Title = "Dashboard";
            var model = new DashboardIndexModel();
            var username = AuthenticatedUser.UserName;
            var dashboardQuery = context.DashboardItems.Select(o => new
            {
                o.IsDeleted,
                o.UserProfile.UserName,
                o.EntityId,
                o.Name,
                o.DashboardType
            }).Where(o => o.UserName == username && !o.IsDeleted);
            if (dashboardQuery.Any())
            {
                foreach (var dashboardItem in dashboardQuery.ToList())
                {
                    model.DashboardIndexItems.Add(new DashboardIndexItem()
                    {
                        DashboardType = dashboardItem.DashboardType,
                        EntityId =  dashboardItem.EntityId,
                        Name = dashboardItem.Name
                    });
                }

            }

            return View(model);
        }

        public ActionResult Edit()
        {
            var model = new DashboardEditModel();

            model.QuestionSetList = new DropDownHelper().GetQuestionSetDropDown(AuthenticatedUser.Company.CompanyId);
            model.DashboardTypes = new DropDownHelper().GetDashboardTypeDropDown();
            PopulateDashboardEditItems(model);
            return View(model);
        }

        private void PopulateDashboardEditItems(DashboardEditModel model)
        {
            var dashboardItemQuery = context.DashboardItems.Select(o => new
            {
                o.UserProfile.UserId,
                o.IsDeleted,
                o.Name,
                o.OrderPosition,
                o.EntityId,
                o.DashboardType,
                o.Description,
                o.DashboardItemId
            }).Where(o => o.UserId == AuthenticatedUser.UserId && !o.IsDeleted);
            if (dashboardItemQuery.Any())
            {
                foreach (var dashboardItem in dashboardItemQuery.ToList())
                {
                    model.DashboardEditItems.Add(new DashboardEditItem()
                    {
                       DashboardItemId = dashboardItem.DashboardItemId,
                       DashboardType = dashboardItem.DashboardType,
                       Description = dashboardItem.Description,
                       EntityId = dashboardItem.EntityId,
                       Name = dashboardItem.Name,
                       OrderPosition = dashboardItem.OrderPosition
                    });
                }

            }
        }

        protected override void Dispose(bool disposing)
        {
            context.Dispose();
            base.Dispose(disposing);
        }
    }
}
