﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using log4net;
using Microsoft.Ajax.Utilities;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Web.Models;
using Mobi.QualityControl.Web.ViewModels.Relationship;

namespace Mobi.QualityControl.Web.Controllers
{
    [Authorize]
    public class RelationshipController : AuthenticatedController
    {
        ILog log = log4net.LogManager.GetLogger(typeof(RelationshipController));

        //
        // GET: /Relationship/
        private QcContext context = new QcContext();

        public ActionResult Index()
        {
            ViewBag.Title = "Connections";

            var model = new RelationshipIndexModel();
            //List Current relationships
            var companyId = AuthenticatedUser.Company.CompanyId;
            var companyRelationshipQuery = context.CompanyRelationships.Select(o => new
            {
                o.CompanyFromId,
                o.CompanyToId
            }).Where(o => o.CompanyFromId == companyId);

            foreach (var companyRelationship in companyRelationshipQuery.ToList())
            {
                var companyTo = context.Companies.FirstOrDefault(o => o.CompanyId == companyRelationship.CompanyToId);
                model.CompanyItems.Add(new CompanyItem()
                {
                    CompanyId = companyTo.CompanyId,
                    CompanyName = companyTo.CompanyName

                });
            }

            //List New Request
            var requestionRelationshipQuery = context.CompanyRelationshipRequests.Select(o => new
            {
                o.CompanyToId,
                o.RejectedDate,
                o.AcceptedDate,
                o.CompanyFromId,
                o.CompanyRelationshipRequestId
            }).Where(o => o.CompanyToId == companyId && o.RejectedDate == null && o.AcceptedDate == null);

            foreach (var companyRelationshipRequest in requestionRelationshipQuery.ToList())
            {
                var company = context.Companies.Select(o => new { o.CompanyId, o.CompanyName}).FirstOrDefault(o => o.CompanyId == companyRelationshipRequest.CompanyFromId);


                var requestItem = new RelationshipRequestItem();
                requestItem.CompanyName = company.CompanyName;
                requestItem.CompanyId = company.CompanyId;
                requestItem.RequestId = companyRelationshipRequest.CompanyRelationshipRequestId;
                model.RelationshipRequestItems.Add(requestItem);
            }

            var outstandingRelationshipQuery = context.CompanyRelationshipRequests.Where(o => o.CompanyFromId == companyId && o.RejectedDate == null && o.AcceptedDate == null);

            foreach (var companyRelationshipRequest in outstandingRelationshipQuery.ToList())
            {
                var company = context.Companies.Select(o => new { o.CompanyId, o.CompanyName }).FirstOrDefault(o => o.CompanyId == companyRelationshipRequest.CompanyToId);
                model.OutstandingRequestItems.Add(new OutstandingRequestItem()
                {
                    CompanyId = company.CompanyId,
                    CompanyName = company.CompanyName
                });
            }

            return View(model);
        }

        public ActionResult Details(Guid id)
        {
            ViewBag.Title = "View Connection";

            var model = new RelationshipEditModel();
            model.ScreenType = ScreenType.Details;
            PopulateRelationshipEditModel(model,id);
            return View("Edit", model);
        }

        public ActionResult RequestRelationship()
        {
            var model = new RequestRelationshipModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult RequestRelationship(RequestRelationshipModel model)
        {
           
            if (ModelState.IsValid)
            {
                //Check if email exist
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var userToQuery = context.UserProfiles.FirstOrDefault(o => o.UserName == model.EmailAddress);
                if (userToQuery != null)
                {

                    var relationshipExist = context.CompanyRelationships.FirstOrDefault(o => o.CompanyFromId == this.AuthenticatedUser.Company.CompanyId && o.CompanyToId == userToQuery.Company.CompanyId);
                    if (relationshipExist != null)
                    {
                        //return already exists
                        ModelState.AddModelError("","Relationship Already exists");
                        return View(model);
                    }

                    var relationshipRequestExist = context.CompanyRelationshipRequests.FirstOrDefault(o => o.CompanyFromId == this.AuthenticatedUser.Company.CompanyId && o.CompanyToId == userToQuery.Company.CompanyId);
                    if (relationshipRequestExist != null)
                    {
                        // return already pending
                        ModelState.AddModelError("","Request already exists");
                        return View(model);
                    }
                    //Create Request
                    var companyRelationshipRequest = new CompanyRelationshipRequest();
                    companyRelationshipRequest.CompanyRelationshipRequestId = Guid.NewGuid();
                    companyRelationshipRequest.CompanyFromId = AuthenticatedUser.Company.CompanyId;
                    companyRelationshipRequest.CompanyToId = userToQuery.Company.CompanyId;
                    companyRelationshipRequest.RequestedDate = DateTime.UtcNow;
                    companyRelationshipRequest.RequestMessage = model.Message;
                    context.CompanyRelationshipRequests.Add(companyRelationshipRequest);
                    context.SaveChanges();
                    //new Helpers.Notification().SendNotification();
                    TempData["SuccessMessage"] = "Request has been sent.";
                }
                else
                {
                    TempData["ErrorMessage"] = "The user doesn't exist.";
                    return View(model);
                }

            }
            return RedirectToAction("Index", "Relationship");
        }

        public ActionResult ViewRequest(Guid id)
        {
            ViewBag.Title = "Connection Request";
            var model = new RelationshipViewRequestModel();
            var viewRequest = context.CompanyRelationshipRequests.Select(o => new
            {
                o.CompanyFromId,
                o.CompanyRelationshipRequestId,
                o.CreatedUser,
                o.RequestMessage
            }).FirstOrDefault(o => o.CompanyRelationshipRequestId == id);
            if (viewRequest != null)
            {
                var company = context.Companies.Select(o => new { o.CompanyId, o.CompanyName }).FirstOrDefault(o => o.CompanyId == viewRequest.CompanyFromId);
                model.CompanyRelationshipRequestId = id;
                model.CompanyName = company.CompanyName;
                model.UserName = viewRequest.CreatedUser;
                model.Message = viewRequest.RequestMessage;

            }


            return View(model);
        }

        [HttpPost]
        public ActionResult RelationshipResponse(RelationshipResponseModel model)
        {
            var relationshipRequest =
                context.CompanyRelationshipRequests.FirstOrDefault(
                    o => o.CompanyRelationshipRequestId == model.CompanyRelationshipRequestId);

            var companyFrom = context.Companies.FirstOrDefault(o => o.CompanyId == relationshipRequest.CompanyFromId);
            if (relationshipRequest != null)
            {
                switch (Request.Form["action"])
                {
                    case "Accept":
                        //Create Relationship
                        relationshipRequest.AcceptedDate = DateTime.UtcNow;
                        //CompanyFrom
                        var companyFromRelationship = new CompanyRelationship();
                        companyFromRelationship.CompanyRelationshipId = Guid.NewGuid();
                        companyFromRelationship.CompanyFromId = relationshipRequest.CompanyFromId;
                        companyFromRelationship.CompanyToId = relationshipRequest.CompanyToId;
                        context.CompanyRelationships.Add(companyFromRelationship);


                        //companyTo
                        var companyToReleationship = new CompanyRelationship();
                        companyToReleationship.CompanyRelationshipId = Guid.NewGuid();
                        companyToReleationship.CompanyFromId = relationshipRequest.CompanyToId;
                        companyToReleationship.CompanyToId = relationshipRequest.CompanyFromId;
                        context.CompanyRelationships.Add(companyToReleationship);

                        context.SaveChanges();
                        TempData["SuccessMessage"] = "The request from " + companyFrom.CompanyName + " has been accepted.";
                        break;
                    case "Reject":
                        relationshipRequest.RejectedDate = DateTime.UtcNow;

                        TempData["SuccessMessage"] = "The request from " + companyFrom.CompanyName + " has been rejected.";
                        break;

                }
            }
            //TODO Send Notifications


            return RedirectToAction("Index");
        }

        public void PopulateRelationshipEditModel(RelationshipEditModel model,Guid companyToId)
        {
            var relationship =
                context.CompanyRelationships.FirstOrDefault(o => o.CompanyToId == companyToId && o.CompanyFromId == AuthenticatedUser.Company.CompanyId );
            if (relationship != null)
            {
                model.RelationshipId = relationship.CompanyRelationshipId;
                model.CompanyName = context.Companies.FirstOrDefault(o => o.CompanyId == companyToId).CompanyName;
                model.AcceptedDate = relationship.CreatedDate;
            }
        }

        protected override void Dispose(bool disposing)
        {
            context.Dispose();
            base.Dispose(disposing);
        }

    }
}
