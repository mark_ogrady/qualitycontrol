﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using log4net;
using Microsoft.Web.WebPages.OAuth;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Helpers;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Web.Helpers;
using WebMatrix.WebData;
using Mobi.QualityControl.Web.Filters;
using Mobi.QualityControl.Web.Models;


namespace Mobi.QualityControl.Web.Controllers
{



    [Authorize]
    [InitializeSimpleMembership]
    public class AccountController : Controller
    {
        ILog log = log4net.LogManager.GetLogger(typeof(AccountController));
        QcContext context = new QcContext();
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            ViewBag.PageType = "Register";
            return View("Login");
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(LoginModel model)
        {
            if (model.FirstName == null)
                ModelState.AddModelError("", "First Name required.");
            if (model.LastName == null)
                ModelState.AddModelError("", "Last Name required.");
            if (model.CompanyName == null)
                ModelState.AddModelError("", "Company Name required.");
            if (model.UserName == null)
                ModelState.AddModelError("", "Email Address required.");
            if (model.Password == null)
                ModelState.AddModelError("", "Password required.");
            if (model.Code == null)
            {
                ModelState.AddModelError("", "Beta code must be entered.");
            }
            else if (model.Code != "Beta123")
                ModelState.AddModelError("", "Beta code is incorrect.");
            ViewBag.PageType = "Register";
            


            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                    WebSecurity.Login(model.UserName, model.Password);
                    var user = context.UserProfiles.FirstOrDefault(o => o.UserName == model.UserName);
                    var company = new Framework.Model.Company();
                    company.CompanyId = Guid.NewGuid();
                    company.CompanyName = model.CompanyName;
                    context.Companies.Add(company);
                    context.SaveChanges();
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    company.UserProfiles.Add(user);
                    context.SaveChanges();

                    new NotificationTypesHelper().RegisterMessage(user.UserId);
                   
                    return RedirectToAction("Index", "Dashboard");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }
           
            // If we got this far, something failed, redisplay form
            return View("Login",model);
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            var model = new ForgetAccountModel();

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ForgotPassword(ForgetAccountModel model)
        {
            var userProfile = context.UserProfiles.FirstOrDefault(o => o.UserName == model.EmailAddress);
            if (userProfile != null)
            {
                var id = Guid.NewGuid();
                var forgottonRequest = new ForgottenPasswordRequest();
                forgottonRequest.ForgottenPasswordRequestId = id;
                forgottonRequest.UserName = model.EmailAddress;
                context.ForgottenPasswordRequests.Add(forgottonRequest);
                context.SaveChanges();
                new NotificationTypesHelper().ForgottenMessage(id,userProfile.UserId);
            }
            else
            {
                ModelState.AddModelError("", "The user doesn't exist.");
            }
            return View();
        }

        public ActionResult ResetPassword(Guid id)
        {

            return View();
        }
        

        //
        // GET: /Account/Manage

        public ActionResult ChangePassword(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("ChangePassword");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("ChangePassword");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("ChangePassword", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("ChangePassword", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("", String.Format("Unable to create local account. An account with the name \"{0}\" may already exist.", User.Identity.Name));
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

      

     

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Dashboard");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            context.Dispose();
            base.Dispose(disposing);
        }

    }
}
