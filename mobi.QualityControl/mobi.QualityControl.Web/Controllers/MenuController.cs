﻿using System.Linq;
using System.Web.Mvc;
using log4net;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Web.ViewModels.Menu;

namespace Mobi.QualityControl.Web.Controllers
{
    public class MenuController : Controller
    {
        ILog log = log4net.LogManager.GetLogger(typeof(MenuController));


        //
        // GET: /Menu/

        [ChildActionOnly]
        public ActionResult AppSideMenu(string selected)
        {
            using (var context = new QcContext())
            {
                var menuViewModel = new MenuViewModel();
                var menus = context.Menus.Where(mnu => mnu.MenuType == 2).OrderBy(mnu => mnu.OrderPosition).ToList();
                menuViewModel.Menus = menus;
                menuViewModel.SelectedMenu = selected;
                return PartialView("Menu/AppSideMenu", menuViewModel);
            }
        }

        [ChildActionOnly]
        public ActionResult AppTopMenu(string selected)
        {
            using (var context = new QcContext())
            {
                var menuViewModel = new MenuViewModel();
                var menus = context.Menus.Where(mnu => mnu.MenuType == 2).OrderBy(mnu => mnu.OrderPosition).ToList();
                menuViewModel.Menus = menus;
                menuViewModel.SelectedMenu = selected;
                return PartialView("Menu/AppTopMenu", menuViewModel);
            }
        }

    }
}
