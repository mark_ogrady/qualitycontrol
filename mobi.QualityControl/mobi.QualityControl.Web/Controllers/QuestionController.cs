﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Enum;
using Mobi.QualityControl.Framework.Helpers;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Web.Helpers;
using Mobi.QualityControl.Web.Models;
using Mobi.QualityControl.Web.ViewModels.Question;

namespace Mobi.QualityControl.Web.Controllers
{
    [Authorize]
    public class QuestionController : AuthenticatedController
    {
        ILog log = log4net.LogManager.GetLogger(typeof(QuestionController));

        private QcContext context = new QcContext();


        public ActionResult Details(Guid id)
        {
            ViewBag.Title = "View Question";
            var model = new QuestionEditModel();
            model.ScreenType = ScreenType.Details;
            model.QuestionId = id;
            PopulateEditModel(model);

            return View("Edit", model);
        }


        public ActionResult Create(Guid id)
        {
            ViewBag.Title = "Add Question";
            var model = new QuestionEditModel();
            model.QuestionSetId = id;
            PopulateEditModel(model);

            model.ScreenType = ScreenType.Create;
            return View("Edit", model);
        }

        public ActionResult Edit(Guid id)
        {
            ViewBag.Title = "Edit Question";

            var model = new QuestionEditModel();
            model.ScreenType = ScreenType.Edit;

            model.QuestionId = id;
            PopulateEditModel(model);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuestionEditModel model)
        {

            if (ModelState.IsValid)
            {
                Question question;
                if (model.ScreenType == ScreenType.Create)
                {
                    question = new Question();
                    question.QuestionId = Guid.NewGuid();
                    question.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlTypeId == model.ControlTypeId);
                }
                else
                {
                    question = context.Questions.FirstOrDefault(o => o.QuestionId == model.QuestionId && o.QuestionSet.Company.CompanyId == this.AuthenticatedUser.Company.CompanyId);
                }

                question.QuestionText = model.QuestionText;
                question.IsHidden = model.IsHidden;
                question.DefaultValue = model.DefaultValue;
                question.IsMandatory = model.IsMandatory;
                if (question.ControlType.AllowControlList)
                    question.ControlList = context.ControlLists.FirstOrDefault(o => o.ControlListId == model.ControlListId);
                if (model.OrderPosition == 0)
                {
                    var questionCount = context.QuestionSets.Count(o => o.QuestionSetId == model.QuestionSetId);
                    question.OrderPosition = questionCount + 1;
                }
                else
                {
                    question.OrderPosition = model.OrderPosition;

                }


                if (model.ScreenType == ScreenType.Create)
                {
                    question.QuestionSet = context.QuestionSets.FirstOrDefault(o => o.QuestionSetId == model.QuestionSetId);
                    context.Questions.Add(question);

                }
                context.SaveChanges();
                return RedirectToAction("Edit", "QuestionSet", new { id = model.QuestionSetId });
            }
            return View("Edit", model);
        }

        private void PopulateEditModel(QuestionEditModel model)
        {
            model.ControlTypes = new DropDownHelper().GetControlTypeDropdown();
            model.ControlLists = new DropDownHelper().GetControlListDropdown(AuthenticatedUser.Company.CompanyId);
            model.LanguageList = new DropDownHelper().GetLanguaguesForQuestionDropdown(model.QuestionId);
            model.QuestionsFromList = new DropDownHelper().GetQuestionFromDropdown(model.QuestionId);
            model.EvalutionTypes = new DropDownHelper().GetViewEvalutionDropDown();
            var questionLocalsQuery = context.QuestionLocals.Select(o => new
            {
                o.Question.QuestionId,
                o.Language.LanguageId,
                o.Language.LanguageName,
                o.QuestionText
            }).Where(o => o.QuestionId == model.QuestionId);

            foreach (var questionLocal in questionLocalsQuery.ToList())
            {
                model.QuestionLocalEditModels.Add(new QuestionLocalEditModel()
                {
                    LanguageId = questionLocal.LanguageId,
                    LanguageName = questionLocal.LanguageName,
                    QuestionId = questionLocal.QuestionId,
                    QuestionLocalText = questionLocal.QuestionText

                });
            }
            var question = context.Questions.FirstOrDefault(o => o.QuestionId == model.QuestionId && o.QuestionSet.Company.CompanyId == this.AuthenticatedUser.Company.CompanyId);
            if (question != null)
            {
                model.QuestionId = question.QuestionId;
                model.QuestionText = question.QuestionText;
                model.QuestionSetId = question.QuestionSet.QuestionSetId;
                model.ControlName = question.ControlType.ControlName;
                if (question.ControlList != null)
                    model.ControlListId = question.ControlList.ControlListId;

                foreach (var questionRule in question.QuestionRules)
                {
                    model.QuestionRuleModels.Add(new QuestionRuleModel()
                    {
                        QuestionRuleId = questionRule.QuestionRuleId,
                        QuestionId = question.QuestionId,
                        EvalutionValue = questionRule.EvaluationValue,
                        EvalutionType = questionRule.ViewEvalutionType.ToString(),
                        QuestionFromRuleId = questionRule.FromQuestion.QuestionId,
                        QuestionFromText = questionRule.FromQuestion.QuestionText
                    });
                }
            }
        }

        [HttpPost]
        public ActionResult AddQuestionTextLocal(QuestionLocalEditModel model)
        {
            var questionLocal = new QuestionLocal();
            questionLocal.QuestionLocalId = Guid.NewGuid();
            questionLocal.Language = context.Languages.FirstOrDefault(o => o.LanguageId == model.LanguageId);
            questionLocal.Question = context.Questions.FirstOrDefault(o => o.QuestionId == model.QuestionId);
            questionLocal.QuestionText = model.QuestionLocalText;

            context.QuestionLocals.Add(questionLocal);
            context.SaveChanges();

            return RedirectToAction("Edit", "Question", new { id = model.QuestionId });
        }



        public ActionResult Delete(Guid id, Guid questionSetId)
        {

            var question = context.Questions.FirstOrDefault(o => o.QuestionId == id && o.QuestionSet.QuestionSetId == questionSetId);
            if (question != null)
            {
                question.IsDeleted = true;
                question.UpdatedDate = DateTime.UtcNow;
                question.UpdatedUser = AuthenticatedUser.UserName;
                context.SaveChanges();
                TempData["SuccessMessage"] = "The Question " + question.QuestionText +
                                             " has successfully been removed";
            }
            else
            {
                TempData["ErrorMessage"] = "There was an issue removing Question " + question.QuestionText;
            }
            return RedirectToAction("Edit", "QuestionSet", new { id = questionSetId });
        }


        [HttpPost]
        public ActionResult AddQuestionRule(QuestionRuleModel model)
        {

            var question = context.Questions.FirstOrDefault(o => o.QuestionId == model.QuestionId);
            if (question != null)
            {
                var questionRule = new QuestionRule();
                questionRule.QuestionRuleId = Guid.NewGuid();
                questionRule.FromQuestion = context.Questions.FirstOrDefault(o => o.QuestionId == model.QuestionFromRuleId);
                questionRule.ViewEvalutionType = (ViewEvalutionType)Enum.Parse(typeof(ViewEvalutionType), model.EvalutionType);
                questionRule.EvaluationValue = model.EvalutionValue;
                question.QuestionRules.Add(questionRule);
                context.SaveChanges();
            }
            return RedirectToAction("Edit", "Question", new { id = model.QuestionId });
        }


        protected override void Dispose(bool disposing)
        {
            context.Dispose();
            base.Dispose(disposing);
        }

    }
}
