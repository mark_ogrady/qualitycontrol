﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Helpers;
using Mobi.QualityControl.Web.Views.Approval;

namespace Mobi.QualityControl.Web.Controllers
{
    public class ApprovalController : AuthenticatedController
    {
        
        private QcContext _context = new QcContext();
        //
        // GET: /Approval/
        public ActionResult Index()
        {
            var model = new ApprovalIndexModel();
            var approvalItems = _context.ApprovalSteps.Select(o => new
            {
                o.AcceptedDate,
                o.RejectedDate,
                o.UserId,
                o.CurrentStep,
                o.TransactionSet.TransactionSetId,
                o.ApprovalStepId,
                TransCreationDate = o.TransactionSet.CreatedDate,
                
            }).Where(o => o.AcceptedDate == null && o.RejectedDate == null && o.UserId == AuthenticatedUser.UserId && o.CurrentStep);
            if (approvalItems.Any())
            {
                foreach (var approvalItem in approvalItems)
                {
                    model.ApprovalIndexItems.Add(new ApprovalIndexItem()
                    {
                        ApprovalStepId = approvalItem.ApprovalStepId,
                        TransactionCreation = approvalItem.TransCreationDate,
                        TransactionSetId = approvalItem.TransactionSetId
                    });
                    
                }


            }

            return View(model);
        }

        public ActionResult Approve(Guid id)
        {
           // check correct user
            var exisitngApproval = _context.ApprovalSteps.FirstOrDefault(o => o.UserId == AuthenticatedUser.UserId && o.ApprovalStepId == id);
            if (exisitngApproval != null)
            {
                new ApprovalHelper().AcceptStep(id);
                TempData["SuccessMessage"] = "Approval Successful";
            }
            else
            {
                TempData["ErrorMessage"] = "You are not authorised to approve this item";
            }
                return RedirectToAction("Index");


        }

        public ActionResult History(Guid id, string message)
        {
            var exisitngApproval = _context.ApprovalSteps.FirstOrDefault(o => o.UserId == AuthenticatedUser.UserId && o.ApprovalStepId == id);
            if (exisitngApproval != null)
            {
                new ApprovalHelper().RejectStep(id, message);
                TempData["SuccessMessage"] = "Rejection Successful";
            }
            else
            {
                TempData["ErrorMessage"] = "You are not authorised to reject this item";
            }
            return RedirectToAction("Index");
        }
	}
}