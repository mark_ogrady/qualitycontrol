﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Helpers;
using Mobi.QualityControl.Web.Helpers;
using Mobi.QualityControl.Web.Models;
using Mobi.QualityControl.Web.ViewModels.Company;

namespace Mobi.QualityControl.Web.Controllers
{
    
    public class CompanyController : AuthenticatedController
    {
        ILog log = log4net.LogManager.GetLogger(typeof(CompanyController));

       private QcContext context = new QcContext();
        //
        // GET: /Company/
        public ActionResult Edit()
        {
            var model = new CompanyEditModel();
            model.ScreenType = ScreenType.Edit;
            PopulateEditModel(model);
            return View(model);
        }

        public ActionResult Details()
        {
            var model = new CompanyEditModel();
            model.ScreenType = ScreenType.Details;
            PopulateEditModel(model);
            return View("Edit",model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CompanyEditModel model)
        {
            var companyId = this.AuthenticatedUser.Company.CompanyId;

            var existingCompany =
                context.Companies.FirstOrDefault(o => o.CompanyName == model.CompanyName && o.CompanyId != companyId);
            if (existingCompany != null)
                ModelState.AddModelError("","Company Name Already Exists");


            if (ModelState.IsValid)
            {

                
                var company = context.Companies.FirstOrDefault(o => o.CompanyId == companyId);
                if (company != null)
                {
                    company.CompanyName = model.CompanyName;
                    company.CompanyWebsiteUrl = model.CompanyWebsiteUrl;
                    company.CompanyAddress1 = model.CompanyAddress1;
                    company.CompanyAddress2 = model.CompanyAddress2;
                    company.CompanyAddress3 = model.CompanyAddress3;
                    company.CompanyAddress4 = model.CompanyAddress4;
                    company.PostCode = model.PostCode;
                    company.ContactNumber = model.ContactNumber;
                    context.SaveChanges();


                }
                return RedirectToAction("Details", "Company");
            }
            return View(model);
        }

        private void PopulateEditModel(CompanyEditModel model)
        {
            model.CompanyId = AuthenticatedUser.Company.CompanyId;
            model.CompanyName = AuthenticatedUser.Company.CompanyName;
            model.CompanyWebsiteUrl = AuthenticatedUser.Company.CompanyWebsiteUrl;
            model.CompanyAddress1 = AuthenticatedUser.Company.CompanyAddress1;
            model.CompanyAddress2 = AuthenticatedUser.Company.CompanyAddress2;
            model.CompanyAddress3 = AuthenticatedUser.Company.CompanyAddress3;
            model.CompanyAddress4 = AuthenticatedUser.Company.CompanyAddress4;
            model.ContactNumber = AuthenticatedUser.Company.ContactNumber;
            model.SubscriptionList = new DropDownHelper().GetSubscriptionTypeDropDown();
            model.SubscriptionType = AuthenticatedUser.Company.SubscriptionType;
        }

        protected override void Dispose(bool disposing)
        {
            context.Dispose();
            base.Dispose(disposing);
        }
	}
}