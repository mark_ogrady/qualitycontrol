﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using log4net;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Enum;
using Mobi.QualityControl.Framework.Helpers;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Framework.ViewModel.Transaction;
using Mobi.QualityControl.Web.Helpers;
using Mobi.QualityControl.Web.Models;
using Mobi.QualityControl.Web.ViewModels.Transaction;

namespace Mobi.QualityControl.Web.Controllers
{
    [Authorize]
    public class TransactionController : AuthenticatedController
    {
        ILog log = log4net.LogManager.GetLogger(typeof(TransactionController));

        //
        // GET: /Transaction/
        private QcContext _context = new QcContext();

        public ActionResult Start(Guid id)
        {
            var model = new TransactionItemModel();
            var questionSetQuery =
                _context.QuestionSets.FirstOrDefault(
                    o => o.QuestionSetId == id && o.Company.CompanyId == AuthenticatedUser.Company.CompanyId);
            if (questionSetQuery != null)
            {
                var firstQuestion = questionSetQuery.Questions.OrderBy(o => o.OrderPosition).FirstOrDefault();
                if (firstQuestion != null)
                {
                    model.QuestionSetId = questionSetQuery.QuestionSetId;
                    model.QuestionId = firstQuestion.QuestionId;
                    model.QuestionText = new LanaguageHelper().GetQuestionText(firstQuestion.QuestionId, AuthenticatedUser.Language.LanguageId);
                    

                    model.SequenceNo = firstQuestion.OrderPosition;
                    model.ControlType = firstQuestion.ControlType.ControlName;
                    model.QuestionsList = new QuestionSetHelper().GetQuestionList(id,Guid.Empty, AuthenticatedUser.Language.LanguageId);
                    model.Result = firstQuestion.DefaultValue;
                    model.IsFirst = true;
                    if (firstQuestion.ControlList != null)
                    model.ControlListItems = new DropDownHelper().GetControlList(firstQuestion.ControlList.ControlListId);
                }
                else
                {
                    @TempData["ErrorMessage"] = "Questions do not exist for Questions";
                }
            }

            return View("Entry", model);
        }

       

        public ActionResult Entry(Guid transactionSetId, Guid questionId)
        {
            var model = new TransactionItemModel();
            model.TransactionSetId = transactionSetId;
            var transactionQuery = _context.Transactions.FirstOrDefault(o => o.TransactionSet.TransactionSetId == transactionSetId && o.Question.QuestionId == questionId);

            if (transactionQuery != null)
            {
                model.QuestionSetId = transactionQuery.Question.QuestionSet.QuestionSetId;
                model.QuestionId = transactionQuery.Question.QuestionId;
                model.QuestionText = new LanaguageHelper().GetQuestionText( transactionQuery.Question.QuestionId, AuthenticatedUser.Language.LanguageId);
                model.SequenceNo = transactionQuery.Question.OrderPosition;
                model.ControlType = transactionQuery.Question.ControlType.ControlName;
                model.QuestionsList = new QuestionSetHelper().GetQuestionList(model.QuestionSetId,transactionSetId, AuthenticatedUser.Language.LanguageId);
                model.ControlType = transactionQuery.Question.ControlType.ControlName;
                model.Result = transactionQuery.Result;
            }
            else
            {
                var questionQuery = _context.Questions.FirstOrDefault(o => o.QuestionId == questionId);
                if (questionQuery != null)
                {
                    model.TransactionSetId = transactionSetId;
                    model.QuestionSetId = questionQuery.QuestionSet.QuestionSetId;
                    model.QuestionText = new LanaguageHelper().GetQuestionText(questionQuery.QuestionId, AuthenticatedUser.Language.LanguageId);
                    
                    model.SequenceNo = questionQuery.OrderPosition;
                    model.ControlType = questionQuery.ControlType.ControlName;
                    if (questionQuery.ControlList != null)
                        model.ControlListItems =  new DropDownHelper().GetControlList(questionQuery.ControlList.ControlListId);

                }
            }
            model.QuestionsList = new QuestionSetHelper().GetQuestionList(model.QuestionSetId, transactionSetId, AuthenticatedUser.Language.LanguageId);

            return View("Entry", model)
            ;

        }

        public ActionResult Previous(Guid questionId, Guid transactionSetId)
        {
            var question = _context.Questions.FirstOrDefault(o => o.QuestionId == questionId);
             var prevQuestion = _context.Questions.OrderBy(o => o.OrderPosition).FirstOrDefault(o => o.QuestionSet.QuestionSetId == question.QuestionSet.QuestionSetId && o.OrderPosition == (question.OrderPosition -1));

            return RedirectToAction("Entry", new {transactionSetId, questionId = prevQuestion.QuestionId});
        }


        public ActionResult Next(TransactionItemModel model)
        {
            var transactionSetId = model.TransactionSetId;
            var questionSetId = model.QuestionSetId;
          
            var nextModel = new TransactionHelper().SaveAndNextQuestion(model, this.AuthenticatedUser.Language.LanguageId);
            if (nextModel != null)
            {
                return View("Entry", nextModel);
            }
            else
            {
                nextModel = new TransactionItemModel();
                nextModel.TransactionSetId = transactionSetId;
                nextModel.QuestionsList = new QuestionSetHelper().GetQuestionList(questionSetId, transactionSetId, this.AuthenticatedUser.Language.LanguageId);
                // Will want REdirect 
                return View("Finish", model);
            }
        }

        public ActionResult Finish(Guid transactionSetId)
        {
            var model = new TransactionItemModel();
            model.TransactionSetId = transactionSetId;
            var transactionSetQuery = _context.TransactionSets.FirstOrDefault(o => o.TransactionSetId == transactionSetId);
            model.QuestionsList = new QuestionSetHelper().GetQuestionList(transactionSetQuery.QuestionSet.QuestionSetId,transactionSetId, this.AuthenticatedUser.Language.LanguageId);

            return View(model);
        }


        [HttpPost]
        public ActionResult Finish(TransactionItemModel model)
        {
            var transactionSetQuery = _context.TransactionSets.FirstOrDefault(o => o.TransactionSetId == model.TransactionSetId);
            transactionSetQuery.IsEntryComplete = true;
            if (transactionSetQuery.ApprovalSteps.Count == 0)
            {

               
                transactionSetQuery.IsFullyComplete = true;
            }
            else
            {
                //notify first person of approval
                var approvalStep = transactionSetQuery.ApprovalSteps.Select(o => new {o.UserId,o.OrderPosition}).OrderBy(o => o.OrderPosition).FirstOrDefault();
                new NotificationHelper().SendNotification(approvalStep.UserId, 1, true, "Approval Required", "There is an outstanding approval required", NotificationType.ApprovalRequired);
            }

            _context.SaveChanges();
            return RedirectToAction("Index", "Dashboard");

        }

      

        
        public ActionResult Create(Guid id)
        {
            var model = new TransactionEditModel();
            var questionSet = _context.QuestionSets.FirstOrDefault(o => o.QuestionSetId == id);
            model.ScreenType = ScreenType.Create;
            model.QuestionSetId = questionSet.QuestionSetId;
            model.Questions = questionSet.Questions.ToList();

            return View("Edit", model);
        }

        public ActionResult Edit(Guid id)
        {
            var model = new TransactionEditModel();
            var questionSet = _context.TransactionSets.FirstOrDefault(o => o.TransactionSetId == id);
            model.ScreenType = ScreenType.Edit;
            model.QuestionSetId = questionSet.TransactionSetId;
            model.Questions = questionSet.QuestionSet.Questions.ToList();

            return View(model);
        }

        public ActionResult View(Guid id)
        {
            var model = new TransactionEditModel();
            var questionSet = _context.TransactionSets.FirstOrDefault(o => o.TransactionSetId == id);
            model.ScreenType = ScreenType.Details;
            model.QuestionSetId = questionSet.TransactionSetId;
            model.Questions = questionSet.QuestionSet.Questions.ToList();

            return View("Edit",model);
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }

    }
}
