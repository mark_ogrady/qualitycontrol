﻿using System.Linq;
using System.Web.Mvc;
using log4net;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Model;

namespace Mobi.QualityControl.Web.Controllers
{
    public class AuthenticatedController : Controller
    {

        ILog log = log4net.LogManager.GetLogger(typeof(AuthenticatedController));

        private UserProfile _authenticatedUser;

        public UserProfile AuthenticatedUser
        {
            get
            {
                if (_authenticatedUser == null)
                {
                    // get the profile
                    if (User != null && User.Identity.IsAuthenticated)
                    {
                        _authenticatedUser = GetCurrentUser();
                    }
                }
               

                return _authenticatedUser;

            }
            set { _authenticatedUser = value; }
        }

        #region Events

        protected override void OnException(ExceptionContext filterContext)
        {
            if (AuthenticatedUser != null)
            {
                log4net.GlobalContext.Properties["UserName"] = AuthenticatedUser.UserName;
                log4net.GlobalContext.Properties["CompanyName"] = AuthenticatedUser.Company.CompanyName;
            }

            log.Error(filterContext.Exception.Message, filterContext.Exception);

            filterContext.Result = new ViewResult()
            {
                ViewName = "Error"
            };
            
            filterContext.ExceptionHandled = true;
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            SetViewBagDefaultProperties();
            base.OnActionExecuted(filterContext);
        }

        #endregion


        #region Method

        private UserProfile GetCurrentUser()
        {
            var context = new QcContext();

            if (this.User.Identity.Name != "")
            {
                var userProfile = context.UserProfiles.FirstOrDefault(o => o.UserName == this.User.Identity.Name);

                return userProfile;
            }

            return null;
        }

        private void SetViewBagDefaultProperties()
        {
            if (this.AuthenticatedUser != null)
            {
                ViewBag.AuthenticatedUserName = this.AuthenticatedUser.UserName;
            }
        }



      
        #endregion

    }

}
