﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using log4net;
using Microsoft.Ajax.Utilities;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Enum;
using Mobi.QualityControl.Framework.Helpers;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Web.Helpers;
using Mobi.QualityControl.Web.Models;
using Mobi.QualityControl.Web.ViewModels.Results;

namespace Mobi.QualityControl.Web.Controllers
{
    [Authorize]
    public class ResultsController : AuthenticatedController
    {
        ILog log = log4net.LogManager.GetLogger(typeof(ResultsController));

        //
        // GET: /Results/
        private QcContext context = new QcContext();

        public ActionResult Index()
        {
           
            var model = new ResultViewIndexModel();
            var resultsViewQuery = context.ResultViews.Select(o => new
            {
                o.QuestionSet.Company.CompanyId,
                o.ResultViewId,
                o.ResultViewName

            }).Where(o => o.CompanyId == AuthenticatedUser.Company.CompanyId);
            foreach (var resultView in resultsViewQuery)
            {
                model.ResultViewIndexItems.Add(new ResultViewIndexItem()
                {
                    ResultViewId = resultView.ResultViewId,
                    ResultViewName = resultView.ResultViewName,
                });

            }
            var relationshipResults = context.ResultViewShares.Select(o => new
            {
                o.Company.CompanyId,
                o.ResultView.ResultViewId,
                o.ResultView.ResultViewName
            }).Where(o => o.CompanyId == AuthenticatedUser.Company.CompanyId);
            foreach (var relationshipResult in relationshipResults.ToList())
            {
                model.ResultViewSharedIndexItems.Add(new ResultViewSharedIndexItem()
                {
                    ResultViewId = relationshipResult.ResultViewId,
                    ResultViewName = relationshipResult.ResultViewName,
                });
            }

            return View(model);
        }

        public ActionResult ResultsSharesForCompany(Guid id)
        {
            var model = new ResultViewIndexModel();
            var relationshipResults = context.ResultViewShares.Select(o => new
            {
                o.Company.CompanyId,
               ResultViewCompanyId = o.ResultView.QuestionSet.Company.CompanyId,
               o.ResultView.ResultViewId,
               o.ResultView.ResultViewName
            }).Where(o => o.CompanyId == AuthenticatedUser.Company.CompanyId && o.ResultViewCompanyId == id);
            foreach (var relationshipResult in relationshipResults.ToList())
            {
                model.ResultViewSharedIndexItems.Add(new ResultViewSharedIndexItem()
                {
                    ResultViewId = relationshipResult.ResultViewId,
                    ResultViewName = relationshipResult.ResultViewName,
                });
            }

            return View(model);


        }

        public ActionResult Create(Guid? questionSetId)
        {
            var model = new ResultViewEditModel();
            model.ScreenType = ScreenType.Create;
            if (questionSetId != null)
            {
                model.QuestionSetId = (Guid)questionSetId;
                model.QuestionSetName = context.QuestionSets.Select(o => new
                {
                    o.QuestionSetId,
                    o.QuestionSetName
                    }).FirstOrDefault(o => o.QuestionSetId == model.QuestionSetId).QuestionSetName;
            }
            else
            {
                model.QuestionSetList = new DropDownHelper().GetQuestionSetDropDown(AuthenticatedUser.Company.CompanyId);
            }


            return View("Edit", model);
        }

        public ActionResult Details(Guid id)
        {
            var model = new ResultViewEditModel();
            model.ScreenType = ScreenType.Details;
            PopulateEditModel(id, model);
           return View("Edit", model);
            
        }

        public ActionResult Edit(Guid id)
        {
            var model = new ResultViewEditModel();
            model.ScreenType = ScreenType.Edit;
            PopulateEditModel(id, model);

            return View(model);

        }

        [HttpPost]
        public ActionResult Edit(ResultViewEditModel model)
        {
            ResultView resultView;
            if (model.ScreenType == ScreenType.Create)
            {
                resultView = new ResultView();
                resultView.ResultViewId = Guid.NewGuid();
                resultView.QuestionSet = context.QuestionSets.FirstOrDefault(o => o.QuestionSetId == model.QuestionSetId);
            }
            else
            {
                resultView = context.ResultViews.FirstOrDefault(o => o.ResultViewId == model.ResultViewId && o.QuestionSet.Company.CompanyId == this.AuthenticatedUser.Company.CompanyId);
            }
            resultView.ResultViewName = model.ResultViewName;

            if (model.ScreenType == ScreenType.Create)
            {

                context.ResultViews.Add(resultView);

            }
            context.SaveChanges();
            TempData["SuccessMessage"] = "The Result View has been successfully updated.";
            return RedirectToAction("Edit", new { id = resultView.ResultViewId });
        }


        [HttpPost]
        public ActionResult AddRule(ResultViewItemModel model)
        {
            var resultViewItem = new ResultViewItem();
            resultViewItem.ResultViewItemId = Guid.NewGuid();
            resultViewItem.Question = context.Questions.FirstOrDefault(o => o.QuestionId == model.QuestionId);
            resultViewItem.ExpressionValue = model.ExpressionValue;
            resultViewItem.ViewEvalutionType = model.ViewEvalutionType;
            var resultView = context.ResultViews.FirstOrDefault(o => o.ResultViewId == model.ResultViewId);
            resultView.ResultViewItems.Add(resultViewItem);
            
            context.SaveChanges();
            return RedirectToAction("Edit", new { id = model.ResultViewId });
        }

        [HttpPost]
        public ActionResult AddDisplayColumn(ResultViewDisplayItemModel model)
        {
            var resultViewDisplayItem = new ResultViewDisplayItem();
            resultViewDisplayItem.ResultViewDisplayItemId = Guid.NewGuid();
            resultViewDisplayItem.Question = context.Questions.FirstOrDefault(o => o.QuestionId == model.QuestionId);
            var resultView = context.ResultViews.FirstOrDefault(o => o.ResultViewId == model.ResultViewId);
            resultView.ResultViewDisplayItems.Add(resultViewDisplayItem);

            context.SaveChanges();
            return RedirectToAction("Edit", new { id = model.ResultViewId });
        }


        [HttpPost]
        public ActionResult AddCompanyShareView(ResultViewShareModel model)
        {
            var resultViewShare = new ResultViewShare();
            resultViewShare.ResultViewShareId = Guid.NewGuid();
            resultViewShare.Company = context.Companies.FirstOrDefault(o => o.CompanyId == model.CompanyId);
            resultViewShare.ResultView = context.ResultViews.FirstOrDefault(o => o.ResultViewId == model.ResultViewId);
            context.ResultViewShares.Add(resultViewShare);
            context.SaveChanges();
            return RedirectToAction("Edit", new { id = model.ResultViewId });

        }



        private void PopulateEditModel(Guid id, ResultViewEditModel model)
        {
            var resultViewQuery = context.ResultViews.Select(o => new
            {
                o.ResultViewId,
                o.QuestionSet.Company.CompanyId,
                o.QuestionSet.QuestionSetName,
                o.QuestionSet.QuestionSetId,
                o.ResultViewName,
                ResultViewItems = o.ResultViewItems.Select(rvi => new
                {
                    rvi.ResultViewItemId,
                    rvi.Question.QuestionText,
                    rvi.ViewEvalutionType,
                    rvi.ExpressionValue

                }),
                ResultViewDisplayItems = o.ResultViewDisplayItems.Select(rvd => new
                {
                    rvd.Question.QuestionId,
                    rvd.Question.QuestionText,
                    rvd.ResultViewDisplayItemId
                }),
                ResultViewShares = o.ResultViewShares.Select(rvs => new
                {
                    rvs.Company.CompanyId,
                    rvs.Company.CompanyName,
                    rvs.ResultView.ResultViewId,
                    rvs.ResultViewShareId
                })
            }).FirstOrDefault(o => o.ResultViewId == id && o.CompanyId == AuthenticatedUser.Company.CompanyId);
            if (resultViewQuery != null)
            {
                model.ResultViewName = resultViewQuery.ResultViewName;
                model.QuestionSetName = resultViewQuery.QuestionSetName;
                model.QuestionSetId = resultViewQuery.QuestionSetId;
                model.ResultViewId = resultViewQuery.ResultViewId;
                model.QuestionList = new DropDownHelper().GetQuestionDropDown(AuthenticatedUser.Company.CompanyId, model.QuestionSetId);
                model.EvaluationTypes = new DropDownHelper().GetViewEvalutionDropDown();
                model.CompaniesList = new DropDownHelper().GetAllRelationshipResultCompaniesDropDown(AuthenticatedUser.Company.CompanyId,id);

                foreach (var resultViewItem in resultViewQuery.ResultViewItems)
                {
                    model.ResultViewItemModels.Add(new ResultViewItemModel()
                    {
                        ResultViewItemId = resultViewItem.ResultViewItemId,
                        QuestionText = resultViewItem.QuestionText,
                        ViewEvalutionType = resultViewItem.ViewEvalutionType,
                        ExpressionValue = resultViewItem.ExpressionValue
                    });
                }

                foreach (var resultViewDisplayItem in resultViewQuery.ResultViewDisplayItems)
                {
                    model.ResultViewDisplayItemModels.Add(new ResultViewDisplayItemModel()
                    {
                        QuestionId = resultViewDisplayItem.QuestionId,
                        QuestionText = resultViewDisplayItem.QuestionText,
                        ResultViewDisplayItemID = resultViewDisplayItem.ResultViewDisplayItemId
                    });
                }

                foreach (var resultViewShare in resultViewQuery.ResultViewShares)
                {
                    model.ResultViewShareModels.Add(new ResultViewShareModel()
                    {
                        CompanyId = resultViewShare.CompanyId,
                        CompanyName = resultViewShare.CompanyName,
                       ResultViewId = resultViewShare.ResultViewId,
                       ResultViewShareId = resultViewShare.ResultViewShareId
                    });
                }
            }

        }


        public ActionResult ResultView(Guid id)
        {
            var model = new TransactionSetResultModel();
            var questionList = new List<string>();
            //Get ResultView 
            var resultViewQuery = context.ResultViews.Select(o => new
            {
                o.ResultViewId,
                ResultViewDisplayItems = o.ResultViewDisplayItems.Select(rvdi => new
                {
                    Question = rvdi.Question
                }),
                o.QuestionSet.QuestionSetId,
                ResultViewItems = o.ResultViewItems.Select(rvi => new
                {
                    rvi.Question.QuestionId,
                    rvi.ViewEvalutionType,
                    rvi.ExpressionValue
                })

            }).FirstOrDefault(o => o.ResultViewId == id);
            if (resultViewQuery != null)
            {
                foreach (var resultViewDisplayItem in resultViewQuery.ResultViewDisplayItems.ToList())
                {
                    model.QuestionResultHeaders.Add(new QuestionResultHeader()
                    {
                        OrderPosition = resultViewDisplayItem.Question.OrderPosition,
                        QuestionId = resultViewDisplayItem.Question.QuestionId,
                        QuestionName = resultViewDisplayItem.Question.QuestionText
                    });
                   questionList.Add(resultViewDisplayItem.Question.QuestionId.ToString());
                }

                var resultViewExpressions = resultViewQuery.ResultViewItems.ToList();

                var transactionSetQuery =
                    context.TransactionSets.Select(o => new
                    {
                        o.TransactionSetId,
                        Transactions = o.Transactions.Select(tr => new
                        {
                            tr.Question.QuestionId,
                            Result = tr.Result,
                            ControlName = tr.Question.ControlType.ControlName,
                            OrderPosition = tr.Question.OrderPosition
                        }),
                        o.QuestionSet.QuestionSetId
                    }).Where(
                        o => o.QuestionSetId == resultViewQuery.QuestionSetId);

                foreach (var transactionSet in transactionSetQuery.ToList())
                {
                    var transactionSetItem = new TransactionSetItem();
                    var transactionListItems = new List<TransactionResultItem>();
                    transactionSetItem.TransactionSetId = transactionSet.TransactionSetId;
                    var isValid = true;
                    foreach (var transaction in transactionSet.Transactions.ToList())
                    {

                        //Check rules and display
                        var expression = resultViewExpressions.FirstOrDefault(o => o.QuestionId == transaction.QuestionId);
                        if (expression != null)
                        {
                            if (expression.ViewEvalutionType == ViewEvalutionType.Equals)
                            {
                                if (expression.ExpressionValue != transaction.Result)
                                {
                                    isValid = false;
                                }

                            }

                        }
                        if (questionList.Contains(transaction.QuestionId.ToString()))
                        {
                            transactionSetItem.TransactionResultItems.Add(new TransactionResultItem()
                            {
                                ControlType = transaction.ControlName,
                                OrderPosition = transaction.OrderPosition,
                                QuestionId = transaction.QuestionId,
                                ResultValue = transaction.Result
                            });
                        }
                        
                        
                    }
                    if (isValid)
                    {
                        model.TransactionSetItems.Add(transactionSetItem);
                    }
                }
            }
            return View("ViewAllForQuestionSet", model);
        }





        public ActionResult ViewAllForQuestionSet(Guid id)
        {

            var model = new TransactionSetResultModel();
                context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

            var transactionSetsQuery = context.TransactionSets.Select(o => new
            {
                o.TransactionSetId,
                TransactionCount = o.Transactions.Count(),
                Transactions = o.Transactions.Select(t => new
                {
                    t.Question.ControlType.ControlName,
                    t.Question.OrderPosition,
                    t.Question.QuestionId,
                    t.Result
                }).ToList(),
                o.QuestionSet.QuestionSetId
            }).Where(o => o.QuestionSetId == id).Take(1000).ToList();
            var questions = context.Questions.Select(o => new
            {
                o.OrderPosition,
                o.QuestionSet.QuestionSetId,
                o.QuestionId,
                o.QuestionText
            }).Where(o => o.QuestionSetId == id);
            foreach (var question in questions)
            {
                model.QuestionResultHeaders.Add(new QuestionResultHeader()
                {
                    OrderPosition = question.OrderPosition,
                    QuestionId = question.QuestionId,
                    QuestionName = question.QuestionText.PadRight(20).Substring(0, 10)
                });

            }
            foreach (var transactionSet in transactionSetsQuery.ToList())
            {
                var transactionSetItem = new TransactionSetItem();
                transactionSetItem.TransactionSetId = transactionSet.TransactionSetId;
                if (transactionSet.TransactionCount > 0)
                {
                    foreach (var transaction in transactionSet.Transactions)
                    {
                        transactionSetItem.TransactionResultItems.Add(new TransactionResultItem()
                        {
                            ControlType = transaction.ControlName,
                            OrderPosition = transaction.OrderPosition,
                            QuestionId = transaction.QuestionId,
                            ResultValue = transaction.Result
                        });
                    }
                }
                model.TransactionSetItems.Add(transactionSetItem);
            }
            return View(model);
        }

    }
}
