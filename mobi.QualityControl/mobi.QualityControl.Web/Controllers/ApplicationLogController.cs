﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Web.ViewModels.ApplicationLogs;

namespace Mobi.QualityControl.Web.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ApplicationLogController : Controller
    {
        private QcContext context = new QcContext();
        //
        // GET: /ErrorLog/
        public ActionResult Index(bool? includeDismissed)
        {
            var model = new ApplicationIndexModel();
            model.IncludeDismissed = includeDismissed ?? false;
            var applogsQuery = context.ApplicationLogs.ToList();

            if (includeDismissed == false || includeDismissed == null)
            {
                applogsQuery.Where(o => !o.IsDismissed);
            }

            foreach (var log in applogsQuery.ToList())
            {
                model.ApplicationLogItems.Add(new ApplicationLogItem()
                {
                    ApplicationId = log.ApplicationLogId,
                    Company = log.Company,
                    CreationDate = log.CreationDate,
                    Exception = log.Exception,
                    Level = log.Level,
                    Logger = log.Logger,
                    Message = log.Message,
                    Thread = log.Thread,
                    User = log.User
                });
            }


            return View(model);
        }

        public ActionResult DismissLog(Guid id)
        {
            var log = context.ApplicationLogs.FirstOrDefault(o => o.ApplicationLogId == id);
            if (log != null)
            {
                log.IsDismissed = true;
                context.SaveChanges();
            }
            return RedirectToAction("Index", new {includeDismissed = false});

        }
	}
}