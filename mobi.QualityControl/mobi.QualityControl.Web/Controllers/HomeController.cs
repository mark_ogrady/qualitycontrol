﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;
using log4net;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Web.Models.Home;
using Mobi.QualityControl.Web.ViewModels.Home;


namespace Mobi.QualityControl.Web.Controllers
{
    

    public class HomeController : Controller
    {
        ILog log = log4net.LogManager.GetLogger(typeof(HomeController));

        private QcContext context = new QcContext();
        public ActionResult Index()
        {
            HomeViewModel model = new HomeViewModel();
           

            return View(model);
        }

        public ActionResult Developer()
        {
            ViewBag.Title = "About Quality Control";

            return View();
        }

        public ActionResult Pricing()
        {
            ViewBag.Title = "Pricing Site";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact";
            var model = new Mobi.QualityControl.Web.ViewModels.Home.Contact();
            model.Number1 = new Random().Next(1, 10);
            model.Number2 = new Random().Next(1, 10);



            return View(model);
        }

        [HttpPost]
        public ActionResult Contact(Mobi.QualityControl.Web.ViewModels.Home.Contact model)
        {
            if (Convert.ToInt32(model.Total) != (model.Number1 + model.Number2))
                ModelState.AddModelError("","The calculation you have entered is incorrect.");

            if (ModelState.IsValid)
            {
                string message = string.Format("{0} {1} {2} {3} {4}", model.CompanyName, model.Email, model.Name,
                    model.Message, model.PhoneNumber);
                var internalMessage = new InternalMessage();
                internalMessage.InternalMessageId = Guid.NewGuid();
                internalMessage.CompanyName = model.CompanyName;
                internalMessage.Name = model.Name;
                internalMessage.EmailAddress = model.Email;
                internalMessage.PhoneNumber = model.PhoneNumber;
                internalMessage.Message = model.Message;
                internalMessage.CreationDate = DateTime.UtcNow;
                context.InternalMessages.Add(internalMessage);
                context.SaveChanges();

                try
                {
                    new Framework.Mail.Gmail().SendMail("mark@markogrady.com", model.Email, "Contact through Site", message);
                }
                catch (Exception)
                {
                    //TODO Log Error
                    
                }
                
                TempData["Success"] = "Thank you for you email. You will be contacted shortly";
            }
            return View(model);
        }

        

        [HttpPost]
        public ActionResult RegisterInterest(RegisterInterestModel model)
        {
            var internalMessage = new InternalMessage();
            internalMessage.InternalMessageId = Guid.NewGuid();
            internalMessage.EmailAddress = model.EmailAddress;
            internalMessage.Message = "RegisterInterest";
            internalMessage.CreationDate = DateTime.UtcNow;
            context.InternalMessages.Add(internalMessage);
            context.SaveChanges();
            new Framework.Mail.Gmail().SendMail("mark@markogrady.com", model.EmailAddress, "RegisterInterest", model.EmailAddress);
            return Content("Thank you for your interest.");
            TempData["SuccessMessage"] = "Thank you for your interest we will be in contact soon.";
            if (model.ReturnUrl != "")
            {
                return RedirectToAction(model.ReturnUrl);
            }
            return RedirectToAction("Index", "Home");

        }
    }
}
