﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Model;

namespace Mobi.QualityControl.Web.Helpers
{
    public class UserHelper
    {
        public UserProfile CurrentUserProfile(string userName)
        {
            using (var context = new QcContext())
            {

                try
                {
                    return context.UserProfiles.FirstOrDefault(o => o.UserName == userName);
                }
                catch (Exception)
                {

                    return new UserProfile();
                }
            }
        }

        public int GetNotificationCount(string userName)
        {
            return 0;
            using (var context = new QcContext())
            {
                var user = context.UserProfiles.FirstOrDefault(o => o.UserName == userName);
                if (user == null) return 0;
                var notifications = context.Notifications.Where(o => o.ToId == user.UserId);
                if (notifications.Any())
                {
                    return notifications.Count();
                }

                return 0;

            }
        }
    }
}