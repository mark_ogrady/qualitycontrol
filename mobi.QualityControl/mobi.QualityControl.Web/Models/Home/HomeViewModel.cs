﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Mobi.QualityControl.Web.Models.Home
{
    public class HomeViewModel
    {
        public List<SliderItem> SliderItems { get; set; }
    }
}