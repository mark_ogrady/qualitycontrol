﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.Models.Home
{
    public class SliderItem
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public string ImageUrl { get; set; }
        public string LinkText { get; set; }
        public string LinkUrl { get; set; }
        public string Animation { get; set; }
        public string Active { get; set; }
    }
}