﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.Models
{
    interface IViewModel
    {
        ScreenType ScreenType { get; set; }
    }
}