﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using Mobi.QualityControl.Framework.DataContext;
using WebMatrix.WebData;

namespace Mobi.QualityControl.Web.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
    {
        private static SimpleMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Ensure ASP.NET Simple Membership is initialized only once per app start
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
        }

        private class SimpleMembershipInitializer
        {
            public SimpleMembershipInitializer()
            {
                Database.SetInitializer<QcContext>(null);

                try
                {
                    using (var context = new QcContext())
                    {
                        if (!context.Database.Exists())
                        {
                            // Create the SimpleMembership database without Entity Framework migration schema
                            ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                        }
                    }

                    WebSecurity.InitializeDatabaseConnection("QcConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);

                    if (!Roles.RoleExists("Administrator"))
                        Roles.CreateRole("Administrator");

                    if (!Roles.RoleExists("Standard"))
                        Roles.CreateRole("Standard");

                    if (!WebSecurity.UserExists("mark@markogrady.com"))
                        WebSecurity.CreateUserAndAccount("mark@markogrady.com", "password");

                    if (!Roles.GetRolesForUser("mark@markogrady.com").Contains("Administrator"))
                        Roles.AddUsersToRoles(new[] { "mark@markogrady.com" }, new[] { "Administrator" });

                    if (!WebSecurity.UserExists("st@qc.mobi"))
                        WebSecurity.CreateUserAndAccount("st@qc.mobi", "password");

                    if (!Roles.GetRolesForUser("st@qc.mobi").Contains("Standard"))
                        Roles.AddUsersToRoles(new[] { "st@qc.mobi" }, new[] { "Standard" });

                    if (!WebSecurity.UserExists("Derek.Fraser@herrco.co.uk"))
                        WebSecurity.CreateUserAndAccount("Derek.Fraser@herrco.co.uk", "password");

                    if (!Roles.GetRolesForUser("Derek.Fraser@herrco.co.uk").Contains("Standard"))
                        Roles.AddUsersToRoles(new[] { "Derek.Fraser@herrco.co.uk" }, new[] { "Standard" });
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
                }
            }
        }
    }
}
