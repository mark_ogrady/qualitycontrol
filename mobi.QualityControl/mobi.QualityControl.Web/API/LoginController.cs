﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Web.Filters;
using Mobi.QualityControl.Web.Models;
using WebMatrix.WebData;

namespace Mobi.QualityControl.Web.API
{
    [InitializeSimpleMembership]
    public class LoginController : ApiController
    {
        private QcContext _qcContext = new QcContext();
        // GET: Login
        public IHttpActionResult Login(dynamic credentials)
        {
            var model = new ExternalUserModel();

            string username = credentials.username;
            string password = credentials.password;

            if (WebSecurity.Login(username, password))
            {
                var user = _qcContext.UserProfiles.FirstOrDefault(o => o.UserName == username);
                if (user != null)
                {
                    var sessionId = Guid.NewGuid();
                    model.UserId = user.UserId;
                    model.Email = user.UserName;
                    model.Name = user.FirstName + " " + user.LastName;
                    model.Token = sessionId;
                    model.ExpirationDate = DateTime.UtcNow.AddDays(2);

                    var userSession = new UserSession();
                    userSession.UserSessionId = sessionId;
                    userSession.CreatedDate = DateTime.UtcNow;
                    userSession.ExpirationDate = model.ExpirationDate;
                    userSession.UserProfile = user;


                    _qcContext.UserSessions.Add(userSession);
                    _qcContext.SaveChanges();
                }
            }
            else
            {
                model.ErrorMessage = "Login Failed Message";
            }





            return Json(model);
        }
    }
}