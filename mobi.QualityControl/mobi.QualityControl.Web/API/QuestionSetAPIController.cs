﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Web.ViewModels.QuestionSet;

namespace Mobi.QualityControl.Web.API
{
    public class QuestionSetAPIController : ApiController
    {
        private QcContext _context = new QcContext();

        public IHttpActionResult GetQuestionSets()
        {
            var model = new QuestionSetIndexModel();
            var questionSets = _context.QuestionSets.Select(o => new
            {
               QuestionCount = o.Questions.Count,
               QuestionSetId = o.QuestionSetId,
               QuestionSetName = o.QuestionSetName,
               TransactionSetCount = o.TransactionSets.Count,
               Questions = o.Questions.Select(q => 
                   q.QuestionText

               ),

            }).Take(30).ToList();
            foreach (var questionSet in questionSets)
            {
                var questionSetItem = new QuestionSetItem();

                questionSetItem.QuestionCount = questionSet.QuestionCount;
                questionSetItem.QuestionSetId = questionSet.QuestionSetId;
                questionSetItem.QuestionSetName = questionSet.QuestionSetName;
                questionSetItem.TransactionCount = questionSet.TransactionSetCount;
                

                foreach (var question in questionSet.Questions)
                {
                  
                }
                model.QuestionSetItems.Add(questionSetItem);
            }

            return Json(model.QuestionSetItems);
        }

    }
}
