﻿using System.Web;
using System.Web.Optimization;

namespace Mobi.QualityControl.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));



            #region Front End
            bundles.Add(new StyleBundle("~/Styles/frontcss").Include(
                "~/Content/Front/plugins/owl-carousel/owl.carousel.css",
                "~/Content/Front/plugins/owl-carousel/owl.theme.css",
                "~/Content/Front/plugins/onescroll/css/demo.css",
                "~/Content/Front/plugins/onescroll/css/component.css",
                "~/Content/Front/plugins/headereffects/css/component.css",
                "~/Content/Front/plugins/headereffects/css/normalize.css",
                "~/Content/Front/plugins/pace/pace-theme-flash.css",
                "~/Content/Front/plugins/boostrapv3/css/bootstrap.css",
                "~/Content/Front/plugins/boostrapv3/css/bootstrap-theme.css",
                "~/Content/Front/css/style.css",
                "~/Content/Front/css/magic_space.css",
                "~/Content/Front/css/responsive.css",
                "~/Content/Front/css/animate.min.css",
                "~/Content/Front/plugins/rs-plugin/css/settings.css",
                "~/Content/Front/plugins/cookiecutter/cookiecuttr.css"

             ));

            bundles.Add(new ScriptBundle("~/bundles/front").Include(
                "~/Content/Front/plugins/jquery-1.8.3.js",
                "~/Content/Front/plugins/rs-plugin/js/jquery.themepunch.plugins.js",
                "~/Content/Front/plugins/rs-plugin/js/jquery.themepunch.revolution.js",
                "~/Content/Front/plugins/boostrapv3/js/bootstrap.js",
                "~/Content/Front/plugins/pace/pace.js",
                "~/Content/Front/plugins/jquery-unveil/jquery.unveil.js",
                "~/Content/Front/plugins/owl-carousel/owl.carousel.js",
                "~/Content/Front/plugins/modernizr.custom.js",
                "~/Content/Front/plugins/waypoints.js",
                "~/Content/Front/plugins/parrallax/js/jquery.parallax-1.1.3.js",
                "~/Content/Front/plugins/jquery-nicescroll/jquery.nicescroll.js",
                "~/Content/Front/plugins/jquery-appear/jquery.appear.js",
                "~/Content/Front/plugins/jquery-numberAnimate/jquery.animateNumbers.js",
                "~/Content/Front/plugins/cookiecutter/jquery.cookie.js",
                "~/Content/Front/plugins/cookiecutter/jquery.cookiecuttr.js"
));
            #endregion


            #region App

            bundles.Add(new StyleBundle("~/Styles/appcss").Include(
                "~/Content/Admin/plugins/dropzone/css/dropzone.css",
                "~/Content/Admin/plugins/gritter/css/jquery.gritter.css",
                "~/Content/Admin/plugins/bootstrap-datepicker/css/datepicker.css",
                "~/Content/Admin/plugins/jquery-ricksaw-chart/css/rickshaw.css",
                "~/Content/Admin/plugins/jquery-morris-chart/css/morris.css",
                "~/Content/Admin/plugins/jquery-slider/css/jquery.sidr.light.css",
                "~/Content/Admin/plugins/bootstrap-select2/select2.css",
                "~/Content/Admin/plugins/jquery-jvectormap/css/jquery-jvectormap-1.2.2.css",
                "~/Content/Admin/plugins/boostrap-checkbox/css/bootstrap-checkbox.css",
                "~/Content/Admin/plugins/gritter/css/jquery.gritter.css",
                "~/Content/Admin/plugins/ios-switch/ios7-switch.css",
                "~/Content/Admin/plugins/imagepicker/css/image-picker.css",
                "~/Content/Admin/plugins/jquery-nestable/jquery.nestable.css",
                "~/Content/Admin/plugins/jquery-datatable/css/jquery.dataTables.css",
                "~/Content/Admin/plugins/datatables-responsive/css/datatables.responsive.css",
                "~/Content/Admin/plugins/jquery-notifications/css/messenger.css",
                "~/Content/Admin/plugins/jquery-notifications/css/messenger-theme-flat.css",
                "~/Content/Admin/plugins/jquery-notifications/css/location-sel.css",
                "~/Content/Admin/plugins/boostrapv3/css/bootstrap.css",
                "~/Content/Admin/plugins/boostrapv3/css/bootstrap-theme.css",
                "~/Content/Admin/css/animate.css",
                "~/Content/Admin/css/style.css",
                "~/Content/Admin/css/responsive.css",
                "~/Content/Admin/css/custom-icon-set.css"
                ));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                 "~/Content/Admin/plugins/jquery-1.8.3.js",
    "~/Content/Admin/plugins/jquery-ui/jquery-ui-1.10.1.custom.js",
    "~/Content/Admin/plugins/boostrapv3/js/bootstrap.js",
    "~/Content/Admin/plugins/breakpoints.js",
    "~/Content/Admin/plugins/jquery-unveil/jquery.unveil.js",
    "~/Content/Admin/plugins/jquery.touchpunch/jquery.ui.touch-punch.js",
    "~/Content/Admin/plugins/pace/pace.js",
    "~/Content/Admin/plugins/jquery-slimscroll/jquery.slimscroll.js",
    "~/Content/Admin/plugins/jquery-numberAnimate/jquery.animateNumbers.js",
    "~/Content/Admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js",
    "~/Content/Admin/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js",
    "~/Content/Admin/plugins/jquery-slimscroll/jquery.slimscroll.js",
    "~/Content/Admin/plugins/jquery-block-ui/jqueryblockui.js",
    "~/Content/Admin/plugins/bootstrap-select2/select2.js",
    "~/Content/Admin/plugins/jquery-ricksaw-chart/js/raphael-js",
    "~/Content/Admin/plugins/jquery-ricksaw-chart/js/d3.v2.js",
    "~/Content/Admin/plugins/jquery-ricksaw-chart/js/rickshaw.js",
    "~/Content/Admin/plugins/jquery-morris-chart/js/morris.js",
    "~/Content/Admin/plugins/jquery-easy-pie-chart/js/jquery.easypiechart.js",
    "~/Content/Admin/plugins/jquery-slider/jquery.sidr.js",
    "~/Content/Admin/plugins/jquery-jvectormap/js/jquery-jvectormap-1.2.2.js",
    "~/Content/Admin/plugins/jquery-jvectormap/js/jquery.vmap.europe.js",
    "~/Content/Admin/plugins/jquery-sparkline/jquery-sparkline.js",
    "~/Content/Admin/plugins/jquery-flot/jquery.flot.js",
    "~/Content/Admin/plugins/jquery-flot/jquery.flot.animator.js",
    "~/Content/Admin/plugins/skycons/skycons.js",
    "~/Content/Admin/plugins/jquery-nestable/jquery.nestable.js",
    "~/Content/Admin/plugins/jquery-datatable/js/jquery.dataTables.js",
    "~/Content/Admin/plugins/jquery-datatable/extra/js/TableTools.js",
    "~/Content/Admin/plugins/datatables-responsive/js/datatables.responsive.js",
    "~/Content/Admin/plugins/datatables-responsive/js/lodash.js",
    "~/Content/Admin/plugins/jquery-notifications/js/messenger.js",
    "~/Content/Admin/plugins/jquery-notifications/js/messenger-theme-future.js",
    "~/Content/Admin/plugins/jquery-notifications/js/demo/location-sel.js",
    "~/Content/Admin/plugins/jquery-notifications/js/demo/theme-sel.js",
    "~/Content/Admin/plugins/jquery-notifications/js/demo/demo.js",
    "~/Content/Admin/js/notifications.js",
    "~/Content/Admin/plugins/jquery-datatable/extra/js/ZeroClipboard.js",
    "~/Content/Admin/js/datatables.js"


                ));
            System.Web.Optimization.BundleTable.EnableOptimizations = false;
            #endregion
        }
    }
}