﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Web;
using System.Web.Http;
using Mobi.QualityControl.Web.Filters;
using System.Web.Mvc;

namespace Mobi.QualityControl.Web.App_Start
{
    public class CustomGlobalConfig
    {
        public static void Customise(HttpConfiguration config)
        {
            config.Filters.Add(new ValidationActionFilter());
           
        }
    }
}