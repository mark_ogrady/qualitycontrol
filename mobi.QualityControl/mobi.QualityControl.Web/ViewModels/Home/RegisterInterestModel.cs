﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.Home
{
    public class RegisterInterestModel
    {
        public string ReturnUrl { get; set; }
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }
    }
}