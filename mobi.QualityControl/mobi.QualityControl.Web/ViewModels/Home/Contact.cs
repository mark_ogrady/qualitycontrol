﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.Home
{
    public class Contact
    {
       
        public string Name { get; set; }
        public string CompanyName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

       
        public string PhoneNumber { get; set; }
        public string Message { get; set; }

        public int Number1 { get; set; }
        public int Number2 { get; set; }
        [Required]
        public string Total { get; set; }
    }
}