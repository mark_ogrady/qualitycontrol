﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.Menu
{
    public class MenuViewModel
    {
        public string SelectedMenu { get; set; }
        public IEnumerable<Mobi.QualityControl.Framework.Model.Menu> Menus { get; set; }
    }
}