﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.Notification
{
    public class NotificationIndexModel
    {
        public NotificationIndexModel()
        {
            NotificationIndexItems = new List<NotificationIndexItem>();
        }

        public List<NotificationIndexItem> NotificationIndexItems { get; set; }

    }

    public class NotificationIndexItem
    {
        public Guid NotificiationId { get; set; }
        public string Subject { get; set; }
        public string MessageBody { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public bool IsDismissed { get; set; }
    }
        
}