﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Web.Models;

namespace Mobi.QualityControl.Web.ViewModels.Company
{
    public class CompanyEditModel : IViewModel
    {
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int NumberOfUsers { get; set; }

        public HttpPostedFileBase CompanyLogo { get; set; }
        public string CompanyWebsiteUrl { get; set; }

        public string CompanyAddress1 { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyAddress3 { get; set; }
        public string CompanyAddress4 { get; set; }
        public string PostCode { get; set; }

        public string ContactNumber { get; set; }

        public CompanySubscription SubscriptionType { get; set; }
        public List<SelectListItem> SubscriptionList { get; set; }
        public ScreenType ScreenType { get; set; }
    }
}