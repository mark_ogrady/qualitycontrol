﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Web.Models;

namespace Mobi.QualityControl.Web.ViewModels.Relationship
{
    public class RelationshipEditModel : IViewModel
    {
        public Guid RelationshipId { get; set; }
        public string CompanyName { get; set; }
        public DateTime AcceptedDate { get; set; }

        public ScreenType ScreenType { get; set; }
    }
}
