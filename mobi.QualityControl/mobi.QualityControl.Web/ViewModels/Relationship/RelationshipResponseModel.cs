﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.Relationship
{
    public class RelationshipResponseModel
    {
        public Guid CompanyRelationshipRequestId { get; set; }
    }
}