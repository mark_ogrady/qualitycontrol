﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.Relationship
{
    public class RelationshipIndexModel
    {
        public List<CompanyItem> CompanyItems { get; set; }
        public List<RelationshipRequestItem> RelationshipRequestItems { get; set; }
        public List<OutstandingRequestItem> OutstandingRequestItems { get; set; }

        public RelationshipIndexModel()
        {
            CompanyItems = new List<CompanyItem>();
            RelationshipRequestItems = new List<RelationshipRequestItem>();
            OutstandingRequestItems = new List<OutstandingRequestItem>();
        }
    }

    public class RelationshipRequestItem
    {
        public Guid RequestId { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Message { get; set; }
    }

    public class OutstandingRequestItem
    {
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Message { get; set; }
    }

    public class CompanyItem
    {
        public Guid CompanyId { get; set; }
        public String CompanyName { get; set; } 
    }
}