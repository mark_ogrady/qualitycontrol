﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.Relationship
{
    public class RelationshipViewRequestModel
    {
        public Guid CompanyRelationshipRequestId { get; set; }
        public string CompanyName { get; set; }
        public string UserName { get; set; }
        public string  Message { get; set; }

    }
}