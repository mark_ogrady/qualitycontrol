﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.Relationship
{
    public class RelationshipAddModel
    {
        public List<NewCompanyItem> NewCompanyItems { get; set; }
    }

    public class NewCompanyItem
    {
        public Guid CompanyId{ get; set; }
        public string CompanyName { get; set; }
        public string CompanyUrl { get; set; }
    }
}