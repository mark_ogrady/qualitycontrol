﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Mobi.QualityControl.Web.Models;

namespace Mobi.QualityControl.Web.ViewModels.ControlList
{
    public class ControlListEditModel : IViewModel
    {
        public Guid ControlListId { get; set; }
        [Display(Name = "Control List Name")]
        public string ControlListName { get; set; }

        public List<ControlListModelItem> ControlListModelItems { get; set; }

        public ControlListEditModel()
        {
            ControlListModelItems = new List<ControlListModelItem>();
        }

        public ScreenType ScreenType { get; set; }
        public bool HasImages { get; set; }

    }

    public class ControlListModelItem
    {
        public Guid ControlListItemId { get; set; }
        public Guid ControlListId { get; set; }
        public string ItemName { get; set; }
        public string ImageUrl {get;set;}
        public HttpPostedFileBase UploadFile { get; set; }

    }
}