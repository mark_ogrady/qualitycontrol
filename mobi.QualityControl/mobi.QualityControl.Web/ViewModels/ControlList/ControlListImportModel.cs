﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.ControlList
{
    public class ControlListImportModel
    {
        public HttpPostedFileBase ExcelFile { get; set; }
        public bool IsNew { get; set; }
        public bool DeleteMissing { get; set; }
        public Guid ControlListId { get; set; }
        
    }

    public class ImportListItem
    {
        public string Name { get; set; }
        public string Value { get; set; }
        
    }
}