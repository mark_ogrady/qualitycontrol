﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.ControlList
{
    public class ControlListIndexModel 
    {
        public List<ControlListIndexItem> ControlListIndexItems { get; set; }

        public ControlListIndexModel()
        {
            ControlListIndexItems = new List<ControlListIndexItem>();
        }
        
    }

    public class ControlListIndexItem
    {
        public Guid ControlListId { get; set; }
        public string ControlListName { get; set; }
        public int ItemCount { get; set; }
    }
}