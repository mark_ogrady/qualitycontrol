﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.Results
{
    public class TransactionSetResultModel
    {
        public TransactionSetResultModel()
        {
            TransactionSetItems = new List<TransactionSetItem>();
            QuestionResultHeaders = new List<QuestionResultHeader>();
        }

        public List<QuestionResultHeader> QuestionResultHeaders { get; set; }
        public Guid QuestionSetId { get; set; }
        public string QuestionSetName { get; set; }
        public List<TransactionSetItem> TransactionSetItems { get; set; }
        public int ColumnCount { get; set; }
    }

    public class QuestionResultHeader
    {
        public Guid QuestionId { get; set; }
        public int OrderPosition { get; set; }
        public string QuestionName { get; set; }

    }

    public class TransactionSetItem
    {
        
        public TransactionSetItem()
        {
            TransactionResultItems = new List<TransactionResultItem>();
        }

        public Guid TransactionSetId { get; set; }
        public DateTime CreationDateTime { get; set; }
        public List<TransactionResultItem> TransactionResultItems { get; set; }
    }

    public class TransactionResultItem
    {
        public Guid QuestionId { get; set; }
        public string ControlType { get; set; }
        public int OrderPosition { get; set; }

        public string ResultValue { get; set; }
    }
}