﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.Results
{
    public class ResultViewIndexModel
    {
        public ResultViewIndexModel()
        {
            ResultViewIndexItems = new List<ResultViewIndexItem>();
            ResultViewSharedIndexItems = new List<ResultViewSharedIndexItem>();
        }
        public List<ResultViewIndexItem> ResultViewIndexItems { get; set; }
        public List<ResultViewSharedIndexItem> ResultViewSharedIndexItems { get; set; }
    }

    public class ResultViewIndexItem
    {
        public Guid ResultViewId { get; set; }
        public string ResultViewName { get; set; }
        public int TransactionCount { get; set; }
    }

    public class ResultViewSharedIndexItem
    {
        public Guid ResultViewId { get; set; }
        public string ResultViewName { get; set; }
        public int TransactionCount { get; set; }
    }
}