﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using System.Web.Mvc;
using Mobi.QualityControl.Framework.Enum;
using Mobi.QualityControl.Web.Helpers;
using Mobi.QualityControl.Web.Models;

namespace Mobi.QualityControl.Web.ViewModels.Results
{
    public class ResultViewEditModel : IViewModel
    {
        public Guid QuestionSetId { get; set; }
        public string QuestionSetName { get; set; }
        public Guid ResultViewId { get; set; }
        public string ResultViewName { get; set; }
        public List<SelectListItem> QuestionSetList { get; set; }
        public List<ResultViewItemModel> ResultViewItemModels { get; set; }
        public List<ResultViewDisplayItemModel> ResultViewDisplayItemModels { get; set; }
        public List<ResultViewShareModel> ResultViewShareModels { get; set; }
        public List<SelectListItem> QuestionList { get; set; }
        public List<SelectListItem> EvaluationTypes { get; set; }
        public List<SelectListItem> CompaniesList { get; set; }
        public ResultViewEditModel()
        {
            QuestionSetList = new List<SelectListItem>();
            ResultViewItemModels = new List<ResultViewItemModel>();
            ResultViewDisplayItemModels = new List<ResultViewDisplayItemModel>();
            ResultViewShareModels = new List<ResultViewShareModel>();
            QuestionList = new List<SelectListItem>();
            EvaluationTypes = new List<SelectListItem>();
            CompaniesList = new List<SelectListItem>();
        }

        public ScreenType ScreenType { get; set; }
    }

    public class ResultViewItemModel
    {
        public Guid ResultViewId { get; set; }
        public Guid ResultViewItemId { get; set; }
        public Guid QuestionId { get; set; }
        public ViewEvalutionType ViewEvalutionType { get; set; }
        public string QuestionText { get; set; }
        public List<SelectListItem> QuestionList { get; set; }
        public List<SelectListItem> EvaulationTypes  { get; set; }
        public string ExpressionValue { get; set; }

        public ResultViewItemModel()
        {
            QuestionList = new List<SelectListItem>();
        }

    }

    public class ResultViewDisplayItemModel
    {
        public Guid ResultViewId { get; set; }
        public Guid ResultViewDisplayItemID { get; set; }
        public Guid QuestionId { get; set; }
        public string QuestionText { get; set; }

    }

    public class ResultViewShareModel
    {
        public Guid ResultViewId { get; set; }
        public Guid ResultViewShareId { get; set; }
        public string CompanyName { get; set; }
        public Guid CompanyId { get; set; }
    }
}