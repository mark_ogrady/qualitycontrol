﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mobi.QualityControl.Web.Models;

namespace Mobi.QualityControl.Web.ViewModels.Job
{
    public class JobEditModel : IViewModel
    {
        public JobEditModel()
        {
            JobTransactionSetItems = new List<JobTransactionSetItem>();
        }

        public Guid JobId { get; set; }
        public string JobCode { get; set; }
        public List<JobTransactionSetItem> JobTransactionSetItems { get; set; }  
        public ScreenType ScreenType { get; set; }
    }

    public class JobTransactionSetItem
    {
        public Guid TransactionSetId { get; set; }
        public string  TransRef { get; set; }

    }
}