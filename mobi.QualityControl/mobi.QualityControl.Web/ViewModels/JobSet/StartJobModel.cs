﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.JobSet
{
    public class StartJobModel
    {
        public Guid JobSetId { get; set; }
        public String JobReference { get; set; }
    }
}