﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mobi.QualityControl.Web.Models;

namespace Mobi.QualityControl.Web.ViewModels.JobSet
{
    public class JobSetEditModel :IViewModel
    {
        public JobSetEditModel()
        {
            JobSetQuestionSets = new List<JobSetQuestionSet>();
            QuestionSetListItems = new List<SelectListItem>();
            JobSetJobsItemModels = new List<JobSetJobsItemModel>();
        }

        public Guid JobSetId { get; set; }
        public string JobSetName { get; set; }
        public List<JobSetQuestionSet> JobSetQuestionSets { get; set; }
        public ScreenType ScreenType { get; set; }
        public List<SelectListItem> QuestionSetListItems { get; set; }
        public List<JobSetJobsItemModel> JobSetJobsItemModels { get; set; }
        
    }

    public class AddQuestionSetModel
    {
        public Guid JobSetId { get; set; }
        public Guid QuestionSetId { get; set; }
    }

    public class JobSetQuestionSet
    {
        public Guid QuestionSetId { get; set; }
        public string QuestionSetName { get; set; }

    }

    public class JobSetJobsItemModel
    {
        public Guid JobId { get; set; }
        public string JobReference { get; set; }
    }
}