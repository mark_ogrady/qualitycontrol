﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.JobSet
{
    public class JobSetIndexModel
    {
        public JobSetIndexModel()
        {
            JobSetIndexItems = new List<JobSetIndexItem>();
        }
        public List<JobSetIndexItem> JobSetIndexItems { get; set; }
    }

    public class JobSetIndexItem
    {
        public Guid JobSetId { get; set; }
        public string JobSetName { get; set; }
    }
}