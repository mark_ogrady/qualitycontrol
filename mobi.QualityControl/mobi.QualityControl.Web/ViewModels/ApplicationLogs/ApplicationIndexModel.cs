﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.ApplicationLogs
{
    public class ApplicationIndexModel
    {
        public List<ApplicationLogItem> ApplicationLogItems { get; set; }
        public bool IncludeDismissed { get; set; }
        public ApplicationIndexModel()
        {
            ApplicationLogItems = new List<ApplicationLogItem>();
        }
        
    }

    public class ApplicationLogItem
    {
        public Guid ApplicationId { get; set; }
        public DateTime CreationDate { get; set; }
        public string User { get; set; }
        public string Company { get; set; }
        public string Thread { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }

    }
}