﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Mobi.QualityControl.Framework.Migrations;
using Mobi.QualityControl.Web.Models;

namespace Mobi.QualityControl.Web.ViewModels.User
{
    public class ManageProfileModel : IViewModel
    {
        public ManageProfileModel()
        {
            Languages = new List<SelectListItem>();
        }

        public int UserProfileId { get; set; }
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public Guid CompanyId { get; set; }
        public Guid LanguageId { get; set; }
        public List<SelectListItem> Languages { get; set; }
        public ScreenType ScreenType { get; set; }
    }

    
}