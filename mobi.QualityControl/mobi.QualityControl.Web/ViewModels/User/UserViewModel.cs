﻿using Mobi.QualityControl.Framework.Model;

namespace Mobi.QualityControl.Web.ViewModels.User
{
    public class UserViewModel
    {
        public UserProfile UserProfile { get; set; }
    }
}