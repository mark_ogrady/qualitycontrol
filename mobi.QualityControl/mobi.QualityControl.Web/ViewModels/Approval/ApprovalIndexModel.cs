﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.Views.Approval
{
    public class ApprovalIndexModel
    {
        public ApprovalIndexModel()
        {
            ApprovalIndexItems = new List<ApprovalIndexItem>();
        }

        public List<ApprovalIndexItem> ApprovalIndexItems { get; set; }
    }

    public class ApprovalIndexItem
    {
        public Guid ApprovalStepId { get; set; }
        public Guid TransactionSetId { get; set; }
        public DateTime TransactionCreation { get; set; }
    }
}