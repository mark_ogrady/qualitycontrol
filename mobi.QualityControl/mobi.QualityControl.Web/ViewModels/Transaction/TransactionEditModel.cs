﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mobi.QualityControl.Web.Models;

namespace Mobi.QualityControl.Web.ViewModels.Transaction
{
    public class TransactionEditModel :IViewModel
    {
        public TransactionEditModel()
        {
            Questions = new List<Framework.Model.Question>();
            TransactionItems = new List<TransactionItem>();
        }

        public Guid TransactionSetId { get; set; }
        public Guid QuestionSetId { get; set; }
        public List<TransactionItem> TransactionItems { get; set; }
        public List<Framework.Model.Question> Questions { get; set; }
        public ScreenType ScreenType { get; set; }
    }

    public class TransactionItem
    {
        public Guid TransactionId { get; set; }
        public int QuestionId { get; set; }
        public string Result { get; set; }
    }
}