﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobi.QualityControl.Web.ViewModels.Question
{
    public class QuestionLocalEditModel
    {
        public Guid QuestionId { get; set; }
        public Guid LanguageId { get; set; }
        public string QuestionLocalText { get; set; }

        public string LanguageName { get; set; }
      
    }
}