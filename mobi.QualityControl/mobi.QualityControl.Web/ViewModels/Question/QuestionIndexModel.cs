﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mobi.QualityControl.Web.Models;
using Mobi.QualityControl.Web.ViewModels.ControlType;

namespace Mobi.QualityControl.Web.ViewModels.Question
{
    public class QuestionIndexModel : IViewModel
    {
        public List<QuestionItem> QuestionItems { get; set; }
        public int QuestionSetId { get; set; }

        public QuestionIndexModel()
        {
            QuestionItems = new List<QuestionItem>();
        }

        public ScreenType ScreenType { get; set; }
    }

    public class QuestionItem
    {
        public Guid QuestionId { get; set; }
        public string QuestionText { get; set; }

        public int OrderPosition { get; set; }
        public bool Mandatory { get; set; }
        public string ControlName { get; set; }
    }
}