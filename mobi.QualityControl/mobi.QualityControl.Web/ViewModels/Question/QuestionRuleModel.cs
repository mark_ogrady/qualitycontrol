﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Antlr.Runtime.Misc;

namespace Mobi.QualityControl.Web.ViewModels.Question
{
    public class QuestionRuleModel
    {
        public QuestionRuleModel()
        {
            EvalutionTypes = new ListStack<SelectListItem>();
            QuestionsList = new List<SelectListItem>();
        }

        public List<SelectListItem> EvalutionTypes { get; set; } 
        public List<SelectListItem> QuestionsList { get; set; }

        public Guid QuestionRuleId { get; set; }
        public Guid QuestionId { get; set; }
        public Guid QuestionFromRuleId { get; set; }
        public string EvalutionType { get; set; }
        public string QuestionFromText { get; set; }

        public string EvalutionValue { get; set; }
    }
}