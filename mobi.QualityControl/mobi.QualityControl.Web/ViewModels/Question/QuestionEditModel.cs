﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mobi.QualityControl.Web.Models;

namespace Mobi.QualityControl.Web.ViewModels.Question
{
    public class QuestionEditModel : IViewModel
    {
        public Guid QuestionId { get; set; }

        [Required]
        [Display(Name = "Question")]
        public string QuestionText { get; set; }
        
        public Guid QuestionSetId { get; set; }
        
        public Guid ControlTypeId { get; set; }
        
        public Guid ControlListId { get; set; }
        
        public string ControlName { get; set; }
        
        public int OrderPosition { get; set; }
        
        public string DefaultValue { get; set; }
        
        public bool IsMandatory { get; set; }
        
        public bool IsHidden { get; set; }

        public decimal MaxValue { get; set; }
        public decimal MinValue { get; set; }
        public string Regex { get; set; }
        public string Instructions { get; set; }

        //QuestionRules
       
        
        public List<SelectListItem> ControlTypes { get; set; }
        public List<SelectListItem> ControlLists { get; set; }
        public List<SelectListItem> LanguageList { get; set; }
        public List<QuestionLocalEditModel> QuestionLocalEditModels { get; set; }
        public List<QuestionRuleModel> QuestionRuleModels { get; set; } 
        public List<SelectListItem> QuestionsFromList { get; set; }
        public List<SelectListItem> EvalutionTypes { get; set; }


        public QuestionEditModel()
        {
            ControlTypes = new List<SelectListItem>();
            ControlLists = new List<SelectListItem>();
            LanguageList = new List<SelectListItem>();
            QuestionsFromList = new List<SelectListItem>();
            QuestionLocalEditModels = new List<QuestionLocalEditModel>();
            EvalutionTypes = new List<SelectListItem>();
           QuestionRuleModels = new List<QuestionRuleModel>();
        }

        public ScreenType ScreenType { get; set; }
    }
}