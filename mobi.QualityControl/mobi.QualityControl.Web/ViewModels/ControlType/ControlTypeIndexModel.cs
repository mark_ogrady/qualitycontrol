﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Mobi.QualityControl.Web.ViewModels.ControlType
{
    public class ControlTypeIndexModel
    {
        public List<Mobi.QualityControl.Framework.Model.ControlType> ControlTypeItems { get; set; }
      
        public ControlTypeIndexModel()
        {
            ControlTypeItems = new List<Mobi.QualityControl.Framework.Model.ControlType>();
        }

      
    }

    
}