﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mobi.QualityControl.Framework.Enum;

namespace Mobi.QualityControl.Web.ViewModels.Dashboard
{
    public class DashboardEditModel
    {

        public DashboardEditModel()
        {
            DashboardEditItems = new List<DashboardEditItem>();
            DashboardTypes = new List<SelectListItem>();
            QuestionSetList = new List<SelectListItem>();
        }
        public List<DashboardEditItem> DashboardEditItems { get; set; }
        public List<SelectListItem> DashboardTypes { get; set; }
        public List<SelectListItem> QuestionSetList { get; set; }
    }

    public class DashboardEditItem
    {
        public Guid DashboardItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DashboardType DashboardType { get; set; }
        public int OrderPosition { get; set; }
        public Guid EntityId { get; set; }
    }
}