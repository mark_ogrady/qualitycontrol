﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mobi.QualityControl.Framework.Enum;

namespace Mobi.QualityControl.Web.ViewModels.Dashboard
{
    public class DashboardIndexModel
    {
        public DashboardIndexModel()
        {
            DashboardIndexItems = new List<DashboardIndexItem>();
        }

        public List<DashboardIndexItem> DashboardIndexItems { get; set; }
    }

    public class DashboardIndexItem
    {
        public Guid EntityId { get; set; }
        public DashboardType DashboardType { get; set; }
        public string Name { get; set; }

    }
}