﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mobi.QualityControl.Web.ViewModels.QuestionSet
{
    public class QuestionSetIndexModel
    {
        public List<QuestionSetItem> QuestionSetItems { get; set; }
       

        public QuestionSetIndexModel()
        {
            QuestionSetItems = new List<QuestionSetItem>();
        }
    }

    public class QuestionSetItem
    {
        public QuestionSetItem()
        {
            QuestionItems = new List<QuestionItem>();
        }

        public Guid QuestionSetId { get; set; }
        public string QuestionSetName { get; set; }
        public int QuestionCount { get; set; }
        public int TransactionCount { get; set; }
        public List<QuestionItem> QuestionItems { get; set; }   
    }

    public class QuestionItem
    {
        public Guid QuestionId { get; set; }
        public string QuestionText { get; set; }

    }

    
}