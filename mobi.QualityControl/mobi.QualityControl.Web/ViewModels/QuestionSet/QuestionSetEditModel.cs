﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using System.Web.Mvc;
using Mobi.QualityControl.Web.Models;
using Mobi.QualityControl.Web.ViewModels.Question;

namespace Mobi.QualityControl.Web.ViewModels.QuestionSet
{
    public class QuestionSetEditModel : IViewModel
    {
        public Guid QuestionSetId { get; set; }
        [Display(Name ="Question Set Name")]
        public string QuestionSetName { get; set; }
        public ScreenType ScreenType { get; set; }
        public List<QuestionItem> QuestionItems { get; set; }
        public bool TrackLocation { get; set; }
        public List<ApprovalRuleItem> ApprovalRuleItems { get; set; }
        public List<SelectListItem> UserList { get; set; } 

        public QuestionSetEditModel()
        {
            QuestionItems = new List<QuestionItem>();
            ApprovalRuleItems = new List<ApprovalRuleItem>();
            UserList = new List<SelectListItem>();

        }

        
    }

    public class AddApprovalRuleModel
    {
        public int UserId { get; set; }
        public Guid QuestionSetId { get; set; }
    }


    public class ApprovalRuleItem
    {
        public Guid ApprovalRuleId { get; set; }
        public string UserName { get; set; }
    }
}