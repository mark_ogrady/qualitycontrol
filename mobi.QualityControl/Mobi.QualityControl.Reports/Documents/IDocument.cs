﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobi.QualityControl.Reports.Documents
{
    interface IDocument
    {
        void CreateJobDocument(Guid id);

        void CreateQuestionSetDocumnet(Guid id);
    }
}
