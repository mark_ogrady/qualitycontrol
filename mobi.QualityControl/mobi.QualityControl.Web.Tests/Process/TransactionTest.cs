﻿using System;
using System.Linq;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Enum;
using Mobi.QualityControl.Framework.Helpers;
using Mobi.QualityControl.Framework.Model;
using Mobi.QualityControl.Framework.ViewModel.Transaction;
using Mobi.QualityControl.Web.Helpers;
using NUnit.Framework;
using NUnit.Framework;

namespace Mobi.QualityControl.Web.Tests.Process
{
    [TestFixture]
    public class TransactionTest
    {
        [Test]
        public void AllQuestionsNoRules()
        {
            // test Questions
            //get first question in QuestionSet
            



            //Loop through

            Assert.AreEqual(5,5);

        }
        



        [Test]
        public void DestroyAndCreateTransactionSet()
        {
            var questionSetId = Guid.Parse("CE458E27-2C5E-4528-97B9-36852261F39D");
            var transactionSetId = Guid.Parse("7CE72F17-3D43-42F1-874C-2431F20F455E");
            
            //Delete all transactions
            new TransactionHelper().DeleteTransactionSet(transactionSetId);
            var transactionSet =  new TransactionHelper().CreateTransactionSet(questionSetId,transactionSetId);

            Assert.NotNull(transactionSet);
        }

        [Test]
        public void SevenQuestion1Rule()
        {
            var questionSetId = Guid.Parse("CE458E27-2C5E-4528-97B9-36852261F39D");
            var transactionSetId = Guid.Parse("7DF5C0AF-1F16-4A67-8A77-96FEC1EF1C1F");
            var languageId = LanguageType.English;
            
            //Delete all transactions
            new TransactionHelper().DeleteTransactionSet(transactionSetId);
            var transactionSet =  new TransactionHelper().CreateTransactionSet(questionSetId,transactionSetId);

             var finish = false;

             while (finish == false)
             {
                 var model = new TransactionItemModel();


                 model = new TransactionHelper().SaveAndNextQuestion(model, languageId);

                 if (model == null)
                 {
                     finish = true;
                 }
             }




            Assert.NotNull(transactionSet);
        }
        
        
    }
}
