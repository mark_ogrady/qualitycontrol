﻿using NUnit.Framework;

namespace Mobi.QualityControl.Web.Tests.Mail
{
    /// <summary>
    /// Summary description for GmailTest
    /// </summary>
    [TestFixture]
    public class GmailTest
    {
        [Test]
        public void SendBasicMail()
        {
            Assert.IsTrue(new Framework.Mail.Gmail().SendMail("mark@markogrady.com", "info@qualitycontrol.mobi", "", "", "Subject TEst", "MessageTEST"));
        }
    }
}
