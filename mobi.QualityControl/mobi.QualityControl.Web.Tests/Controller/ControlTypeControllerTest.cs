﻿using System;
using System.Web.Mvc;
using NUnit.Framework;
using Mobi.QualityControl.Web.Controllers;
using Mobi.QualityControl.Web.ViewModels.ControlType;

namespace Mobi.QualityControl.Web.Tests.Controller
{
    [TestFixture]
    public class ControlTypeControllerTest
    {
        [Test]
        public void Index()
        {
            var controller = new ControlTypeController();
            var result = (ViewResult) controller.Index();
            var model = result.Model;

            Assert.IsNotNull(model as ControlTypeIndexModel);

        }
    }
}
