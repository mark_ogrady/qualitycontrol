﻿using System;
using System.Linq;
using NUnit.Framework;
using Mobi.QualityControl.Framework.DataContext;

namespace Mobi.QualityControl.Web.Tests.Fix
{
    [TestFixture]
    public class QuestionFix
    {
        [Test]
        public void CorrectOrderPositions()
        {
            using (var context = new QcContext())
            {
                var questionsQuery = context.QuestionSets;

                foreach (var questionSet in questionsQuery.ToList())
                {
                    var i = 1;
                    foreach (var question in questionSet.Questions)
                    {
                        question.OrderPosition = i++;
                    }
                    context.SaveChanges();
                }

            }
        }

        [Test]
        public void FixSetAllDefaultValues()
        {
            using (var context = new QcContext())
            {

                context.SaveChanges();
            }

        }



        [Test]
        public void FixAllControlResults()
        {
            using (var context = new QcContext())
            {
                var questionsQuery = context.QuestionSets.Where(o => o.Company.CompanyName == "Seed Company 1");

                foreach (var questionSet in questionsQuery.ToList())
                {
                    var i = 0;
                    foreach (var question in questionSet.Questions.Where(o => o.ControlType.ControlName == "DropDown").ToList())
                    {
                        question.ControlList =
                            context.ControlLists.FirstOrDefault(o => o.Company == question.QuestionSet.Company);
                    }
                    context.SaveChanges();
                }

            }
        }
    }
}
