﻿using NUnit.Framework;
using Mobi.QualityControl.Framework.DataContext;

namespace Mobi.QualityControl.Web.Tests.Seed
{
    [TestFixture]
    public class Seed
    {
        [Test]
        public void SeedData()
        {
            var qcDatabaseInitialiser = new QcDatabaseInitialiser();

            using (var context = new QcContext())
            {
                qcDatabaseInitialiser.SeedData(context);
            }
            Assert.IsTrue(true);
        
        
        }

        [Test]
        public void SeedControls()
        {
           
            using (var context = new QcContext())
            {
                Framework.Seed.SeedControls.CreateStandardControls(context);
            }
            Assert.IsTrue(true);


        }

        [Test]
        public void SeedLanguages()
        {

            using (var context = new QcContext())
            {
                Framework.Seed.SeedLanguage.CreateLanguages(context);
            }
            Assert.IsTrue(true);


        }
        [Test]
        public void SeedMenu()
        {

            using (var context = new QcContext())
            {
                Framework.Seed.SeedMenu.CreateStandardMenu(context);
            }
            Assert.IsTrue(true);
        }

        [Test]
        public void SeedControlLists()
        {

            using (var context = new QcContext())
            {
                Framework.Seed.SeedControlList.CreateControlLists(context);
            }
            Assert.IsTrue(true);


        }


        [Test]
        public void SeedMenus()
        {
            

            using (var context = new QcContext())
            {
                Framework.Seed.SeedMenu.CreateStandardMenu(context);
            }
            Assert.IsTrue(true);


        }

        [Test]
        public void AddUserNames()
        {
            using (var context = new QcContext())
            {
                Framework.Seed.SeedUser.FixUsers(context);
            }
        }

        [Test]
        public void AddUserToDatabase()
        {
            using (var context = new QcContext())
            {
                Framework.Seed.SeedCompany.AddSeedUserToCompany(context);
            }
        }


        [Test]
        public void CreateDashboardItems()
        {
            using (var context = new QcContext())
            {
                Framework.Seed.SeedDashboardItems.CreateDashboardItems(context);
            }
        }

        [Test]
        public void CreateSeedQuestionSets()
        {
            using (var context = new QcContext())
            {
                Framework.Seed.SeedQuestion.CreateQuestionSetsForSeedCompany(context);
                Framework.Seed.SeedQuestion.CreateQuestionSetsForLpCompany(context);
            }

        }

        [Test]
        public void CreateSeedQuestions()
        {
            using (var context = new QcContext())
            {
                Framework.Seed.SeedQuestion.AddQuestionsToQuestionSet(context);
            }

        }
        

        [Test]
        public void CreateSeedCompanies()
        {
            using (var context = new QcContext())
            {
                Framework.Seed.SeedCompany.CreateStandardCompany(context);
                Framework.Seed.SeedCompany.AddSeedUserToCompany(context);

            }

        }
        [Test]
        public void CreateSeedCompanieSubscription()
        {
            using (var context = new QcContext())
            {
                Framework.Seed.SeedSubscription.CreateSubscriptions(context);

            }

        }

       


    }
}
