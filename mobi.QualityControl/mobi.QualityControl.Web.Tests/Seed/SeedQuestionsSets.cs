﻿using System;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using Mobi.QualityControl.Framework.Seed;
using NUnit.Framework;
using Mobi.QualityControl.Framework.DataContext;
using Mobi.QualityControl.Framework.Model;

namespace Mobi.QualityControl.Web.Tests.Seed
{
    [TestFixture]
    public class SeedQuestionsSets
    {
        [Test]
        public void SeedTenQuestionsNoRules()
        {
            var questionSet = new QuestionSet();
            
            using (var context = new QcContext())
            {
                questionSet.QuestionSetId = Guid.Parse("0D74B761-125F-43C8-A43F-794D7BD73DD6");
                questionSet.QuestionSetName = "SeedTenQuestionsNoRules";

                for (int i = 0; i < 10; i++)
                {
                    var question = new Question();
                    question.QuestionId = Guid.NewGuid();
                    question.QuestionText = "Seed10 " + i.ToString();
                    question.OrderPosition = i;
                    question.ControlType = context.ControlTypes.FirstOrDefault(o => o.ControlName == "Checkbox");
                    questionSet.Questions.Add(question);
                }
                context.QuestionSets.Add(questionSet);
            }

        }

        [Test]
        public void SeedTenQuestions1Rule()
        {

            using (var context = new QcContext())
            {

                SeedQuestion.CreateQuestionSetWith1Rule(context);

            }
        }


    }
}
