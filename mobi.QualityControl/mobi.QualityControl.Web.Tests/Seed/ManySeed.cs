﻿using System;
using System.Linq;
using Mobi.QualityControl.Framework.Helpers;
using NUnit.Framework;
using Mobi.QualityControl.Framework.DataContext;
using NUnit.Framework;

namespace Mobi.QualityControl.Web.Tests.Seed
{
    [TestFixture]
    public class ManySeed
    {
        [Test]
        public void CreateSeedCompanies()
        {
            using (var context = new QcContext())
            {
                Framework.Seed.SeedCompany.CreateManyCompanies(context);


            }

        }

        [Test]
        public void SeedQuestionSEt()
        {
            using (var context = new QcContext())
            {

                Framework.Seed.SeedQuestion.CreateQuestionsForMany(context);

            }

        }

        [Test]
        public void SeedTransactions()
        {
            using (var context = new QcContext())
            {
                var manyCompanies = context.Companies.Select(o => new
                {
                    o.CompanyId,
                    o.CompanyName,
                    o.QuestionSets.Count,
                    QuestionSets = o.QuestionSets.Select(qs => new {qs.QuestionSetId, qs.TransactionSets.Count})
                }).Where(o => o.CompanyName.StartsWith("Many")).ToList();

               

                foreach (var manyCompany in manyCompanies)
                {
                    foreach (var quesetionSet in manyCompany.QuestionSets)
                    {
                        if (quesetionSet.Count == 0)
                            continue;
                        for (int i = 0; i < 40; i++)
                        {
                            new TransactionHelper().CreateTransactionSet(quesetionSet.QuestionSetId);
                        }

                    }
                }


            }
        }




        [Test]
        public void SeedTransactionsForQuestionSet()
        {
            var questionSetId = Guid.Parse("1fab1922-e4e6-443c-86e6-0a8fa1971953");
            for (int i = 0; i < 1000000; i++)
            {
                 var transaction = new TransactionHelper();
               var trans =  transaction.CreateTransactionSet(questionSetId);

                GC.Collect();
            }

        }


    
    }
}
